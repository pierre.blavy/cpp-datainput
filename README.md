# cpp-datainput
This program is a simple tool used to write data files like measurment in experiment. It provides a simple configurable GUI that checks data and a way to export it in text files.

More details on the [wiki](https://forgemia.inra.fr/pierre.blavy/cpp-datainput/-/wikis/)

# License
* SQlite is in public domain
* The rest of the code is licenced under [LGPL3](https://www.gnu.org/licenses/lgpl-3.0.fr.html)
