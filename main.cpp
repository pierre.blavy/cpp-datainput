#include "widgets/MainWindow.hpp"

#include <QApplication>
#include <QLocale>
#include <QTranslator>

#include <config/config.hpp>
#include <config/convert/convert_QString.hpp>

#include <iostream>

#include <tdb/tdb_sqlite.hpp>



int main(int argc, char *argv[])
{
    QApplication a(argc, argv);
    MainWindow w;
    QTranslator translator;

    config::Config conf;
    conf.load("in/config.txt");

    //configure language
    const QString lang = conf.blocks.get_unique("general").values.get_unique<QString>("language");
    const QString translation_path = "translations/"+lang+".qm";
    bool tr_ok = translator.load(translation_path);
    if(tr_ok){
        std::cout << "Using translation "<< translation_path.toStdString() <<std::endl;
        a.installTranslator(&translator);
    }else{
        std::cerr << "Cannot find translation : "<< translation_path.toStdString() <<std::endl;
    }

    //configure database
    const std::string db_path = conf.blocks.get_unique("general").values.get_unique("database");
    tdb::Connection_t<tdb::Tag_sqlite> db(db_path);

    tdb::execute(db, "create table if not exists meta_table(table_id integer primary key, table_name varchar unique not null, table_description varchar not null );");
    tdb::execute(db, "create table if not exists meta_column(column_id integer primary key, table_id integer not null, column_name varchar not null, column_type varchar not null, column_description varchar not null, foreign key (table_id) references meta_table(table_id), unique(table_id,column_name) );");
    tdb::execute(db, "create view if not exists meta_summary as select * from meta_table natural join meta_column;");
    tdb::execute(db, "create table if not exists meta_enum("
                     " column_id integer not null,"
                     " shortcut varchar not null,"
                     " value varchar not null,"
                     " label varchar not null,"
                     " description varchar not null, "
                     " primary key(column_id,value),"
                     "foreign key (column_id) references meta_column(column_id) )"
                     ";");

    //configure main window
    w.configure(db,conf);
    w.translate();

    w.setStyleSheet("color:#000000");

    w.show();
    return a.exec();
}
