#ifndef INTVALIDATOR_T_HPP
#define INTVALIDATOR_T_HPP

#include <QValidator>
#include <convert/convert.hpp>

#include <convert/from_QString.hpp>

#include <QIntValidator>

//class IntValidator_tag;

template<typename T>
    class IntValidator_t : public QValidator {

    public:
    IntValidator_t(T minimum, T maximum, QObject *parent = nullptr)
        : QValidator(parent)
        , m_min(minimum)
        , m_max(maximum)
    {}

    IntValidator_t(QObject *parent = nullptr)
        : IntValidator_t(std::numeric_limits<T>::lowest(),std::numeric_limits<T>::max(),parent)
    {}

    quint32 bottom() const { return m_min; }
    quint32 top()    const { return m_max; }

    void setBottom(T minimum){
        if(m_min==minimum)
            return;
        m_min=minimum;
        changed();
    }

    void setTop(T maximum){
        if(m_max==maximum)
            return;
        m_max=maximum;
        changed();
    }

    void setRange(T minimum, T maximum){
        setBottom(minimum);
        setTop(maximum);
    }

    QValidator::State validate(QString &input, int&) const override{

        input.remove(' ');
        input.remove('\t');


        if(input.size()==0){return QValidator::Acceptable;}

        //only digits
        static thread_local QRegularExpression re("-{0,1}[0-9]*");
        if(!re.match(input).hasMatch()){return QValidator::Invalid;}

        if(input=="-"){return QValidator::Intermediate;}

        if(input=="-0"){input="0"; return QValidator::Acceptable;}

        //convertible to int
        T new_val;
        try{
            new_val = convert<T >(input);
        }catch(...){
            return QValidator::Invalid;
        }

        //in boundaries
        if(new_val < m_min){
            if(new_val >=0 ){return QValidator::Intermediate;}
            else{return QValidator::Invalid;}
        }

        if(new_val > m_max){
            return QValidator::Invalid;
        }

        return QValidator::Acceptable;
    }


    private:
    T m_min;
    T m_max;
};





#endif // INTVALIDATOR_T_HPP
