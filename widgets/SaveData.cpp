#include "SaveData.hpp"

#include <qtt/qtt_layout.hpp>
#include <QComboBox>
#include <QLabel>
#include <QCheckBox>
#include <QPushButton>
#include <QScrollArea>
#include <widgets/Filepath.hpp>


#include <convert/to_QString.hpp>
#include <tdb/tdb_sqlite.hpp>
#include <tdb/functors/all.hpp>



SaveData::SaveData(tdb::Connection_t<tdb::Tag_sqlite> &db_,const config::Block &conf,  QWidget* parent):
    QWidget(parent),
    db(db_)
{

    //--- widgets ---
    auto * main_layout = new QVBoxLayout();
    this->setLayout(main_layout);

    //general config
    QWidget* config_widget=nullptr;
    qtt::cstr_in_layout(config_widget,main_layout);
    auto * config_layout = new QGridLayout();
    config_layout->setSpacing(0);
    config_layout->setContentsMargins(0,0,0,0);
    config_widget->setLayout(config_layout);
    config_widget->setStyleSheet ("padding:0px;");


    qtt::cstr_in_grid(folder_label,config_layout,0,0);
    qtt::cstr_in_grid(folder_edit ,config_layout,0,1);

    folder_edit->set_directory();
    folder_edit->set_must_exists();

    //tables
    qtt::cstr_in_layout(tables_label,main_layout);

    QScrollArea * tables_scroll=nullptr;
    qtt::cstr_in_layout(tables_scroll,main_layout);

    QVBoxLayout * tables_layout=new QVBoxLayout();
    tables_scroll->setLayout(tables_layout);
    tables_scroll->setVerticalScrollBarPolicy(Qt::ScrollBarAsNeeded);

    //checkboxes
    tdb::Fn_foreach<tdb::Tag_sqlite,std::tuple<std::string,std::string>,std::tuple<>,true> q_read_meta_table;
    q_read_meta_table.prepare(db,"select table_name, table_description from meta_table  where table_name not in ('meta_column','meta_table') order by table_name");
    q_read_meta_table([&](std::string n, std::string d){
        QCheckBox * check_table=nullptr;
        qtt::cstr_in_layout(check_table, tables_layout, convert<QString>(n) );
        check_table->setStyleSheet("padding:3px;");
        check_table->setSizePolicy(QSizePolicy::Expanding,QSizePolicy::Minimum );
        check_table->setToolTip( convert<QString>(d) );

        check_table->setCheckState(Qt::Checked);
        ++ntables;

        connect(check_table,&QCheckBox::stateChanged, this, [&,n](int state){on_table_check(state==2,n); } );

        table_checked.push_back(check_table);
    });

    qtt::space_v(tables_layout);
    qtt::cstr_in_layout(save_button,main_layout);

    //connect
    //checkboxes are connected in the checkboxes loop
    connect(folder_edit,&Filepath::statusChanged,this,&SaveData::update_ntables);
    connect(save_button,&QPushButton::clicked   ,this,&SaveData::on_save_click);


    translate(false);

}

void SaveData::translate(bool tr_sons){
    folder_label->setText(tr("Save directory"));
    tables_label->setText(tr("Tables"));
    tables_label->setToolTip(tr("Checked tables will be saved"));

    update_ntables();
}




void SaveData::on_table_check(bool checked, const std::string &table_name){
    if(checked){++ntables;}else{--ntables;}
    update_ntables();
}


void SaveData::on_save_click(){


    QStringList tables;
    for(const QCheckBox *b :table_checked ){
        if(b->isChecked()){tables.push_back(b->text());}
    }

    QString path = folder_edit->path();

    if(path!="" and tables.size()>0){
      emit saveClicked(path,tables);
    }
}



void SaveData::update_ntables(){

    QString text;
    QString tt;
    bool enabled=true;

    auto add_text=[&](const QString&s){if(text!=""){text+=" ";}text+=s;};
    auto add_tt  =[&](const QString&s){if(tt  !=""){tt  +="\n";}tt  +=s;};

    //--- check the number of tables selected ---
    if(ntables==0){
        //nothing to save
        add_text(tr("Nothing to save",""));
        add_tt(tr("Please, check at least one table"));
        enabled=false;
    }else{
        add_text(tr("Save %n table(s)","",ntables));
    }


    //--- check the folder path ---
    if(folder_edit->status()==2){
        add_text(tr("Invalid directory","must be short"));
        add_tt  (tr("Invalid save directory","can be long"));
        enabled=false;
    }

   save_button->setText( text);
   save_button->setToolTip( tt);
   save_button->setEnabled(enabled);
}
