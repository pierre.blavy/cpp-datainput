#ifndef ISOK_PIERRE_HPP
#define ISOK_PIERRE_HPP

#include <QWidget>

class QLabel;


class IsOk : public QWidget{
    Q_OBJECT
public:
    enum Status_e{OK=0,WARNING=1,ERROR=2};

    explicit IsOk (QWidget*parent=nullptr);

    void set_ok();
    void set_warning(const QString &msg);
    void set_error  (const QString &msg);

    bool is_ok()     const{return state_v==OK;}
    bool is_warning()const{return state_v==WARNING;}
    bool is_error()  const{return state_v==ERROR;}

    QString get_message()const;

    Status_e value()const;

    signals:
    void valueChanged(Status_e);

    private:
    Status_e state_v = ERROR;
    QLabel*label=nullptr;

    void setState(Status_e);

};






#endif
