#ifndef WIDGETS_MAINWINDOW_PIERRE_HPP
#define WIDGETS_MAINWINDOW_PIERRE_HPP

#include <QMainWindow>
#include <qtt/qtt_Translatable.hpp>


namespace config{class Block;}
class QTabWidget;
class TableWidget;
class SaveData;
class Tools;

namespace tdb{
  class Tag_sqlite;
  template<typename T> class Connection_t;
}

class MainWindow : public QMainWindow, qtt::Translatable{
    Q_OBJECT

    public :
    MainWindow();
    void configure(tdb::Connection_t<tdb::Tag_sqlite>&, const config::Block &conf);
    void translate(bool tr_sons=true)override final;


    private:
    QTabWidget * tabs = nullptr;
    std::unordered_map<std::string, TableWidget *> tab_map;//table_name => TableWidget* (NOT owned)

    SaveData * save_data=nullptr;
    int save_data_i=0;//index in tabs

    Tools * tools=nullptr;
    int tools_i=0;//index in tabs

    tdb::Connection_t<tdb::Tag_sqlite> *db=nullptr;

    void on_save(const QString path, const QStringList & table_names);



};


#endif
