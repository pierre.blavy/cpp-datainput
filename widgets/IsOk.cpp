#include "IsOk.hpp"
#include <qtt/qtt_layout.hpp>
#include <QLabel>

namespace{

static const char *xpm_ok[] = {
    /* columns rows colors chars-per-pixel */
    "32 32 2 1 ",
    "  c None",
    ". c green",
    /* pixels */
    "        .....                   ",
    "    ...........                 ",
    "   .............     ...        ",
    "  ...............   .....      .",
    " ......... ......   .....    ...",
    " .....       ....   ....    ....",
    " ....        .....  ....    ....",
    ".....        .....  ....   .....",
    ".....         ....  ....  ..... ",
    "....          ....  .... .....  ",
    "....          ....  ..........  ",
    "....          ..... .........   ",
    ".....         ..... ........    ",
    ".....         ............      ",
    " ....         ...........       ",
    " ....         .... .....        ",
    " ....         .... ......       ",
    " .....       ..... ......       ",
    " .....       ..... .......      ",
    "  .....     .....  ........     ",
    "  .....    ......  ........     ",
    "   .....  ......   .........    ",
    "   ............    .........    ",
    "    ..........     .... .....   ",
    "     ........     ..... ......  ",
    "      ......      .....  ...... ",
    "                  .....   ......",
    "                  .....   ......",
    "                  ....     .....",
    "                   ..        ...",
    "                                ",
    "                                "
};


static const char *xpm_error[] = {
    /* columns rows colors chars-per-pixel */
    "32 32 2 1 ",
    "  c None",
    ". c red",
    /* pixels */
    " ...                            ",
    ".....                        .. ",
    "......                      ....",
    " ......                    .....",
    "  ......                  ......",
    "   ......               ....... ",
    "    ......            ........  ",
    "     ......          ........   ",
    "      ......        ........    ",
    "       ......      .......      ",
    "        ......    ......        ",
    "         ......  ......         ",
    "          ..... ......          ",
    "           ..........           ",
    "           .........            ",
    "            .......             ",
    "           ........             ",
    "          .........             ",
    "         ...........            ",
    "        ...... ......           ",
    "      .......   .....           ",
    "     .......     .....          ",
    "    .......       .....         ",
    "   ......         ......        ",
    "   .....           ......       ",
    "  .....             ......      ",
    " ......              .......    ",
    " .....                ........  ",
    " ....                  ........ ",
    " ....                   ........",
    " ....                     ......",
    "  ..                        ... "
};

static const char *xpm_warning[] = {
    /* columns rows colors chars-per-pixel */
    "32 32 3 1 ",
    "  c None",
    ". c black",
    "X c yellow",
    /* pixels */
    "               XXXX             ",
    "              XXXXX             ",
    "             XXXXXXX            ",
    "             XXXXXXX            ",
    "            XXXXXXXXX           ",
    "            XXXXXXXXX           ",
    "           XXXX..XXXXX          ",
    "           XXX....XXXX          ",
    "          XXXX....XXXXX         ",
    "         XXXXX....XXXXX         ",
    "         XXXXX....XXXXX         ",
    "        XXXXXX....XXXXXX        ",
    "       XXXXXXX....XXXXXX        ",
    "       XXXXXXX....XXXXXXX       ",
    "      XXXXXXXX....XXXXXXX       ",
    "      XXXXXXXX....XXXXXXXX      ",
    "     XXXXXXXXX....XXXXXXXX      ",
    "     XXXXXXXXX....XXXXXXXXX     ",
    "    XXXXXXXXXX....XXXXXXXXX     ",
    "    XXXXXXXXXX....XXXXXXXXX     ",
    "   XXXXXXXXXXX....XXXXXXXXXX    ",
    "  XXXXXXXXXXXX....XXXXXXXXXX    ",
    "  XXXXXXXXXXXXX..XXXXXXXXXXXX   ",
    " XXXXXXXXXXXXXXXXXXXXXXXXXXXX   ",
    " XXXXXXXXXXXXXXXXXXXXXXXXXXXXX  ",
    "XXXXXXXXXXXXXXX..XXXXXXXXXXXXX  ",
    "XXXXXXXXXXXXXX....XXXXXXXXXXXXX ",
    "XXXXXXXXXXXXXX....XXXXXXXXXXXXXX",
    "XXXXXXXXXXXXXXX..XXXXXXXXXXXXXXX",
    " XXXXXXXXXXXXXXXXXXXXXXXXXXXXXXX",
    "        XXXXXXXXXXXXXXXXXXX     ",
    "                                "
};

}

QString IsOk::get_message()const{
    return label->toolTip();
}



IsOk::IsOk (QWidget*parent):QWidget(parent){
    auto main_layout = qtt::new_layout<QHBoxLayout>();
    this->setLayout(main_layout);
    this->setStyleSheet("padding:0px");
    main_layout->setContentsMargins(0,0,0,0);
    main_layout->setSpacing(0);
    qtt::cstr_in_layout(label,main_layout);

    label->setSizePolicy(QSizePolicy::Minimum,QSizePolicy::Minimum );
    this ->setSizePolicy(QSizePolicy::Minimum,QSizePolicy::Minimum );

    set_error("undefined");
}



void IsOk::set_ok(){
    label->setToolTip("");
    label->setPixmap(QPixmap(xpm_ok));
    setState(OK);
}


void IsOk::set_warning(const QString &msg){
    label->setToolTip(msg);
    label->setPixmap(QPixmap(xpm_warning));
    setState(WARNING);
}



void IsOk::set_error  (const QString &msg){
    label->setToolTip(msg);
    label->setPixmap(QPixmap(xpm_error));
    setState(ERROR);

}


void IsOk::setState(Status_e e){
  if(e==state_v){return;}
  state_v=e;
  emit valueChanged(e);
}

IsOk::Status_e IsOk::value()const{
    return state_v;
}


