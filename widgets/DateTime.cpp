
#include "DateTime.hpp"
#include "convert/convert.hpp"
#include "qregularexpression.h"
#include "qtt/qtt_layout.hpp"

#include <widgets/IsOk.hpp>
#include <QLabel>
#include <QPushButton>
#include <QLineEdit>

#include <QRegularExpression>
#include <QDate>
#include <QTime>
#include <QTimeZone>
#include <QDateTime>
#include <QApplication>


#include <convert/from_QString.hpp>
#include <convert/to_QString.hpp>
#include <convert/QDateTime_to_QString.hpp>

#include <QKeyEvent>

/*
namespace{
template<typename T>
QString to_digits(const T&t, qsizetype n){
    QString r = convert<QString>(t);
    while(r.size()<n){r='0'+r;}
    return r;
}
}
*/

/*
QString DateTime::to_string(const QDateTime &dt){
    return
          str::to_digits<Qstring>( dt.date().year()        , 4 ) + "-"
        + str::to_digits<Qstring>( dt.date().month()       , 2 ) + "-"
        + str::to_digits<Qstring>( dt.date().day() , 2 ) + " "
        + str::to_digits<Qstring>( dt.time().hour() , 2 ) + ":"
        + str::to_digits<Qstring>( dt.time().minute() , 2 ) + ":"
        + str::to_digits<Qstring>( dt.time().second() , 2 ) + "."
        + str::to_digits<Qstring>( dt.time().msec()   , 3 ) + " "
        + dt.timeZone().id()
        ;
}
*/


bool DateTime_keyhandler::eventFilter(QObject* obj, QEvent* event){
    //https://wiki.qt.io/How_to_catch_enter_key
    if (event->type()==QEvent::KeyPress) {
        QKeyEvent* key = static_cast<QKeyEvent*>(event);
        if ( (key->key()==Qt::Key_T) &&  ( QApplication::keyboardModifiers() & Qt::ControlModifier)  ) {
            dt->on_now();
            dt->setFocus();
        } else {
            return QObject::eventFilter(obj, event);
        }
        return true;
    } else {
        return QObject::eventFilter(obj, event);
    }
    return false;
}



DateTime::DateTime (const DateTime_config &conf, QWidget*parent):
  QWidget(parent),
  DateTime_config(conf)
{
    auto main_layout = qtt::new_layout<QHBoxLayout>();
    this->setLayout(main_layout);

    qtt::cstr_in_layout(value_edit,     main_layout);
    qtt::cstr_in_layout(now_button,     main_layout);
    qtt::cstr_in_layout(timezone_label, main_layout);
    qtt::cstr_in_layout(na_button,      main_layout);
    qtt::cstr_in_layout(isok      ,     main_layout);

    now_button->setStyleSheet("padding:3px;");
    na_button->setStyleSheet ("padding:3px;");


    value_edit->installEventFilter(new DateTime_keyhandler(this));

    value_edit     ->setSizePolicy(QSizePolicy::Expanding,QSizePolicy::Minimum );//max
    now_button     ->setSizePolicy(QSizePolicy::Minimum,QSizePolicy::Minimum );
    na_button     ->setSizePolicy(QSizePolicy::Minimum,QSizePolicy::Minimum );

    timezone_label ->setSizePolicy(QSizePolicy::Minimum,QSizePolicy::Minimum );
    isok           ->setSizePolicy(QSizePolicy::Minimum,QSizePolicy::Minimum );

    //todo connect and validate
    connect(value_edit,&QLineEdit::textChanged, this, [&]{on_change();} );
    connect(now_button,&QPushButton::clicked  , this, [&]{on_now();} );
    connect(na_button,&QPushButton::clicked   , this, [&]{set_na();} );

    set_na();

    translate(false);
}


DateTime::DateTime(QWidget*parent):DateTime(DateTime_config(),parent) {};



void DateTime::translate(bool){
    now_button->setText   (tr("Now"));
    now_button->setToolTip(tr("Set date and time to the current date. Keyboard shortcut Ctrl+T"));

    na_button->setText   (tr("NA"));
    na_button->setToolTip(tr("Set to NA"));

    QString avaiable_tz;
    QTimeZone tz;
    for(const auto &x : tz.availableTimeZoneIds()){
        avaiable_tz+=", ";
        avaiable_tz+=x;
    }
    timezone_label->setToolTip(tr("Available timezones:%1").arg(avaiable_tz));

}

#include<QMessageBox>

void DateTime::on_change(){
    static const QRegularExpression r_date{
        //without timezone
         "([0-9]{4}-[0-9]{2}-[0-9]{2}\\s+[0-9]{2}:[0-9]{2}:[0-9]{2}\\.[0-9]{3})"
        "|([0-9]{4}-[0-9]{2}-[0-9]{2}\\s+[0-9]{2}:[0-9]{2}:[0-9]{2})"
        "|([0-9]{4}-[0-9]{2}-[0-9]{2}\\s+[0-9]{2}:[0-9]{2})"
        "|([0-9]{4}-[0-9]{2}-[0-9]{2})"

        //with timezone  \\s*[a-zA-Z].*
        "|([0-9]{4}-[0-9]{2}-[0-9]{2}\\s+[0-9]{2}:[0-9]{2}:[0-9]{2}\\.[0-9]{3}\\s*[a-zA-Z].*)"
        "|([0-9]{4}-[0-9]{2}-[0-9]{2}\\s+[0-9]{2}:[0-9]{2}:[0-9]{2}\\s*[a-zA-Z].*)"
        "|([0-9]{4}-[0-9]{2}-[0-9]{2}\\s+[0-9]{2}:[0-9]{2}\\s*[a-zA-Z].*)"
        "|([0-9]{4}-[0-9]{2}-[0-9]{2}\\s*[a-zA-Z].*)"

        //only digits without timezone
        "|([0-9]{17})"  //yyyymmddhhmmsszzz
        "|([0-9]{14})"  //yyyymmddhhmmss
        "|([0-9]{8})"  //yyyymmdd

        //only digits with timezone
        "|([0-9]{17}\\s*[a-zA-Z].*)"  //yyyymmddhhmmsszzz tz
        "|([0-9]{14}\\s*[a-zA-Z].*)"  //yyyymmddhhmmss tz
        "|([0-9]{8}\\s*[a-zA-Z].*)"  //yyyymmdd tz
    };

    //this->dt=QDateTime();//invalid datetime


    QString s = value_edit->text().trimmed();
    {
        QString s_lower= s.toLower();
        if(s_lower=="na" or s_lower=="nan"){
            set_na();
            return;
        }
    }


    if( ! r_date.match(s). hasMatch() ){
        set_error(tr("Invalid date format") );
        return;
    }



    //normalize s
    s.replace('-',"")
     .replace(':',"")
     .replace('.',"")
     .replace(' ',"")
        .replace('\t',"");

    //split digits and timezone
    QString digits;
    QString tz;
    bool is_digit=true;
    for(const char c : s.toStdString()){
        is_digit &= (std::isdigit(c)!=0);
        if(is_digit){
            qDebug() << c << " is digit";
            digits+=c;
        }else{
            qDebug() << c << " is NOT digit";

            is_digit=false;
            tz+=c;
        }
    }
    tz=tz.trimmed();

    //parse timezone
    QTimeZone timezone;
    if(tz=="locale"){
        timezone=QTimeZone::systemTimeZone() ;
    }if(tz==""){
        timezone=QTimeZone(in_timezone) ;
    }else{
        timezone=QTimeZone(tz.toLocal8Bit()) ;
    }

    //update_tz(timezone);



    //parse date and time
    QDate date;
    QTime time;
    if(digits.length()==17){
        int y = convert<int>(digits.mid(0,4));
        int m = convert<int>(digits.mid(4,2));
        int d = convert<int>(digits.mid(6,2));

        int hh = convert<int>(digits.mid(8,2));
        int mm = convert<int>(digits.mid(10,2));
        int ss = convert<int>(digits.mid(12,2));
        int z = convert<int>(digits.mid(14,3));

        date=QDate(y,m,d);
        time=QTime(hh,mm,ss,z);
    }else if(digits.length()==14){
        int y = convert<int>(digits.mid(0,4));
        int m = convert<int>(digits.mid(4,2));
        int d = convert<int>(digits.mid(6,2));

        int hh = convert<int>(digits.mid(8,2));
        int mm = convert<int>(digits.mid(10,2));
        int ss = convert<int>(digits.mid(12,2));

        date=QDate(y,m,d);
        time=QTime(hh,mm,ss,default_z);
    }else if(digits.length()==8){
        int y = convert<int>(digits.mid(0,4));
        int m = convert<int>(digits.mid(4,2));
        int d = convert<int>(digits.mid(6,2));

        /*QMessageBox msgBox;
        msgBox.setText(tr("s=%1,d=%2").arg(digits.mid(6,2)).arg(d));
        msgBox.exec();*/
        date=QDate(y,m,d);
        time=QTime(default_hh,default_mm,default_ss,default_z);
    }


    QString err="";
    bool has_error=false;
    if(!timezone.isValid()){
        if(has_error){err+="\n";}
        err+=tr("Invalid timezone, timezone=%1").arg(tz);
        has_error=true;
    }

    if(!date.isValid()){
        if(has_error){err+="\n";}
        err+=tr("Invalid date");
        has_error=true;
    }

    if(!time.isValid()){
        if(err!=""){err+="\n";}
        err+=tr("Invalid time");
        has_error=true;
    }

    if(has_error){
        set_error(err);
        return;
    }


    set_ok( QDateTime(date,time,timezone) );


    //TODO store dt somewhere
}

void DateTime::set_error(const QString &msg){
    dt=QDateTime();
    isok->set_error(msg);
    value_edit->setToolTip(tr("Error=%1").arg(msg));
    timezone_label->setText("??");
}


void DateTime::set_ok(const QDateTime &new_dt){
    value_edit->setToolTip(
        tr("Value=%1")   .arg(convert<QString>(new_dt)) + "\n"+
        tr("UTC=%1")     .arg(convert<QString>( new_dt.toUTC() )) + "\n"+
        tr("timestamp=%1").arg( new_dt.toMSecsSinceEpoch() )
    );


    isok->set_ok();
    dt=new_dt;
    timezone_label->setText(new_dt.timeZone().id() );

}

void DateTime::set_na(){
    isok->set_warning(tr("Datetime is NA"));
    value_edit->setToolTip(tr("Value=%1").arg("NA"));
    value_edit->setText("NA");
    timezone_label->setText("NA");
    dt=std::nullopt;
}


QString DateTime::text()const{
    return value_edit->text();
}


void DateTime::setText(const QString &s){
    if(s==value_edit->text()){return;}

    value_edit->setText(s) ;
    on_change();
}

std::optional<DateTime::timestamp_t> DateTime::timestamp()const{
    if( dt.has_value() ){
        if(dt->isValid()){return dt->toMSecsSinceEpoch();}
        else{return std::nullopt;}
    }else{
        return std::nullopt;
    }
}

/*
void DateTime::setTimestamp(const std::optional<DateTime::timestamp_t> &ts){
    if(ts.has_value()){
        dt=QDateTime::fromMSecsSinceEpoch(ts.value());
        setText(to_string(dt.value()));
    }else{
        dt=std::nullopt;
        setText("");
    }
}
*/

void DateTime::setTimestamp(const std::optional<DateTime::timestamp_t> &ts){
    setTimestamp(ts,out_timezone);
}

void DateTime::setTimestamp(const std::optional<timestamp_t> &ts, const QTimeZone &tz){
    if(ts.has_value()){
        dt=QDateTime::fromMSecsSinceEpoch(ts.value(),tz);
        //qDebug() << tz.abbreviation(dt.value());
        setText(convert<QString>(dt.value()));
    }else{
        dt=std::nullopt;
        setText("");
    }
}






void DateTime::on_now(){
    dt=QDateTime::currentDateTime();
    setText(convert<QString>(dt.value()));
}

void DateTime::setFocus(){
    value_edit->setFocus();
}

bool DateTime::isValid()const{
    if(! dt.has_value()){return true;}//NA is valid
    return dt.value().isValid();
}
