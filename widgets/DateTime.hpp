#ifndef DATETIME_WIDGET_PIERRE_HPP
#define DATETIME_WIDGET_PIERRE_HPP


#include "qtimezone.h"
#include <QWidget>
#include <qtt/qtt_Translatable.hpp>

#include <QDateTime>
#include <QEvent>

class QLineEdit;
class IsOk;
class QLabel;
class QPushButton;
class QTimeZone;
class DateTime;


struct DateTime_config{
    int default_hh=0;
    int default_mm=0;
    int default_ss=0;
    int default_z =0;
    QTimeZone out_timezone{QTimeZone::UTC};//prefered timezone, how things are displayed
    QTimeZone in_timezone{QTimeZone::UTC};//default read timezone
};

class DateTime_keyhandler:public QObject {
    Q_OBJECT
    public:
    explicit DateTime_keyhandler(DateTime * dt_):dt(dt_){}
    protected:
    virtual bool eventFilter(QObject* obj, QEvent* event)override;
    DateTime * dt=nullptr;
};


class DateTime : public QWidget,DateTime_config,qtt::Translatable{
    Q_OBJECT

    public:
    typedef qint64 timestamp_t;

    explicit DateTime (QWidget*parent=nullptr);
    explicit DateTime (const DateTime_config &conf, QWidget*parent=nullptr);


    void translate(bool tr_sons=true)override final;



    QString text()const;
    void setText(const QString &);

    std::optional<timestamp_t> timestamp()const;
    void setTimestamp(const std::optional<timestamp_t> &ts);
    void setTimestamp(const std::optional<timestamp_t> &ts, const QTimeZone &tz);


    bool isValid()const; //true if NA or if dt has a valid value


    void setFocus();

    //static QString to_string(const QDateTime &dt);//helper

    private:
    QLineEdit   * value_edit     = nullptr;
    QPushButton * now_button     = nullptr;
    QPushButton * na_button      = nullptr;
    QLabel      * timezone_label = nullptr;
    IsOk        * isok           = nullptr;

    std::optional<QDateTime> dt;
      //nullopt : valid NA
      //invalid value : something wrong
      //valid value : OK

    private:
    //void update_tz(const QTimeZone &);
    void on_change();
    void on_now();


    void set_ok(const QDateTime &new_dt);
    void set_na();
    void set_error(const QString &message);


    friend class DateTime_keyhandler;
};



/*
struct TimestampWidget_config{
  typedef qint64 timestamp_t;

  //timestamp_type
  enum Type_timestamp_e{
    SECONDS;
    MILLISECONDS;
  }
  Type_timestamp_e type_timestamp=SECONDS;
  
  
  //Date type
  enum Date_type_e{
    DATE,         //yyyy-mm-dd
    DATETIME,     //yyyy-mm-dd hh:mm:ss
    DATETIME_MS   //yyyy-mm-dd hh:mm:ss.zzz  };
  Date_type_e date_type = DATETIME;
  
  std::string in_timezone = "UTC"; //locale as a special value
  
  std::string default_string="";
  std::optional<std::string> default_daytime;
  std::optional<std::string> default_ms;
  
  timestamp_t min_ts = std::numeric_limits<timestamp_t>::lowest();
  timestamp_t max_ts = std::numeric_limits<timestamp_t>::max();
    
  //-- how to print --
  std::string out_timezone = "UTC";
  
};

  bool parse_timestamp(const QString &s, TimestampWidget_config::timestamp_t &write_here, std::string& error_str);;

*/



#endif
