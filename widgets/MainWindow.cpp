#include "MainWindow.hpp"

#include <widgets/TableWidget.hpp>
#include <widgets/SaveData.hpp>

#include "config/config.hpp"
#include "config/get_multiline.hpp"

#include "config/convert/convert_QString.hpp"

#include <convert/to_QString.hpp>

#include <tdb/tdb_sqlite.hpp>
#include <tdb/functors/Fn_execute.hpp>


#include <qtt/qtt_layout.hpp>

#include <iostream>

#include <QMessageBox>

#include <widgets/Tools.hpp>

MainWindow::MainWindow(){
    tabs=new QTabWidget (nullptr);
    this->setCentralWidget(tabs);
    translate(false);
}





void MainWindow::configure(tdb::Connection_t<tdb::Tag_sqlite>&db_r, const config::Block &conf){
    db=&db_r;

    //--- configure the database ---
    //create and prepare
    tdb::Fn_execute<tdb::Tag_sqlite,std::tuple<>,std::tuple<std::string,std::string>,true>             q_insert_meta_table;
    tdb::Fn_execute<tdb::Tag_sqlite,std::tuple<>,std::tuple<std::string,std::string, std::string,std::string>,true> q_insert_meta_column;
    q_insert_meta_table .prepare(*db,"insert or ignore into meta_table(table_name,table_description)values(?,?)");
    q_insert_meta_column.prepare(*db,"insert or ignore into meta_column(table_id,column_name,column_type,column_description)values((select table_id from meta_table where table_name=?),?,?,?)");

    q_insert_meta_table("meta_table" ,"Meta data : description of all tables");
    q_insert_meta_table("meta_column","Meta data : description of all columns");



    //--- tables widgets --
    //tables and column must exists in meta, before being actualy constructed

    for(const auto & conf_table : conf.blocks.crange("table")){

        std::string table_name        = conf_table.values.get_unique<std::string>("name");
        std::string table_description = get_multiline(conf_table,"description",true);
        std::cout << "Create table : " <<table_name <<std::endl;
        q_insert_meta_table(table_name,table_description);

        //register column in meta
        for(const auto &conf_col :conf_table.blocks.crange("column")  ){
            std::string column_name        = conf_col.values.get_unique<std::string>("name");
            std::string column_type        = conf_col.values.get_unique<std::string>("type");
            std::string column_description = get_multiline(conf_col,"description",true);
            q_insert_meta_column(table_name,column_name,column_type,column_description);
        }

        TableWidget * tmp = new TableWidget(conf_table, *db, nullptr);
        int tab_index = tabs->addTab(tmp, convert<QString>(table_name) );
        tab_map.emplace(table_name,tmp);
        tabs->setTabToolTip(tab_index,QString::fromStdString(tmp->description));
        std::cout << "OK" << std::endl;
    }

    //Save data
    save_data=new SaveData(db_r,conf);
    save_data_i = tabs->addTab(save_data,tr("<Export>"));

    //Tools
    tools    = new Tools(conf.blocks.get_unique("tools"));
    tools_i  = tabs->addTab(tools,tr("<Tools>"));


    connect(save_data,&SaveData::saveClicked,this,&MainWindow::on_save);


}


void MainWindow::translate(bool tr_sons){
    this->setWindowTitle(tr("Input editor"));

    tabs->setTabText(save_data_i,tr("&Export data","Export data tab, use E as shortcut") );
    tabs->setTabText(tools_i    ,tr("&Tools","Tools tabs, use T as shortcut") );


    if(tr_sons){
        save_data->translate();

    }

}


void MainWindow::on_save(const QString path, const QStringList & table_names){
    bool ok=true;
    QString err;

    auto add_err=[&](const QString &s){
        if(err!=""){err+="\n";}
        err+=s;
        ok=false;
    };


    for(const QString &tn : table_names){
        try{
           TableWidget *tw =tab_map.at(tn.toStdString());
           tw->save_table(path+"/"+tn+".tsv");
        }catch(std::exception &e){
           add_err( tn+":"+e.what());
        }
    }


    if(!ok){
        QMessageBox msgBox(
            QMessageBox::Critical,
            tr("Error : cannot save"),
            tr("Errors:\n")+err
        );
        msgBox.setStandardButtons(QMessageBox::Ok);
        msgBox.setDefaultButton  (QMessageBox::Ok);
        msgBox.exec();
    }
}

