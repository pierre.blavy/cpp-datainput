#include "TableWidget.hpp"

#include "columns/Column_factory.hpp"

#include <config/config.hpp>
#include <config/get_multiline.hpp>

#include <validate/is_name.hpp>
#include <qtt/qtt_layout.hpp>


#include <QLabel>
#include <QTableWidget>
#include <QPushButton>
#include <QRadioButton>
#include <QFile>

#include <fstream>

#include "convert/to_QString.hpp"
#include "convert/from_QString.hpp"

namespace{
template<typename T>
[[nodiscard]] QTableWidgetItem * new_item_str(const T&t){
    QTableWidgetItem * r = new QTableWidgetItem ( );
    r->setFlags( r->flags() &~ Qt::ItemIsEditable );
    r->setData(Qt::EditRole,convert<QString>(t));
    return r;
}


template<typename T>
[[nodiscard]] QTableWidgetItem * new_item(const T&t){
    QTableWidgetItem * r = new QTableWidgetItem ( );
    r->setFlags( r->flags() &~ Qt::ItemIsEditable );
    r->setData(Qt::DisplayRole,t);
    return r;
}

void select_cell(QTableWidget* table_widget,int row, int column){

    //select a cell
    auto index = table_widget->model()->index(row, column);
    table_widget->selectionModel()->select(
        index, QItemSelectionModel::Select | QItemSelectionModel::Current
    );

    //set current cell
    table_widget->setCurrentCell(row,column);

}



}




void TableWidget::translate(bool tr_sons){

    if(button_delete!=nullptr){ button_delete->setText(tr("Delete")); }
    if(button_modify!=nullptr){ button_modify->setText(tr("Modify")); }
    if(button_create!=nullptr){ button_create->setText(tr("Add new line")); }

    radio_label->setText("<b>"+tr("Mode")+"</b>");

    radio_add ->setText(tr("Add"));
    radio_edit->setText(tr("Modify/Delete"));



    if(tr_sons){

    }

     update_editor_title();
}




TableWidget::TableWidget(const config::Block &conf, tdb::Connection_t<tdb::Tag_sqlite> &db_,  QWidget*parent):QWidget(parent),db(db_){



    table_name  = conf.values.get_unique("name");
    description = get_multiline(conf,"description");

    if(!validate::is_name(table_name)){
        throw std::runtime_error("Invalid table name, a name start with a letter, and contains only letters, numbers and underscores. Error at "+conf.debug_str() );
    }

    //Configure columns
    col_vect   .reserve(conf.blocks.size("column"));
    widget_vect.reserve(conf.blocks.size("column"));

    for(const config::Config &conf_col : conf.blocks.crange("column") ){
        col_vect.emplace_back( new_column(db_, conf_col) );
    }

    //--- create table ---
    tdb::execute(db,sql_create()); {
        std::string sql_create_table="create table if not exists \"" +table_name+"\" (";
        bool is_first = true;
        for(const auto &col : col_vect){
            if(is_first){is_first=false;sql_create_table+="   ";}
            else{sql_create_table+="  ,";}
            sql_create_table+=col->sql_create()+"\n";
        }
        sql_create_table+=");";
        tdb::execute(db,sql_create_table);
    }

    //--- prepare column queries (need create table) ---
    for(const auto &col : col_vect){
        col->prepare();
    }


    //=== Create widgets ===

    //Main layout contains editor_widget and table_widget
    auto main_layout = new QVBoxLayout();
    this->setLayout(main_layout);

    QWidget*      editor_widget = nullptr;
    qtt::cstr_in_layout(editor_widget,main_layout);
    qtt::cstr_in_layout(table_widget,main_layout,0,col_vect.size()+1);

    //editor constains editor_title and editor_columns_widget
    auto editor_layout = qtt::new_layout<QVBoxLayout>();
    editor_widget->setLayout(editor_layout);

    QWidget*      radio_widget = nullptr;
    qtt::cstr_in_layout(radio_widget,editor_layout);
    auto radio_layout =  qtt::new_layout<QHBoxLayout>();
    radio_widget->setLayout(radio_layout);
    qtt::cstr_in_layout(radio_label,radio_layout);
    qtt::cstr_in_layout(radio_add,radio_layout);
    qtt::cstr_in_layout(radio_edit,radio_layout);

    qtt::cstr_in_layout(editor_title,editor_layout);
    editor_title->setText("TODO title");

    QWidget* editor_columns_widget = nullptr;
    qtt::cstr_in_layout(editor_columns_widget,editor_layout);


    //editor_columns_widget contains columns stuff
    auto editor_columns_layout = qtt::new_layout<QGridLayout>();
    editor_columns_widget->setLayout(editor_columns_layout);
    editor_columns_layout->setColumnStretch(0,0);
    editor_columns_layout->setColumnStretch(1,1);

    //editor_action contains buttons
    QWidget* editor_button_widget = nullptr;
    qtt::cstr_in_layout(editor_button_widget,editor_layout);
    auto editor_button_layout = qtt::new_layout<QHBoxLayout>();
    editor_button_widget->setLayout(editor_button_layout);

    qtt::cstr_in_layout(button_delete,editor_button_layout);
    qtt::cstr_in_layout(button_modify,editor_button_layout);
    qtt::cstr_in_layout(button_create,editor_button_layout);

    //--- populate columns ---

    QStringList horizontal_headers;
    horizontal_headers.append("rowid");

    for(size_t col_index = 0; col_index < col_vect.size(); ++col_index){
        auto& col = *col_vect[col_index];

        //populate in editor
        QLabel * editor_label = nullptr;
        qtt::cstr_in_grid(editor_label,editor_columns_layout,col_index,0);
        editor_label->setText   ( QString::fromStdString(col.column_name) );
        editor_label->setToolTip( QString::fromStdString(col.description) );
        //set_label(editor_label,col);

        auto editor_edit = col.new_widget();
        editor_columns_layout->addWidget(editor_edit,col_index,1);
        widget_vect.push_back(editor_edit);

        horizontal_headers.append(QString::fromStdString(col.column_name));
        //auto it = new QTableWidgetItem(1000);
        //it->setFlags( it->flags()   & ~Qt::ItemIsEditable );
        //table_widget->setItem(0, col_index+1 , it );
    }
    table_widget->setHorizontalHeaderLabels(horizontal_headers);

    //--- TODO populate lines (read from database) ---

    //--- TODO create and prepare delete, edit, create queries ---
    //TODO loop bind ???
    //TODO variants to sql ???

    //--- connect ---
    connect(this->button_create,&QPushButton::clicked,this,&TableWidget::on_create);
    connect(this->button_modify,&QPushButton::clicked,this,&TableWidget::on_modify);
    connect(this->button_delete,&QPushButton::clicked,this,&TableWidget::on_delete);

    //--- queries ---
    q_newline.prepare(db,"insert into \""+table_name+"\" (rowid) VALUES (NULL)");
    q_delline.prepare(db,"delete from \""+table_name+"\" where rowid=?");


    //---load content from the database in the table ---
    //rowid
    tdb::Fn_foreach<
        tdb::Tag_sqlite,
        std::tuple< typename tdb::Rowid_t<tdb::Tag_sqlite>::type  >,
        std::tuple<>,
        false
    > fn_rowid;

    int line_index=0;
    fn_rowid.prepare(db,"SELECT rowid  from \""+ table_name +"\" ");
    fn_rowid( [&line_index,this](tdb::Rowid_t<tdb::Tag_sqlite>::type rowid){
        table_widget->setRowCount(line_index+1);
        table_widget->setItem(line_index,0,  new_item( rowid ) );
        ++line_index;
    } );

    //other columns
    /*
    for(size_t i=0; i < col_size(); ++i){
        int line_index=0;
        col_vect[i]->db_read_all(db,name,[&](std::optional<std::string>  s){
            if( s.has_value() ){ table_widget->setItem(line_index,i+1, new_item_str(s.value() )  );}
            ++line_index;
        });
    }*/
    for(int lin_i=0; lin_i < table_widget->rowCount(); ++lin_i){
        Rowid rowid = line_rowid(lin_i);
        for(size_t col_i=0; col_i < col_size(); ++col_i){
            QTableWidgetItem * item = col_vect[col_i]->new_db_read(rowid);
            table_widget->setItem(lin_i,col_i+1,item);
        }
    }


    //--onEnter --
    //for all widgets except the last one : set next widget focus
    for(size_t i=1; i < col_size(); ++i){
        //widget_vect[i-1]->next_widget =widget_vect[i] ;
        auto w = widget_vect[i];
        widget_vect[i-1]->onEnter=[w](){w->setFocus();};
    }

    //for last widget : add
    if(widget_vect.size()>0){
        widget_vect.back()->onEnter=[this](){
            if     (this->mode==ADD) {on_create();}
            //else if(this->mode==EDIT){on_modify();}
        };
    }


    table_widget->setSortingEnabled(true);
    table_widget->sortByColumn(0,Qt::DescendingOrder);


    //--- connect ---
    connect(table_widget,&QTableWidget::cellClicked, this, &TableWidget::on_selectCell );
    connect(radio_add,   &QRadioButton::clicked ,    this, &TableWidget::on_mode_change);
    connect(radio_edit,  &QRadioButton::clicked ,    this, &TableWidget::on_mode_change);

    //radio_add->setChecked(true);

    set_mode_add();

    translate();
}



void TableWidget::on_mode_change(){
   if(radio_edit->isChecked()){ set_mode_edit(); }
   else{                        set_mode_add();  }
}

bool TableWidget::can_edit()const{
    return table_widget->rowCount()>0;
}



void TableWidget::set_mode_edit(){
    //once
    if(mode==EDIT){return;}
    mode=EDIT;

    if(!can_edit()){
        set_mode_add();
        return;
    }


    //radio
    radio_add ->setChecked(false);
    radio_edit->setChecked(true);

    //buttons
    button_delete->show();
    button_modify->show();
    button_create->hide();

    auto line_index = table_widget->currentRow();

    //no row selected, fallback to first row
    if(line_index<0){
        if(table_widget->rowCount()>0){
            //auto index = table_widget->model()->index(0, 0);
            table_widget->clearSelection();
            select_cell(table_widget,0,0);
            on_selectCell(0,0);
        }else{
            //empty table
            table_widget->clearSelection();
            table_widget->setCurrentCell(-1,-1);
            set_mode_add();
            return;
        }
    }

    update_editor_title();
}

void TableWidget::set_mode_add(){
    //once
    if(mode==ADD){return;}
    mode=ADD;

    //radio
    radio_add ->setChecked(true);
    radio_edit->setChecked(false);

    //buttons
    button_delete->hide();
    button_modify->hide();
    button_create->show();
    table_widget->clearSelection();

    update_editor_title();
}



TableWidget::Rowid TableWidget::line_rowid(int row){
    QString rowid_str = table_widget->item(row,0)->text();
    Rowid   rowid     = convert<Rowid>(rowid_str);
    return  rowid;
}

void TableWidget::update_editor_title(){
    //auto selection = table_widget->selectedItems();

    int line=table_widget->currentRow();

    //if an item is selected
    if(line>=0){
        //int line     = selection.front()->row();
        Rowid rowid  = line_rowid(line);

        if(mode==ADD){
           editor_title->setText(tr("Cloned from rowid:%1").arg(rowid));
        }
        else if(mode==EDIT){
           editor_title->setText(tr("Edit rowid:%1").arg(rowid));
        }else{
           editor_title->setText("?a?");
        }
    }else{
        if(mode==ADD){
           editor_title->setText(tr("New line"));
        }else if(mode==EDIT){
            editor_title->setText(tr("No line selected"));
        }else{
            editor_title->setText("?b?");
        }

    }
}


void TableWidget::on_selectCell(int row,int column){
    //mode add => do nothing
    //if(radio_add->isChecked()){return;}

    //std::cout << "on_selectCell" << row << " " << column << std::endl;

    Rowid   rowid     = line_rowid(row);

    //column0 in table is rowid, which is not editable
    //column 1+ in table matches widget_vect 0+
    if(column>0){
        widget_vect.at(column-1)->setFocus();
    }else{
        if(widget_vect.size()>0){ widget_vect.at(0)->setFocus();}
    }

    //load line in widgets
    for(size_t i =0; i< col_size(); ++i){
        widget_vect[i]->db_read(rowid);
    }

    update_editor_title();
}

void TableWidget::disable(QPushButton *b){
    b->setEnabled(false);
    b->setStyleSheet("QPushButton{color: grey;}");
}

void TableWidget::enable(QPushButton *b){
    b->setEnabled(true);
    b->setStyleSheet("");
}



/*
void TableWidget::set_mode_add(){
    if(mode==ADD){return;}

    mode=ADD;

    button_delete->hide();
    button_modify->hide();
    button_create->show();

    table_widget->clearSelection();

    update_editor_title();
    //table_widget->setSelectionMode(QAbstractItemView::SelectionMode::NoSelection);
}

*/

/*
void TableWidget::set_mode_edit(){
    if(mode==EDIT){return;}
    mode=EDIT;




    button_delete->show();
    button_modify->show();
    button_create->hide();

    auto line_index = table_widget->currentRow();
    auto col_index = table_widget->currentColumn();

    std::cout <<"index "<<  line_index <<" "<<col_index<<std::endl;

    //no row selected, fallback to first row
    if(line_index<0){
        if(table_widget->rowCount()>0){
            auto index = table_widget->model()->index(0, 0);
            table_widget->clearSelection();
            table_widget->setCurrentCell(0,0);
            table_widget->selectRow(0);
            on_selectCell(0,0);
        }else{
            //empty table
            table_widget->clearSelection();
            table_widget->setCurrentCell(-1,-1);
            set_mode_add();
            return;
        }
    }

    update_editor_title();
}
*/

void TableWidget::on_delete(){
    //get the rowid
    auto line_index = table_widget->currentRow();
    auto rowid_item = table_widget->item(line_index,0);

    if(rowid_item==nullptr){
        return;
    }

    Rowid rowid     = convert<Rowid>(rowid_item->text() );

    //delete in the database
    q_delline(rowid);

    //delete in the table
    table_widget->removeRow(line_index);

    if(can_edit()){
        int select_me = line_index;
        if(select_me>=table_widget->rowCount()){select_me=table_widget->rowCount()-1;}
        select_cell(table_widget,select_me,0);
        on_selectCell(select_me,0);
        update_editor_title();
    }else{
        set_mode_add();
        for(auto w : widget_vect){
            w->setDefault();
        }
    }

}

void TableWidget::on_modify(){
  //get the rowid
  auto line_index = table_widget->currentRow();
  auto rowid_item =  table_widget->item(line_index,0);
  Rowid rowid     = convert<Rowid>(rowid_item->text() );

  //set the database
  auto tr = tdb::transaction(db);
  for(size_t i=0; i < col_size(); ++i ){
      const std::string &col_name = col_vect[i]->column_name;
      widget_vect[i]->db_write(rowid);
  }
  tr.commit();

  //update table from database content
  for(size_t i=0; i < col_size(); ++i){
      //QString text = col_vect[i]->db_read(db,name,rowid);
      //std::unique_ptr<QTableWidgetItem> it= std::make_unique<QTableWidgetItem>();
      table_widget->setItem(line_index,i+1, col_vect[i]->new_db_read(rowid) );
      line_index=table_widget->indexFromItem(rowid_item).row(); //see (1)
  }
  //(1) : IMPORTANT: as the table may be sorted, the newly added item, or the item after edition, may appears on a different row.
  //we, therefore need to update the line index



}

void TableWidget::on_create(){

    auto tr = tdb::transaction(db);
    auto rowid = q_newline();

    //set database content
    for(size_t i=0; i < col_size(); ++i ){
        //const std::string &col_name = col_vect[i]->column_name;
        widget_vect[i]->db_write(rowid);
    }
    tr.commit();


    //create a new line in table_widget
    auto line_index = table_widget->rowCount();
    table_widget->setRowCount(line_index+1);
    auto rowid_item = new_item(rowid);
    table_widget->setItem(line_index,0, rowid_item );
    line_index=table_widget->indexFromItem(rowid_item).row(); //see (1)


    //get database content, write it in the new line
    for(size_t i=0; i < col_size(); ++i){
        //QString text = col_vect[i]->db_read(db,name,rowid);
        QTableWidgetItem * item = col_vect[i]->new_db_read(rowid);
        table_widget->setItem(line_index,i+1, item);
        line_index=table_widget->indexFromItem(rowid_item).row(); //see (1)
    }

    //(1) : IMPORTANT: as the table may be sorted, the newly added item, or the item after edition, may appears on a different row.
    //we, therefore need to update the line index

    set_mode_add();
    table_widget->scrollToItem(rowid_item);
    table_widget->clearSelection() ;

    //set the widgets to their default values
    for(size_t i=0; i < col_size(); ++i){
        widget_vect[i]->setDefault();
    }

    //if at least one widget, set focus
    if(col_size()>0){
        widget_vect[0]->setFocus();
    }




}



std::string TableWidget::sql_create()const{
    bool is_first=true;
    std::string s =  "CREATE TABLE IF NOT EXISTS \""+table_name+"\"(\n";
    for(const auto &c : col_vect){
        if(is_first)  {is_first=false; s+="   ";}
        else[[likely]]{s+="\n  ,";}
        s+=c->sql_create();
    }
    s+="\n);";
    return s;
}


void TableWidget::save_table(const QString &path){
    std::ofstream out(path.toStdString().c_str() );

    if(!out){
        throw std::runtime_error(tr("Cannot write to file %1").arg(path).toStdString());
    }

    //header
    {
        bool is_first=true;
        for(const auto &col :col_vect ){
            if(is_first){is_first=false;}
            else{out << "\t";}
            out<<col->save_header();
        }
        out<<"\n";
    }

    tdb::Fn_foreach<tdb::Tag_sqlite, std::tuple<Rowid_t>, std::tuple<>, false> q_rowid;
    q_rowid.prepare(db,"select rowid from \""+table_name+"\"");

    auto save_data=[&](Rowid_t rowid){
        bool is_first=true;
        for(const auto &col :col_vect ){
            if(is_first){is_first=false;}
            else{out << "\t";}
            out<<col->save_data(rowid);//warning query within query
        }
        out<<"\n";
    };

    q_rowid(save_data);

}

