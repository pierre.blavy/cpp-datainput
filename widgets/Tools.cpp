#include "Tools.hpp"

#include <config/config.hpp>
#include <config/convert/convert_QString.hpp>

#include <convert/QDateTime_to_QString.hpp>
#include <convert/from_QString.hpp>



#include <qtt/qtt_layout.hpp>

#include <widgets/IsOk.hpp>

#include <QLabel>
#include <QLineEdit>
#include <QPlainTextEdit>
#include <QPushButton>
#include <QTimeZone>
#include <QTextStream>
#include <QApplication>
#include <QClipboard>

Tool_datetime::Tool_datetime(const config::Block &conf, QWidget* parent):QWidget(parent){
    auto main_layout = new QGridLayout;
    this->setLayout(main_layout);

    //config
    qtt::cstr_in_grid_span(input_title       ,main_layout,1,1,1,3);

    qtt::cstr_in_grid     (input_format_label,main_layout,2,1);
    qtt::cstr_in_grid     (input_format_edit ,main_layout,2,2);
    qtt::cstr_in_grid     (input_format_isok ,main_layout,2,3);

    qtt::cstr_in_grid     (input_timezone_label,main_layout,3,1);
    qtt::cstr_in_grid     (input_timezone_edit ,main_layout,3,2);
    qtt::cstr_in_grid     (input_timezone_isok ,main_layout,3,3);

    qtt::cstr_in_grid_span(output_title       ,main_layout,4,1,1,3);

    qtt::cstr_in_grid     (output_format_label,main_layout,5,1);
    qtt::cstr_in_grid     (output_format_edit ,main_layout,5,2);
    qtt::cstr_in_grid     (output_format_isok ,main_layout,5,3);

    qtt::cstr_in_grid     (output_timezone_label,main_layout,6,1);
    qtt::cstr_in_grid     (output_timezone_edit ,main_layout,6,2);
    qtt::cstr_in_grid     (output_timezone_isok ,main_layout,6,3);

    qtt::cstr_in_grid     (output_error_label,main_layout,7,1);
    qtt::cstr_in_grid     (output_error_edit ,main_layout,7,2);

    //input data
    qtt::cstr_in_grid_span(input_data_label  ,main_layout,8,1,1,3);
    qtt::cstr_in_grid_span(input_data        ,main_layout,9,1,1,3);

    qtt::cstr_in_grid_span(run_button        ,main_layout,10,1,1,3);


    //output data
    qtt::cstr_in_grid_span(output_data_label  ,main_layout,11,1,1,3);

    qtt::cstr_in_grid     (output_stats_ok_label  ,main_layout,12,1);
    qtt::cstr_in_grid_span(output_stats_ok        ,main_layout,12,2,1,2);

    qtt::cstr_in_grid     (output_stats_error_label  ,main_layout,13,1);
    qtt::cstr_in_grid_span(output_stats_error        ,main_layout,13,2,1,2);

    qtt::cstr_in_grid     (output_stats_empty_label  ,main_layout,14,1);
    qtt::cstr_in_grid_span(output_stats_empty        ,main_layout,14,2,1,2);

    qtt::cstr_in_grid_span(output_data        ,main_layout,15,1,1,3);

    output_data        ->setReadOnly(true);
    output_stats_ok    ->setReadOnly(true);
    output_stats_error ->setReadOnly(true);
    output_stats_empty ->setReadOnly(true);

    //connect
    connect(run_button,&QPushButton::clicked, this, &Tool_datetime::on_run );

    connect( input_format_edit ,&QLineEdit::textChanged,this,[&](){check_format( input_format_edit, input_format_isok,  input_oldtz,  input_timezone_edit)  ;});
    connect(output_format_edit ,&QLineEdit::textChanged,this,[&](){check_format(output_format_edit,output_format_isok, output_oldtz, output_timezone_edit);});

    connect( input_timezone_edit ,&QLineEdit::textChanged,this,[&](){check_timezone( input_timezone_edit, input_timezone_isok,  input_format_edit);});
    connect(output_timezone_edit ,&QLineEdit::textChanged,this,[&](){check_timezone(output_timezone_edit,output_timezone_isok, output_format_edit);});

    connect(input_data,&QPlainTextEdit::textChanged,this,[&](){output_stats_ok->setText(""); output_stats_error->setText("");output_stats_empty->setText("");});

    //configure
    input_format_edit ->setText(conf.values.get_unique<QString>("in_format"));
    output_format_edit->setText(conf.values.get_unique<QString>("out_format"));

    input_timezone_edit ->setText(conf.values.get_unique<QString>("in_timezone"));
    output_timezone_edit->setText(conf.values.get_unique<QString>("out_timezone"));

    output_error_edit->setText(conf.values.get_unique<QString>("out_error"));


    Tool_datetime::translate(false);
}

void Tool_datetime::translate(bool){


    //https://doc.qt.io/qt-6/qdatetime.html#toString
    QString str_format    = tr("Format");
    QString str_format_tt = "<u>"+tr("Timestamp")+"</u><br>"
                            "<b>"+tr("seconds")+"</b> : timestamp_s<br>"
                            "<b>"+tr("milliseconds")+"</b> : timestamp_ms<br>"
                            "<br>"
                            "<u>"+tr("Dates and time")+":</u><br>"
                            "<b>"+tr("year")+"</b> : yyyy<br>"
                            "<b>"+tr("month")+"</b> : MM<br>"
                            "<b>"+tr("day")+"</b> : dd<br>"
                            "<b>"+tr("hours")+"</b> : hh<br>"
                            "<b>"+tr("minutes")+"</b> : mm<br>"
                            "<b>"+tr("seconds")+"</b> : ss<br>"
                            "<b>"+tr("milliseconds")+"</b> : z";

    QString str_timezone    = tr("Timezone");
    QString str_timezone_tt = "";


    input_title ->setText("<b>"+tr("Input config")+"</b>");
    output_title->setText("<b>"+tr("Output config")+"</b>");

    input_format_label ->setText(str_format); input_format_label ->setToolTip(str_format_tt);
    output_format_label->setText(str_format); output_format_label->setToolTip(str_format_tt);

    input_timezone_label ->setText(str_timezone); input_timezone_label ->setToolTip(str_timezone_tt);
    output_timezone_label->setText(str_timezone); output_timezone_label->setToolTip(str_timezone_tt);

    output_error_label->setText(tr("Error_string"));
    output_error_label->setToolTip(tr("Output this string when the input is invalid"));

    input_data_label ->setText("<b>"+tr("Input data")+"</b>");
    output_data_label->setText("<b>"+tr("Output data")+"</b>");

    output_stats_ok_label->setText(tr("Ok"));
    output_stats_error_label->setText(tr("Errors"));
    output_stats_empty_label->setText(tr("Empy"));

    output_stats_ok_label->setToolTip(tr("Number of lines correctly converted"));
    output_stats_error_label->setToolTip(tr("Number of lines with an error"));
    output_stats_empty_label->setToolTip(tr("Number of empty lines"));

    run_button->setText(tr("Convert and copy"));
}

void Tool_datetime::on_run(){
    output_data->setPlainText("");
    output_stats_ok    ->setText("");
    output_stats_error->setText("");
    output_stats_empty ->setText("");

    size_t n_ok=0;
    size_t n_error=0;
    size_t n_empty=0;



    QString text = input_data->toPlainText();
    QTextStream stream(&text);

    auto parse_in  = mk_input_parser();
    auto write_out = mk_output_writer();

    QString line;
    while (stream.readLineInto(&line)) {
        line=line.trimmed();

        //write empty lines
        if(line==""){
            output_data->insertPlainText("\n");
            ++n_empty;
            continue;
        }

        QDateTime dt=parse_in(line);

        //write invalid datetimes as errors
        if(!dt.isValid()){
           output_data->insertPlainText(output_error_edit->text()+"\n");
           ++n_error;
           continue;
        }

        //write valid datetimes
        QString tmp = write_out(dt);//dt must be valid here
        output_data->insertPlainText(tmp+"\n");
        ++n_ok;

    }

    //Write errors count
    output_stats_ok    ->setText(convert<QString>(n_ok) );
    output_stats_error->setText(convert<QString>(n_error) );
    output_stats_empty ->setText(convert<QString>(n_empty) );

    //copy to clipboard
    QClipboard *clipboard = QApplication::clipboard();
    clipboard->setText(output_data->toPlainText());



}

void Tool_datetime::check_format(QLineEdit* edit, IsOk* isok, QString& oldtz, QLineEdit* tz_edit){
    QString str = edit->text().trimmed();


    //timestamp
    bool is_ts= false;
    if(str=="timestamp_s"){
        isok->set_ok();
        edit->setToolTip(tr("Timestamp in <b>seconds</b>"));
        is_ts=true;
    }

    if(str=="timestamp_ms"){
        isok->set_ok();
        edit->setToolTip(tr("Timestamp in </b>milliseconds</b>"));
        is_ts=true;
    }

    if(is_ts){
        if(tz_edit->isEnabled()){
            oldtz=tz_edit->text();
            tz_edit->setEnabled(false);
            tz_edit->setText(tr("unused"));
        }
        return;
    }

    if(!tz_edit->isEnabled()){
        tz_edit->setText(oldtz);
        tz_edit->setEnabled(true);
    }

    //empty => error
    edit->setToolTip("");
    if(str==""){
        isok->set_error(tr("Empty string"));
        return;
    }



    bool has_year    = str.contains("yyyy");
    bool has_month   = str.contains("MM");
    bool has_day     = str.contains("dd");
    bool has_hours   = str.contains("hh");
    bool has_minutes = str.contains("mm");
    bool has_seconds = str.contains("ss");
    bool has_ms      = str.contains("z");

    isok->set_ok();

    if(!has_year and !has_month and !has_day and !has_hours and !has_minutes and !has_seconds and !has_ms){
        isok->set_warning(tr("Nothing related to a date"));
        return;
    }

    if(has_year and !has_month and has_day){
        isok->set_warning(tr("Strange format, use MM for month, not mm"));
        return;
    }


    if(str.contains("zz")){
        isok->set_warning(tr("Strange format, use z for milliseconds, not zz or zzz"));
        return;
    }

    if(str.contains("ms")){
        isok->set_warning(tr("Strange format, use z for milliseconds, not ms"));
        return;
    }


    QString yes_str = "<b>" + tr("yes") + "</b>";
    QString no_str  = tr("no");
    auto bstr=[&](bool b)->const QString&{
        return b?yes_str:no_str;
    };

    edit->setToolTip(
        "<u>"+tr("Datetime")+"</u><br>"+
        tr("Year")+" (yyyy) : "+bstr(has_year) + "<br>" +
        tr("Month")+" (MM) : "+bstr(has_month) + "<br>" +
        tr("Day")+" (dd) : "+bstr(has_day) + "<br>" +
        tr("Hours")+" (hh) : "+bstr(has_hours) + "<br>" +
        tr("Minutes")+" (mm) : "+bstr(has_minutes) + "<br>" +
        tr("Seconds")+" (ss) : "+bstr(has_seconds) + "<br>" +
        tr("Milliseconds")+" (z) : "+bstr(has_ms)
    );

}

void Tool_datetime::check_timezone(QLineEdit* edit, IsOk* isok, QLineEdit* format){

    //timestamp => ignore
    if(format->text()=="timestamp_s" or format->text()=="timestamp_ms"){
        isok->set_ok();
        edit->setToolTip("");
        return;
    }

    //check if timezone is valid
    QString   str=edit->text().trimmed();
    QTimeZone tz=QTimeZone(str.toLocal8Bit()) ;

    if(tz.isValid()){
        isok->set_ok();
        edit->setToolTip( convert<QString>(tz) );
        return;
    }else{
        isok->set_error(tr("Invalid timezone"));
        edit->setToolTip("" );
        return;
    }

}


std::function< QDateTime(const QString &)   > Tool_datetime::mk_input_parser(){

    if(input_format_edit->text()=="timestamp_s"){
        return [](const QString &s)->QDateTime{
            try{
                return QDateTime::fromSecsSinceEpoch(convert<qint64>(s));
            }catch(...){
                return QDateTime();
            }
        };
    }

    if(input_format_edit->text()=="timestamp_ms"){
        return [](const QString &s)->QDateTime{
            try{
                return QDateTime::fromMSecsSinceEpoch(convert<qint64>(s));
            }catch(...){
                return QDateTime();
            }
        };
    }

    QString format=input_format_edit->text();
    QTimeZone tz = QTimeZone(input_timezone_edit->text().toLocal8Bit());
    return [format,tz](const QString &s)->QDateTime{
        auto dt = QDateTime::fromString(s,format);
        dt.setTimeZone(tz);
        return dt;
    };


}


std::function< QString  (const QDateTime &) > Tool_datetime::mk_output_writer(){
    //dt is assumed to be valid

    if(output_format_edit->text()=="timestamp_s"){
        return [](const QDateTime &dt)->QString{
            return convert<QString>(dt.toSecsSinceEpoch());
        };
    }

    if(output_format_edit->text()=="timestamp_ms"){
        return [](const QDateTime &dt)->QString{
            return convert<QString>(dt.toMSecsSinceEpoch());
        };
    }

    QString format=output_format_edit->text();
    QTimeZone tz = QTimeZone(output_timezone_edit->text().toLocal8Bit());
    return [format,tz](const QDateTime &dt)->QString{
        auto dt2=QDateTime::fromMSecsSinceEpoch( dt.toMSecsSinceEpoch() ,tz);
        return dt2.toString(format);
    };



}


/*
QDateTime Tool_datetime::from_input(const QString &s,  const QTimeZone &tz){
    //QString   str = input_timezone_edit->text().trimmed();
    //QTimeZone tz  = QTimeZone(str.toLocal8Bit() );


    QDateTime dt  = QDateTime::fromString(s,input_format_edit->text() );
    dt.setTimeZone(tz);
    return dt;
}
*/

Tools::Tools(const config::Block &conf, QWidget* parent):QWidget(parent){
    auto main_layout = new QHBoxLayout;
    this->setLayout(main_layout);

    qtt::cstr_in_layout(tabs,main_layout);

    tool_datetime=new Tool_datetime(conf.blocks.get_unique("datetime"));
    tool_datetime_i = tabs->addTab(tool_datetime,"<Datetime>");

    Tools::translate(false);
}

void Tools::translate(bool tr_sons){
    tabs->setTabText(tool_datetime_i,tr("Datetime"));

    if(tr_sons){
        tool_datetime->translate();
    }
}
