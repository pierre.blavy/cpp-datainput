#ifndef SAVEDATA_HPP
#define SAVEDATA_HPP

#include <QWidget>
#include <qtt/qtt_Translatable.hpp>

#include <tdb/tdb_sqlite.hpp>

class QCheckBox;
class QLabel;
class QPushButton;
class Filepath;

namespace config{class Block;}


class SaveData:public QWidget, qtt::Translatable{
    Q_OBJECT
    public:

    explicit SaveData(tdb::Connection_t<tdb::Tag_sqlite> &db_, const config::Block &conf, QWidget* parent = nullptr);
    void translate(bool tr_sons=true)override final;

    signals:
    void saveClicked(QString path, QStringList table_names);


    private:
    //file config
    QLabel   * folder_label=nullptr;
    Filepath * folder_edit=nullptr;


    //tables
    QLabel      * tables_label=nullptr;
    std::vector<QCheckBox*> table_checked;//not owned
    size_t ntables=0;//number of tables checked
    tdb::Connection_t<tdb::Tag_sqlite> &db;


    void on_table_check(bool checked, const std::string &table_name);
    void on_save_click();
    void update_ntables();

     QPushButton * save_button=nullptr;


};



#endif // SAVEDATA_HPP
