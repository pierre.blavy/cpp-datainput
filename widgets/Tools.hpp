#ifndef TOOLS_HPP
#define TOOLS_HPP

#include <QWidget>
#include <QDateTime>

#include <qtt/qtt_Translatable.hpp>

namespace config{
class Block;
}

class QTabWidget;
class QLabel;
class QLineEdit;
class IsOk;
class QPlainTextEdit;
class QPushButton;




class Tool_datetime:public QWidget, qtt::Translatable{
    Q_OBJECT

    public:
    Tool_datetime(const config::Block &conf, QWidget* parent=nullptr);
    void translate(bool tr_sons=true)override;

    private:
    QLabel*    input_title = nullptr;

    QLabel*    input_format_label = nullptr;
    QLineEdit* input_format_edit  = nullptr;
    IsOk*      input_format_isok  = nullptr;

    QLabel*    input_timezone_label = nullptr;
    QLineEdit* input_timezone_edit  = nullptr;
    IsOk*      input_timezone_isok  = nullptr;

    QLabel*    output_title = nullptr;

    QLabel*    output_format_label = nullptr;
    QLineEdit* output_format_edit  = nullptr;
    IsOk*      output_format_isok  = nullptr;

    QLabel*    output_timezone_label = nullptr;
    QLineEdit* output_timezone_edit  = nullptr;
    IsOk*      output_timezone_isok  = nullptr;

    QLabel*    output_error_label = nullptr;
    QLineEdit* output_error_edit  = nullptr;

    QLabel*    output_stats_ok_label = nullptr;
    QLineEdit* output_stats_ok       = nullptr;

    QLabel*    output_stats_error_label = nullptr;
    QLineEdit* output_stats_error       = nullptr;

    QLabel*    output_stats_empty_label = nullptr;
    QLineEdit* output_stats_empty       = nullptr;


    QLabel*         input_data_label = nullptr;
    QPlainTextEdit* input_data  = nullptr;

    QPushButton*    run_button = nullptr;

    QLabel*         output_data_label = nullptr;
    QPlainTextEdit* output_data       = nullptr;

    void on_run();
    void check_format  (QLineEdit* edit, IsOk* isok, QString& oldtz, QLineEdit* tz_edit);
    void check_timezone(QLineEdit* edit, IsOk* isok, QLineEdit* format);


    std::function< QDateTime(const QString &)   > mk_input_parser();
    std::function< QString  (const QDateTime &) > mk_output_writer(); //the datetime passed to the writer is assumed to be valid. Caller must enforce that


    //store the old timezones, used to rollback to them when format changes from timestamp to a datetime format
    QString input_oldtz;
    QString output_oldtz;

};

class Tools:public QWidget, qtt::Translatable{
    Q_OBJECT

    public:
    Tools(const config::Block &conf, QWidget* parent=nullptr);
    void translate(bool tr_sons=true)override;

    private:
    QTabWidget * tabs = nullptr;

    Tool_datetime* tool_datetime=nullptr;
    int tool_datetime_i = -1;


};


#endif // TOOLS_HPP
