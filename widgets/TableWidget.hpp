#ifndef TABLEWIDGET_PIERRE_HPP
#define TABLEWIDGET_PIERRE_HPP

#include "columns/Column_abstract.hpp"


#include <QWidget>
#include <vector>
#include <qtt/qtt_Translatable.hpp>

#include <tdb/tdb_sqlite.hpp>
#include <tdb/functors/all.hpp>


namespace config{class Block;}

class QTableWidget;
class QWidget;
class QLabel;
class QPushButton;
class QRadioButton;


class TableWidget:public QWidget, qtt::Translatable{
    Q_OBJECT;



    public:
    typedef tdb::Rowid<tdb::Tag_sqlite> Rowid_t;
    explicit TableWidget(const config::Block &conf, tdb::Connection_t<tdb::Tag_sqlite> &db_, QWidget* parent = nullptr);

    //std::string sql_select_all()const;

    std::string table_name;
    std::string description;


    void translate(bool tr_sons=true)override final;


    QLabel *      editor_title = nullptr;
    QTableWidget* table_widget = nullptr;

    QLabel *       radio_label= nullptr;
    QRadioButton * radio_add  = nullptr;
    QRadioButton * radio_edit = nullptr;

    QPushButton * button_delete = nullptr;
    QPushButton * button_modify = nullptr;
    QPushButton * button_create = nullptr;

    //--- columns --
    std::vector<Column_ptr>     col_vect;    //Columns
    std::vector<Column_widget*> widget_vect; //Column widgets for editing values


    size_t col_size()const{return col_vect.size();} //both col_vect and widget_vect have the same size

    void save_table(const QString &path);

    private:
    typedef tdb::Rowid<tdb::Tag_sqlite> Rowid;

    bool can_edit()const;
    void set_mode_edit();
    void set_mode_add();

    void on_delete();
    void on_modify();
    void on_create();

    void on_selectCell(int row, int col);

    Rowid line_rowid(int row);

    //--- helpers ---
    void on_mode_change();

    enum Mode_e{ADD,EDIT,UNDEF};
    Mode_e mode=UNDEF;

    //void set_mode_add();
    //void set_mode_edit();

    void update_editor_title();

    static void disable(QPushButton *b);
    static void enable (QPushButton *b);



    //--- queries---
    tdb::Connection_t<tdb::Tag_sqlite> &db;
    tdb::Fn_insert <tdb::Tag_sqlite,std::tuple<>,std::tuple<>     ,false> q_newline;
    tdb::Fn_execute<tdb::Tag_sqlite,std::tuple<>,std::tuple<Rowid>,false> q_delline;


private:
    std::string sql_create()const;

};


#endif
