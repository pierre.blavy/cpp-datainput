#include "Column_float.hpp"
#include <QLineEdit>
#include <QPushButton>
#include <widgets/IsOk.hpp>



#include <qtt/qtt_layout.hpp>

#include <convert/to_QString.hpp>
#include <convert/to_string.hpp>

#include <QMessageBox>

#include <config/config.hpp>
#include <config/convert/convert_lexical.hpp>
#include <widgets/IntValidator_t.hpp>
#include <QTableWidgetItem>


Column_float_widget::Column_float_widget(
    const Column_float &col_,
    QWidget * parent
  ):
    Column_widget(parent),
    col(col_)
{

    //--- construct widgets ---
    auto main_layout = qtt::new_layout<QHBoxLayout>();
    this->setLayout(main_layout);
    qtt::cstr_in_layout(value_edit,main_layout);
    qtt::cstr_in_layout(nan_button,main_layout);
    qtt::cstr_in_layout(isok     ,main_layout);


    //--- widget params ---
    value_edit->setSizePolicy(QSizePolicy::Expanding,QSizePolicy::Minimum );

    nan_button->setSizePolicy(QSizePolicy::Minimum,QSizePolicy::Minimum );
    nan_button->setStyleSheet ("padding:3px;");

    //--- connect and translate ---
    connect(nan_button,&QPushButton::clicked  ,this,&Column_float_widget::setNan);
    connect(value_edit,&QLineEdit::textChanged,this,&Column_float_widget::on_change);

    translate(false);

    setDefault();

}

void Column_float_widget::translate(bool){
    nan_button->setText( tr("NA") );
    nan_button->setToolTip( tr("Set to missing value"));

    QString tt=tr("Float value");
    if(col.min_v.has_value()){
        if(tt!=""){tt+="\n";}
        tt+=(tr("Minimum = %1").arg(col.min_v.value()));
    }
    if(col.max_v.has_value()){
        if(tt!=""){tt+="\n";}
        tt+=(tr("Maximum = %1").arg(col.max_v.value()));
    }
    if(!col.min_v.has_value() and !col.max_v.has_value() ){
        tt=tr("Any float");
    }
    value_edit->setToolTip(tt);

    err_title=tr("Error : wrong value");
    err_message=tr("Wrong value : %1");
}


void Column_float_widget::on_change(){

    auto compute_new_value=[this](const QString &text)->value_type{

        if(text=="")   {
            return value_type::error(tr("Empty text, please use NA for missing values","Don't translate NA, it's a magic value"));
        }

        if(text=="na" or text=="nan"){
            return value_type::nan();
        }

        numeric_type v;
        try{
            v = convert<numeric_type>(text);
        }catch(...){
            return value_type::error(tr("Wrong value : the string %1 cannot be converted to float.").arg(text) );
        }

        bool ok=true;
        QString err="";
        if(col.min_v.has_value() and v<col.min_v.value()  ){
            ok =false;
            err+=tr("The value is too small. Value=%1, min=%2.").arg(v).arg(col.min_v.value());
        }

        if(col.max_v.has_value() and v>col.max_v.value()  ){
            ok =false;
            if(err!=""){err+="\n";}
            err+=tr("The value is too large. Value=%1, max=%2.").arg(v).arg(col.max_v.value());
        }

        if(!ok){return value_type::error(err);}

        return value_type::value(v);
    };

    QString text=value_edit->text().trimmed().toLower();
    value_type new_value = compute_new_value(text);
    if(new_value != m_value){
        m_value = new_value;
        update_isok();
        emit changed();
    }
}



void Column_float_widget::update_isok(){
    if(m_value.is_value()){isok->set_ok(); return;}
    if(m_value.is_nan())  {isok->set_warning(tr("Value is NA"));return;}
    if(m_value.is_error()){isok->set_error(m_value.get_error());return;}
}


void Column_float_widget::setFocus(){
    value_edit->setFocus();
}

void Column_float_widget::setDefault(){
    if(col.default_v.is_empty()){value_edit->setText("");}
    else if(col.default_v.is_nan()){value_edit->setText("NA");}
    else {setValue(col.default_v.get_value());}
}


void Column_float_widget::setNan(){
    value_edit->setText("NA");
}


auto Column_float_widget::value()const->std::optional<numeric_type>{
    if(m_value.is_error()){
        QMessageBox msgBox(
            QMessageBox::Warning,
            err_title,
            err_message.arg(m_value.get_error())
            );
        msgBox.setStandardButtons(QMessageBox::Ok);
        msgBox.setDefaultButton  (QMessageBox::Ok);
        msgBox.exec();

        return std::nullopt;
    }else{
        return m_value.get_optional();
    }
}





void Column_float_widget::setValue( std::optional<numeric_type> opt_int ){
    m_value.set_optional(opt_int);

    if(m_value.is_nan())  {value_edit->setText("NA");}
    else if(m_value.is_value()){value_edit->setText(convert<QString>( m_value.get_value())  );}
    else{value_edit->setText("<ERROR>");}

    update_isok();

}




void Column_float_widget::db_write(tdb::Rowid<tdb::Tag_sqlite> rowid ){
    auto v=value();
    col.q_update(rowid,v);
}

void Column_float_widget::db_read(tdb::Rowid<tdb::Tag_sqlite> rowid )
{
    auto opt_value = col.q_read(rowid);
    setValue(opt_value);
}




/*
void Column_float::db_read (tdb::Connection_t<tdb::Tag_sqlite>&db, const std::string &table_name, tdb::Rowid<tdb::Tag_sqlite> rowid,QTableWidgetItem *write_here)const{

    if(write_here==nullptr){return;}

    auto opt_float = Column_widget::db_read<numeric_type>(db,table_name, this->column_name,rowid);


    if(opt_float.has_value()){write_here->setData(Qt::DisplayRole,opt_float.value());}
    else                     {write_here->setData(Qt::DisplayRole,QVariant());}

}




Column_widget_float * Column_float::new_widget(QWidget*parent){
  return new Column_widget_float( col_config, parent);
}
*/


Column_float_widget * Column_float::new_widget(QWidget*parent){
    return new Column_float_widget(*this, parent);
}


QTableWidgetItem*  Column_float::new_db_read(tdb::Rowid<tdb::Tag_sqlite> rowid)const{
    std::optional<numeric_type> opt = q_read(rowid);
    if(opt.has_value()){
        QTableWidgetItem * r = new QTableWidgetItem ( );
        r->setFlags( r->flags() &~ Qt::ItemIsEditable );
        r->setData(Qt::EditRole, opt.value() );
        return r;
    }else{
        return  new QTableWidgetItem ( );
    }
}


Column_float::Column_float(Db_t& db_,const config::Block&conf):
    Column_abstract(conf),
    db(db_)
{


    //--- read config ---
    if(conf.values.size("min")!=0 )    { min_v=conf.values.get_unique<numeric_type>("min"); }
    if(conf.values.size("max")!=0 )    { max_v=conf.values.get_unique<numeric_type>("max"); }
    if(conf.values.size("default")!=0 ){
        std::string s = conf.values.get_unique<std::string>("default");
        default_v.from_string<config::Config_tag>(s);
    }else{
        default_v.set_nan();
    }
}

std::string Column_float::save_data(Rowid_t rowid)const{
    auto v = q_read(rowid);
    if(v.has_value()){
        return std::to_string(v.value());
    }else{
        return "NA";
    }
}


void Column_float::prepare(){
    q_read  .prepare(db,"select \""+column_name+"\" from \""+table_name+"\" where rowid=?");
    q_update.prepare(db,"update \""+table_name+"\" set \""+column_name+"\"=?2 where rowid=?1");
}


std::string Column_float::sql_create()const{
    return "\""+column_name + "\" real";
}
