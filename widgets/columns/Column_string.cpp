#include "Column_string.hpp"

#include <qtt/qtt_layout.hpp>
#include <convert/to_QString.hpp>

#include <QLineEdit>
#include <QMessageBox>
#include <QSizePolicy>
#include <QCheckBox>
#include <QTableWidgetItem>

#include <QRegularExpressionValidator>

#include <config/config.hpp>
#include <config/convert/convert_lexical.hpp>
#include <widgets/IsOk.hpp>


#include <tdb/tdb_sqlite.hpp>



Column_string_validator::Column_string_validator(const Column_string & col_, const QRegularExpression &re, QObject *parent ):
  QRegularExpressionValidator(re,parent),col(col_)
{translate(false);}

void Column_string_validator::translate(bool){
     err_too_short=tr("String is too short, size=%1, min_size=%2.");
     err_too_long =tr("String is too long, size=%1, max_size=%2.");
     err_regex    =tr("Regex mismatch, regex=%1.");
}

void Column_string_validator::mix_state(QValidator::State &here,QValidator::State mix_me){
    switch(here){
    case Invalid:{return;}
    case Intermediate:{if(mix_me==Invalid){here=Invalid;} return; }
    case Acceptable:{here=mix_me; return;}
    }
}

QValidator::State Column_string_validator::validate(QString &input, int &pos) const{
    fixup(input);
    why="";

    QValidator::State r = Acceptable;

    if(col.min_size.has_value() and input.size() < col.min_size.value()){
        mix_state(r,Intermediate);
        why+=err_too_short.arg(input.size()).arg(col.min_size.value());
    }

    if(col.regex.has_value()){
        auto tmp= QRegularExpressionValidator::validate(input,pos);
        mix_state(r,tmp);
        if(tmp==Invalid){
            why+=err_regex.arg(col.regex.value().pattern() );
        }
    }

    if(col.max_size.has_value() and input.size() > col.max_size.value()){
       mix_state(r,Invalid);
        why+=err_too_long.arg(input.size()).arg(col.max_size.value() );
    }

    state=r;
    return r;
}

void Column_string_validator::fixup(QString &input)const {
    if(col.trim){input=input.trimmed();}
    if(col.max_size.has_value() and input.size() > col.max_size.value()){
        input=input.mid(0,input.size());
    }

    switch(col.case_rule){
    case Column_string::TOLOWER : {input = input.toLower(); break;}
    case Column_string::TOUPPER : {input = input.toUpper(); break;}
    default:break;
    }
}








Column_string_widget::Column_string_widget(
      const Column_string & col_,
      QWidget * parent
):Column_widget(parent), col(col_){

    auto main_layout = qtt::new_layout<QHBoxLayout>();
    this->setLayout(main_layout);
    qtt::cstr_in_layout(value_edit,main_layout);
    qtt::cstr_in_layout(checkbox_na,main_layout);
    qtt::cstr_in_layout(isok,main_layout);

    //--- value_edit ---
    value_edit->setSizePolicy(QSizePolicy::Expanding,QSizePolicy::Minimum );
    validator = new Column_string_validator(col,  col.regex.value_or( QRegularExpression(".*") )    ,nullptr);
    value_edit->setValidator(validator);

    //--- checkbox_na ---
    //checkbox_na->setChecked(true);

    connect(value_edit,&QLineEdit::textChanged,      this, [&](){checkbox_na->setChecked(false); on_change();} );
    connect(checkbox_na,&QCheckBox::stateChanged    ,this, &Column_string_widget::on_change);

    //--- isok ---
    force_validate();
    translate(false);

    setDefault();

}




void Column_string_widget::translate(bool tr_sons){

    QString tt;
    bool is_first=true;


    if(col.trim==false){
        if(!is_first){tt+="\n";}
        tt+=tr("Not trimmed") ;
        is_first=false;
    }

    if(col.trim==true){
        if(!is_first){tt+="\n";}
        tt+=tr("Trimmed") ;
        is_first=false;
    }

    if(col.max_size.has_value()){
        if(!is_first){tt+="\n";}
        tt+=tr("Max size : %1").arg( col.max_size.value() ) ;
        is_first=false;
    }

    if(col.min_size.has_value()){
        if(!is_first){tt+="\n";}
        tt+=tr("Min size : %1").arg( col.min_size.value() ) ;
        is_first=false;
    }

    if(col.case_rule==Column_string::TOLOWER){
        if(!is_first){tt+="\n";}
        tt+=tr("Lowercase only") ;
        is_first=false;
    }

    if(col.case_rule==Column_string::TOUPPER){
        if(!is_first){tt+="\n";}
        tt+=tr("Uppercase only") ;
        is_first=false;
    }

    if(col.regex.has_value()){
        if(!is_first){tt+="\n";}
        tt+=tr("Regex : %1").arg( col.regex.value().pattern() ) ;
        is_first=false;
    }

    value_edit->setToolTip(tt);

    checkbox_na->setText(tr("NA"));
    checkbox_na->setToolTip(tr("Check this box if string is NA"));

    if(tr_sons){
        validator->translate(true);
    }


    err_title   = tr("Error : wrong value");
    err_message = tr("Wrong value : the string %1 is not valid. It will be replaced by NA.\n\n%2");

    on_change();
}


bool Column_string_widget::is_na()const{
    return checkbox_na->checkState()==Qt::CheckState::Checked;
}

void Column_string_widget::on_change(){
    if(is_na()){
        QSignalBlocker b(value_edit);
        value_edit->setText("");
        isok->set_warning("Value is NA");
    }else{
        value_edit->setFocus();
        switch( validator->state ){
        case QValidator::Invalid:     {isok->set_error(validator->why); break;}
        case QValidator::Intermediate:{isok->set_error(validator->why); break;}
        case QValidator::Acceptable:  {isok->set_ok();                  break;}
        };
    }
}

void Column_string_widget::force_validate(){
    auto tmp_s = QString(value_edit->text());
    int  tmp_i = 0;
    value_edit->validator()->validate(tmp_s,tmp_i);
}







void Column_string_widget::setFocus(){
    value_edit->setFocus();
}

void Column_string_widget::setDefault(){
    setValue(col.default_v);
}


auto Column_string_widget::value()const->std::optional<value_type>{
    if(is_na()){return std::nullopt;}

    if(validator->state!=QValidator::Acceptable){
        QMessageBox msgBox(
            QMessageBox::Warning,
            err_title,
            err_message.arg(value_edit->text(), validator->why )
            );
        msgBox.setStandardButtons(QMessageBox::Ok);
        msgBox.setDefaultButton  (QMessageBox::Ok);
        msgBox.exec();
        return std::nullopt;
    }

    return value_edit->text().toStdString();
}


void Column_string_widget::setValue( std::optional<value_type> opt_str ){
    if(opt_str.has_value() ){
        value_edit->setText( QString::fromStdString(opt_str.value())  );
    }else{
        value_edit->setText("");
        checkbox_na->setCheckState(Qt::Checked);
    }

}



void Column_string_widget::db_write(
    tdb::Rowid<tdb::Tag_sqlite> rowid
){
    auto v=value();
    col.q_update(rowid,v);
}

void Column_string_widget::db_read(
    tdb::Rowid<tdb::Tag_sqlite> rowid
){
    auto opt_value = col.q_read(rowid);
    setValue(opt_value);
}









Column_string_widget * Column_string::new_widget(QWidget*parent){
    return new Column_string_widget(*this,  parent);
}






Column_string::Column_string(Db_t& db_, const config::Block&conf):Column_abstract(conf),db(db_){


    //--- read config ---
    if(conf.values.size("regex")!=0){
        std::string regex_str= conf.values.get_unique("regex");

        try{
            regex=QRegularExpression(QString::fromStdString(regex_str));
            if(! regex.value().isValid() ){throw std::runtime_error( regex.value().errorString().toStdString() );}
        }catch(std::exception &e){
            throw std::runtime_error("Error, cannot construct regex from string, at " +conf.debug_str()+", string=" + regex_str+", error="+e.what() );
        }catch(...){
            throw std::runtime_error("Error, cannot construct regex from string, at " +conf.debug_str()+", string=" + regex_str);
        }
    }

    if(conf.values.size("max_size")!=0){max_size=conf.values.get_unique<qsizetype>("max_size");}
    if(conf.values.size("min_size")!=0){min_size=conf.values.get_unique<qsizetype>("min_size");}
    if(conf.values.size("default")!=0){default_v=conf.values.get_unique<std::string>("default");}
    if(conf.values.size("save_na")!=0){save_na  =conf.values.get_unique<std::string>("save_na");}


    trim=conf.values.get_yes_no("trim",true);

    if(conf.values.size("case")!=0){
        std::string case_str= conf.values.get_unique("case");
        static const std::unordered_map<std::string, Column_string::Case_e> case_map=[]{
            std::unordered_map<std::string, Column_string::Case_e> r;
            r["insensitive"]=Column_string::CASE_INSENSITIVE;
            r["tolower"]    =Column_string::TOLOWER;
            r["toupper"]    =Column_string::TOUPPER;
            return r;
        }();

        auto f = case_map.find(case_str);
        if(f==case_map.end()){
            throw std::runtime_error("invalid value for case at "+conf.debug_str()+" valid values are insensitive,tolower,toupper");
        }else{
            case_rule=f->second;
        }
    }
}

QTableWidgetItem*  Column_string::new_db_read(tdb::Rowid<tdb::Tag_sqlite> rowid)const{
    std::optional<value_type> opt = q_read(rowid);
    if(opt.has_value()){
        QString s = convert<QString>(opt.value());
        QTableWidgetItem * r = new QTableWidgetItem ( );
        r->setFlags( r->flags() &~ Qt::ItemIsEditable );
        r->setData(Qt::EditRole, s );
        return r;
    }else{
      return  new QTableWidgetItem ( );
    }
}



void Column_string::prepare(){
    q_read  .prepare(db,"select \""+column_name+"\" from \""+table_name+"\" where rowid=?");
    q_update.prepare(db,"update \""+table_name+"\" set \""+column_name+"\"=?2 where rowid=?1");
}


std::string Column_string::save_data(Rowid_t rowid)const{
    auto v = q_read(rowid);
    if(v.has_value()){
        return v.value();
    }else{
        return save_na;
    }
}

std::string Column_string::sql_create()const{
    return "\""+column_name + "\" varchar";
}
