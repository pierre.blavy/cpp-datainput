#include "Column_factory.hpp"
#include <config/config.hpp>

#include <unordered_map>
#include <functional>

#include "Column_enum.hpp"
#include "Column_int.hpp"
#include "Column_float.hpp"
#include "Column_string.hpp"
#include "Column_datetime.hpp"


//TODO :
// ADD a default value in the options for everything :
//   default = last or default_value
//   default_value = something
//
//
//Float  : min,max
//String
//  Regex, case (NA, tolower, toupper), trim , max_size
//yes/no
//list of values
//date
//datetime :
//  input_timezone  : the default for the input widget tz)
//  input_timestamp : s, ms, ns : the unit for input timestamp
//  output_timezone : the tz the date and time are displayed
//  output_timestamp : s, ms, ns : the unit for database timestamps
//  export_timestamp : if defined, export a timestamp in this unit
//  export_date_tz   : if defined, export a date in this tz
//  export_timestamp_suffix : if defined, add this suffix to the export col name
//  export_date_suffix : if defined, add this suffix to the export col name
//  use now as a special default value



[[nodiscard]] Column_ptr new_column(tdb::Connection_t<tdb::Tag_sqlite> &db, const config::Block&conf){

    static const auto dat = [&](){
        std::unordered_map<std::string, std::function<Column_abstract*(tdb::Connection_t<tdb::Tag_sqlite> &db, const config::Block&)>  > r;
        r["enum"]     =[](tdb::Connection_t<tdb::Tag_sqlite> &db,const config::Block&c)->Column_abstract*{return new Column_enum(db,c);};
        r["integer"]  =[](tdb::Connection_t<tdb::Tag_sqlite> &db,const config::Block&c)->Column_abstract*{return new Column_int(db,c);};
        r["float"]    =[](tdb::Connection_t<tdb::Tag_sqlite> &db,const config::Block&c)->Column_abstract*{return new Column_float(db,c);};
        r["string"]   =[](tdb::Connection_t<tdb::Tag_sqlite> &db,const config::Block&c)->Column_abstract*{return new Column_string(db,c);};
        r["datetime"] =[](tdb::Connection_t<tdb::Tag_sqlite> &db,const config::Block&c)->Column_abstract*{return new Column_datetime(db, c);};


        return r;
    }();


    std::string type = conf.values.get_unique("type");
    auto f = dat.find(type);

    if(f==dat.cend() ){
        throw std::runtime_error("Invalid column type "+type+" at " + conf.debug_str() );
    }

    return Column_ptr( f->second(db,conf) );


}
