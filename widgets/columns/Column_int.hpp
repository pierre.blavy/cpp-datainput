#ifndef COLUMN_INT_PIERRE_HPP
#define COLUMN_INT_PIERRE_HPP

#include <optional>
#include <QString>

#include "Column_abstract.hpp"
#include <OkNanErr_t.hpp>
#include <tdb/tdb_sqlite.hpp>
#include <tdb/functors/all.hpp>

namespace config{class Block;}
class QLineEdit;
class QPushButton;
class IsOk;
class Column_int;


class Column_int_widget:public Column_widget{
    Q_OBJECT

public:
    typedef qlonglong  numeric_type;
    typedef OkNanErr_t <numeric_type,QString> value_type;

    Column_int_widget(const Column_int&col_, QWidget* parent=nullptr);
    QLineEdit    * value_edit = nullptr;
    QPushButton  * nan_button = nullptr;
    IsOk         * isok       = nullptr;

    virtual void db_write(tdb::Rowid<tdb::Tag_sqlite> rowid)override final;
    virtual void db_read (tdb::Rowid<tdb::Tag_sqlite> rowid)override final;

    virtual void setFocus()override final;
    virtual void setDefault()override final;

    virtual void translate(bool=true)override final;

    std::optional<numeric_type> value()const;
    void setValue( std::optional<numeric_type> );

private:
    void on_change();
    void update_isok();
    void setNan();

signals:
    void changed();

private:
    const Column_int & col;
    value_type m_value;

    QString err_title;
    QString err_message;
};





class Column_int: public Column_abstract{

public:
    typedef Column_int_widget::numeric_type numeric_type;
    typedef tdb::Connection_t<tdb::Tag_sqlite> Db_t;
    typedef tdb::Rowid<tdb::Tag_sqlite> Rowid_t;

    explicit  Column_int(Db_t& db, const config::Block&);

    Column_int_widget * new_widget(QWidget*parent=nullptr) override final;
    std::string sql_create()const override final;
    [[nodiscard]] QTableWidgetItem*  new_db_read(tdb::Rowid<tdb::Tag_sqlite> rowid)const override final;
    void prepare()override final;

    virtual std::string save_data(Rowid_t)const override final;


    //--- db and queries ---
    Db_t&db;
    mutable tdb::Fn_get_value_unique<tdb::Tag_sqlite, std::tuple< std::optional<numeric_type> >, std::tuple<Rowid_t> , false> q_read;
    mutable tdb::Fn_insert          <tdb::Tag_sqlite, std::tuple<>,  std::tuple<Rowid_t,std::optional<numeric_type>> , false> q_update;

    //---confing
    std::optional<numeric_type> min_v;
    std::optional<numeric_type> max_v;
    ValueNanEmpty_t<numeric_type> default_v;

};



#endif 
