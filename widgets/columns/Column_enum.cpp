#include "Column_enum.hpp"
#include <QLineEdit>
#include <QPushButton>
#include <widgets/IsOk.hpp>



#include <qtt/qtt_layout.hpp>

#include <convert/to_QString.hpp>
#include <convert/to_string.hpp>

#include <QMessageBox>
#include <QLabel>

#include <config/config.hpp>
#include <config/get_multiline.hpp>


#include <config/convert/convert_lexical.hpp>
#include <widgets/IntValidator_t.hpp>
#include <QTableWidgetItem>

#include <unordered_set>

Column_enum_widget::Column_enum_widget(
    const Column_enum &col_,
    QWidget * parent
  ):
    Column_widget(parent),
    col(col_)
{

    //--- construct widgets ---
    auto main_layout = qtt::new_layout<QHBoxLayout>();
    this->setLayout(main_layout);
    qtt::cstr_in_layout(value_edit ,main_layout);
    qtt::cstr_in_layout(value_label,main_layout);
    qtt::cstr_in_layout(isok       ,main_layout);


    //--- widget params ---
    value_edit->setSizePolicy(QSizePolicy::Expanding,QSizePolicy::Minimum );
    value_label->setSizePolicy(QSizePolicy::Minimum,QSizePolicy::Minimum );

    //--- connect and translate ---
    //connect(nan_button,&QPushButton::clicked  ,this,&Column_enum_widget::setNan);
    connect(value_edit,&QLineEdit::textChanged,this,&Column_enum_widget::on_change);

    translate(false);

    setDefault();
    on_change();
}

void Column_enum_widget::translate(bool){

    QString tt=tr("Enum value")+"<br>";
    for(const auto &p : col.value_map){
        tt+="<b>"+p.first+"</b>"+":"+p.second.label+"<br>";
    }
    value_edit->setToolTip(tt);

    err_title  =tr("Error : wrong enum value");
    err_message=tr("Wrong enum value. The input %1 will be replaced by %2 ");
    err_invalid_value = tr("Error : invalid value %1, please use %2");
}


void Column_enum_widget::on_change(){
    QString tmp = value_edit->text();
    if(col.trim)   {tmp = tmp.trimmed();}
    if(col.tolower){tmp=tmp.toLower();}

    auto f = col.value_map.find(tmp.toStdString());
    if(f==col.value_map.end()){
        //invalid
        QString all_shortcuts="";
        for(const auto &p:col.value_map){
            if(all_shortcuts!=""){all_shortcuts+=", ";}
            all_shortcuts+=p.first;
        }
        isok->set_error(err_invalid_value.arg(tmp).arg(all_shortcuts));

        const std::string &shortcut = col.value_to_shortcut.at(col.na_value);
        value_label->setText(convert<QString>("!"+col.value_map.at(shortcut).label+"!"));
        value_label->setToolTip(tr("Invalid value interpreted as ")+convert<QString>(col.value_map.at(shortcut).label)  );
        value_s="";
    }else{
        //valid
        isok->set_ok();
        value_label->setText   (convert<QString>(f->second.label));
        value_label->setToolTip(convert<QString>(f->second.description));
        value_s=f->second.value;
    }
}


void Column_enum_widget::setFocus(){
    value_edit->setFocus();
}

void Column_enum_widget::setDefault(){
    value_edit->setText(convert<QString>(col.default_v));
}



std::string Column_enum_widget::value()const{
    if(value_s==""){
        const std::string &shortcut = col.value_to_shortcut.at(col.na_value);
        QMessageBox msgBox(
            QMessageBox::Warning,
            err_title,
            err_message.arg(isok->get_message()).arg(convert<QString>(col.value_map.at(shortcut).label))
        );
        msgBox.setStandardButtons(QMessageBox::Ok);
        msgBox.setDefaultButton  (QMessageBox::Ok);
        msgBox.exec();
        return col.na_value;
    }

    //ok
    return value_s;
}





void Column_enum_widget::setValue( const std::string &s ){
    const std::string &shortcut = col.value_to_shortcut.at(s);
    value_edit->setText(convert<QString>(shortcut));
}




void Column_enum_widget::db_write(tdb::Rowid<tdb::Tag_sqlite> rowid ){
    auto v=value();
    col.q_update(rowid,v);
}

void Column_enum_widget::db_read(tdb::Rowid<tdb::Tag_sqlite> rowid )
{
    std::string value = col.q_read(rowid);
    setValue(value);
}

Column_enum_widget * Column_enum::new_widget(QWidget*parent){
    return new Column_enum_widget(*this, parent);
}


QTableWidgetItem*  Column_enum::new_db_read(tdb::Rowid<tdb::Tag_sqlite> rowid)const{
    std::string value = q_read(rowid);

    const std::string & shortcut = value_to_shortcut.at(value);
    const std::string & label    = value_map.at(shortcut).label;


    QTableWidgetItem * r = new QTableWidgetItem ( );
    r->setFlags( r->flags() &~ Qt::ItemIsEditable );
    r->setData(Qt::EditRole, convert<QString>(label) );
    return r;
}

//#include <iostream>

Column_enum::Column_enum(Db_t& db_,const config::Block&conf):
    Column_abstract(conf),
    db(db_)
{
    //--- read config ---
    tolower = conf.values.get_yes_no("tolower",tolower);
    trim    = conf.values.get_yes_no("tolower",trim);
    na_value= conf.values.get_unique("na_value");//checked latter
    default_v=conf.values.get_unique("default");

    //read values
    std::unordered_set<std::string> labels; //used to chek if all labels are unique
    for(const auto &conf_value : conf.blocks.crange("value")){
        std::string shortcut = conf_value.values.get_unique("shortcut");
        El el;
        el.value = conf_value.values.get_unique("value");
        el.label = conf_value.values.get_unique("label");
        el.description = get_multiline(conf_value,"description",true);

        //labels must be unique
        if(labels.count(el.label)!=0){throw std::runtime_error("Error in config, the label "+el.label+" is not unique, at="+conf_value.debug_str() );}
        labels.insert(el.label);

        //shortcuts must be unique, populate value_map
        if(value_map.count(shortcut)!=0){throw std::runtime_error("Error in config, the shortcut "+shortcut+" is not unique, at="+conf_value.debug_str() );}
        value_map[shortcut]=el;

        //values must be unique, populate value_to_shortcut
        if(value_to_shortcut.count(el.value)!=0){throw std::runtime_error("Error in config, the value "+el.value+" is not unique, at="+conf_value.debug_str() );}
        value_to_shortcut[el.value]=shortcut;
    }

    //na_value must match a value
    if(value_to_shortcut.count(na_value)==0){
        throw std::runtime_error("Error in config, the na_value doesn't match any value at="+conf.debug_str() );
    }




    tdb::Fn_execute<tdb::Tag_sqlite,std::tuple<>,std::tuple<std::string,std::string,std::string,std::string,std::string,std::string>,false> q_insert_meta_enum;
    q_insert_meta_enum.prepare(
        db,
        "insert or ignore into meta_enum(column_id, shortcut, value, label, description ) "
        "values ((select column_id from meta_column natural join meta_table where table_name = ? and column_name=?),?,?,?,? ) "
    );
    // table_name, column_name, shortcut, value, label, description

    tdb::execute_a(db,"delete from meta_enum where column_id = (select column_id from meta_column natural join meta_table where table_name = ? and column_name=?)",table_name,column_name);
    for(const auto &p:value_map){
        q_insert_meta_enum(table_name,column_name,p.first, p.second.value, p.second.label, p.second.description );
    }

    //debug code
    /*
    for(const auto &p : value_map){
        std::cout << p.first << " "<< p.second.value << " "<<  p.second.label << " "<<  p.second.description << std::endl;
    }
*/

}

std::string Column_enum::save_data(Rowid_t rowid)const{
    auto v = q_read(rowid);
    return v;
}


void Column_enum::prepare(){
    q_read  .prepare(db,"select \""+column_name+"\" from \""+table_name+"\" where rowid=?");
    q_update.prepare(db,"update \""+table_name+"\" set \""+column_name+"\"=?2 where rowid=?1");
}


std::string Column_enum::sql_create()const{
    return "\""+column_name + "\" varchar";
}
