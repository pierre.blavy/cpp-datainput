#include "Column_datetime.hpp"


#include <widgets/DateTime.hpp>
#include <qtt/qtt_layout.hpp>
#include <convert/to_QString.hpp>

#include <convert/QDateTime_to_string.hpp>
#include <convert/QDateTime_to_QString.hpp>


#include <QMessageBox>
#include <QTableWidgetItem>

#include <config/config.hpp>
#include <config/convert/convert_lexical.hpp>
#include <config/convert/convert_QString.hpp>


#include <widgets/IntValidator_t.hpp>



Column_datetime_widget::Column_datetime_widget(Column_datetime& col_, QWidget * parent):
    Column_widget(parent),
    col(col_)
{

    auto main_layout = qtt::new_layout<QHBoxLayout>();
    this->setLayout(main_layout);

    value_edit = new DateTime(col.datetime_config,nullptr);
    main_layout->addWidget(value_edit);
    //qtt::cstr_in_layout(value_edit,main_layout);

    err_title=tr("Error : wrong value");
    err_message=tr("Wrong value : the string %1 cannot be converted to date time. It will be replaced by NA");
    translate(false);

    setDefault();

}

void Column_datetime_widget::translate(bool tr_sons){
    if(tr_sons){
        value_edit->translate();
    }
}


void Column_datetime_widget::setFocus(){
    value_edit->setFocus();
}

void Column_datetime_widget::setDefault(){
    value_edit->setText(convert<QString>(col.default_v));
}



auto Column_datetime_widget::value()const->std::optional<value_type>{
    if(value_edit->isValid() ){
        return value_edit->timestamp();
    }


    QMessageBox msgBox(
        QMessageBox::Warning,
        err_title,
        err_message.arg(value_edit->text())
        );
    msgBox.setStandardButtons(QMessageBox::Ok);
    msgBox.setDefaultButton  (QMessageBox::Ok);
    msgBox.exec();

    return std::nullopt;
}


void Column_datetime_widget::setValue( std::optional<value_type> opt_int ){
    if(opt_int.has_value() ){
        value_edit->setTimestamp(  opt_int.value()   );
    }else{
        value_edit->setText("NA");
    }
}


void Column_datetime_widget::db_write(
    tdb::Rowid<tdb::Tag_sqlite> rowid
    ){
    auto v=value();
    col.q_update(rowid,v);
}

void Column_datetime_widget::db_read(
    tdb::Rowid<tdb::Tag_sqlite> rowid
    ){
    auto opt_value = col.q_read(rowid);
    setValue(opt_value);
}









Column_datetime_widget * Column_datetime::new_widget(QWidget*parent){
    return new Column_datetime_widget(*this, parent);
}

Column_datetime::Column_datetime(Db_t &db_, const config::Block&conf):
    Column_abstract(conf),
    db(db_)
{

    //---databae ---

    //--- read config ---
    try{
        datetime_config.out_timezone = QTimeZone( conf.values.get_unique<QString>("out_timezone").toLocal8Bit() );
    }catch(std::exception &e){
        throw std::runtime_error("Invalid out_timezone at "+conf.debug_str()+", error="+e.what());
    }

    try{
        datetime_config.in_timezone = QTimeZone( conf.values.get_unique<QString>("in_timezone").toLocal8Bit() );
    }catch(std::exception &e){
        throw std::runtime_error("Invalid in_timezone at "+conf.debug_str()+", error="+e.what());
    }

    default_v = conf.values.get_unique<std::string>("default","NA");

    datetime_config.default_hh=conf.values.get_unique<int>("default_hh",datetime_config.default_hh);
    datetime_config.default_mm=conf.values.get_unique<int>("default_mm",datetime_config.default_mm);
    datetime_config.default_ss=conf.values.get_unique<int>("default_ss",datetime_config.default_ss);
    datetime_config.default_z =conf.values.get_unique<int>("default_z" ,datetime_config.default_z );

    if(datetime_config.default_hh>23  or datetime_config.default_hh<0){ throw std::runtime_error("Invalid default_hh at "+conf.debug_str()+", value must be in range[0; 23]"); }
    if(datetime_config.default_mm>59  or datetime_config.default_mm<0){ throw std::runtime_error("Invalid default_mm at "+conf.debug_str()+", value must be in range[0; 59]"); }
    if(datetime_config.default_ss>59  or datetime_config.default_ss<0){ throw std::runtime_error("Invalid default_ss at "+conf.debug_str()+", value must be in range[0; 59]"); }
    if(datetime_config.default_z >999 or datetime_config.default_z <0){ throw std::runtime_error("Invalid default_z at " +conf.debug_str()+", value must be in range[0; 999]"); }
}




QTableWidgetItem*  Column_datetime::new_db_read(tdb::Rowid<tdb::Tag_sqlite> rowid)const{
    std::optional<value_type> opt = q_read(rowid);
    if(opt.has_value()){
        QTableWidgetItem * r = new QTableWidgetItem ( );
        r->setFlags( r->flags() &~ Qt::ItemIsEditable );


        auto   date = QDateTime::fromMSecsSinceEpoch(opt.value(), datetime_config.out_timezone );
        QString str = convert<QString>(date);
        r->setData(Qt::EditRole, str );
        return r;
    }else{
        return  new QTableWidgetItem ( );
    }
}



std::string Column_datetime::save_header()const{
    return column_name+"_timestamp\t"
         + column_name+"_date\t"
         + column_name+"_time\t"
         + column_name+"_timezone"
    ;
}


std::string Column_datetime::save_data(Rowid_t e)const{
    std::optional<value_type> v=q_read(e);

    std::string timestamp;
    std::string date;
    std::string time;
    std::string timezone;

    if(v.has_value() ){
        timestamp = convert<QString>(v.value()).toStdString();

        QDateTime dt = QDateTime::fromMSecsSinceEpoch( v.value() , datetime_config.out_timezone );
        date      = convert<std::string>(dt.date());
        time      = convert<std::string>(dt.time());
        timezone  = convert<std::string>(dt.timeZone());

    }
    //else all strings are ""

    return timestamp + "\t"
         + date + "\t"
         + time + "\t"
         + timezone;
    ;
}



void Column_datetime::prepare(){
    q_read  .prepare(db,"select \""+column_name+"\" from \""+table_name+"\" where rowid=?");
    q_update.prepare(db,"update \""+table_name+"\" set \""+column_name+"\"=?2 where rowid=?1");
}

std::string Column_datetime::sql_create()const{
    return "\""+column_name + "\" integer";
}
