#ifndef COLUMN_FLOAT_PIERRE_HPP
#define COLUMN_FLOAT_PIERRE_HPP

#include <optional>
#include <QString>

#include <tdb/tdb_sqlite.hpp>
#include <tdb/functors/all.hpp>

#include "Column_abstract.hpp"
#include <OkNanErr_t.hpp>

namespace config{class Block;}
class QLineEdit;
class QPushButton;
class IsOk;
class Column_float;


class Column_float_widget:public Column_widget{
    Q_OBJECT

    public:
    typedef double   numeric_type;
    typedef OkNanErr_t <numeric_type,QString> value_type;
    typedef tdb::Connection_t<tdb::Tag_sqlite> Db_t;
    typedef tdb::Rowid<tdb::Tag_sqlite> Rowid_t;


    Column_float_widget(const Column_float &col_, QWidget* parent=nullptr);
    QLineEdit    * value_edit = nullptr;
    QPushButton  * nan_button = nullptr;
    IsOk         * isok       = nullptr;

    virtual void db_write(tdb::Rowid<tdb::Tag_sqlite> rowid)override final;
    virtual void db_read (tdb::Rowid<tdb::Tag_sqlite> rowid)override final;

    virtual void setFocus()override final;
    virtual void setDefault()override final;

    virtual void translate(bool=true)override final;

    std::optional<numeric_type> value()const;
    void setValue( std::optional<numeric_type> );

    private:
    void on_change();
    void update_isok();
    void setNan();

    signals:
    void changed();

    private:
    value_type m_value;
    const Column_float &col;

    QString err_title;
    QString err_message;
};

class Column_float: public Column_abstract{


  public:
  typedef Column_float_widget::numeric_type numeric_type;
  typedef tdb::Connection_t<tdb::Tag_sqlite> Db_t;
  typedef tdb::Rowid<tdb::Tag_sqlite> Rowid_t;



  explicit  Column_float(Db_t& db_,const config::Block&);

  Column_float_widget * new_widget(QWidget*parent=nullptr) override final;
  std::string sql_create()const override final;
  [[nodiscard]] QTableWidgetItem*  new_db_read(tdb::Rowid<tdb::Tag_sqlite> rowid)const override final;
  void prepare()override final;

  virtual std::string save_data(Rowid_t)const override final;

  //--- db and queries ---
  Db_t&db;
  mutable tdb::Fn_get_value_unique<tdb::Tag_sqlite, std::tuple< std::optional<numeric_type> >, std::tuple<Rowid_t> , false> q_read;
  mutable tdb::Fn_insert          <tdb::Tag_sqlite, std::tuple<>,  std::tuple<Rowid_t,std::optional<numeric_type>> , false> q_update;

  //col config
  std::optional<numeric_type> min_v;
  std::optional<numeric_type> max_v;

  ValueNanEmpty_t<numeric_type> default_v;

};








#endif 
