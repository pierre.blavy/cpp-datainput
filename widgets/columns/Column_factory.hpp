#ifndef COLUMN_FACTORY_PIERRE_HPP
#define COLUMN_FACTORY_PIERRE_HPP

#include "Column_abstract.hpp"
#include <tdb/tdb_sqlite.hpp>

namespace config{class Block;}


[[nodiscard]] Column_ptr new_column(tdb::Connection_t<tdb::Tag_sqlite> &db, const config::Block&);


#endif 
