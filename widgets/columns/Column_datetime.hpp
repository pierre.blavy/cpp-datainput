#ifndef COLUMN_DATETIME_PIERRE_HPP
#define COLUMN_DATETIME_PIERRE_HPP


#include <optional>

#include <tdb/tdb_sqlite.hpp>
#include <tdb/functors/all.hpp>

#include "Column_abstract.hpp"

#include <widgets/DateTime.hpp>

namespace config{class Block;}

class Column_datetime;



class Column_datetime_widget:public Column_widget{
    Q_OBJECT

    public:

    typedef qint64 value_type;//i.e. timestamp

    Column_datetime_widget(Column_datetime& col_, QWidget* parent=nullptr);

    DateTime * value_edit=nullptr;

    virtual void db_write(tdb::Rowid<tdb::Tag_sqlite> rowid)override final;
    virtual void db_read (tdb::Rowid<tdb::Tag_sqlite> rowid)override final;

    virtual void setFocus()override final;
    virtual void setDefault()override final;

    virtual void translate(bool=true)override final;

    std::optional<value_type> value()const;
    void setValue( std::optional<value_type> );

    const Column_datetime &col;

    QString err_title;
    QString err_message;

};


class Column_datetime: public Column_abstract{

    public:
    typedef Column_datetime_widget::value_type value_type;
    typedef tdb::Connection_t<tdb::Tag_sqlite> Db_t;
    typedef tdb::Rowid<tdb::Tag_sqlite> Rowid_t;

    explicit  Column_datetime(Db_t &db_, const config::Block&);

    Column_datetime_widget * new_widget(QWidget*parent=nullptr) override final;
    std::string sql_create()const override final;
    [[nodiscard]] QTableWidgetItem*  new_db_read(tdb::Rowid<tdb::Tag_sqlite> rowid)const override final;
    void prepare()override final;

    virtual std::string save_header()const override final;
    virtual std::string save_data(Rowid_t)const override final;



    //--- db and queries ---
    Db_t&db;
    mutable tdb::Fn_get_value_unique<tdb::Tag_sqlite, std::tuple< std::optional<value_type> >, std::tuple<Rowid_t> , false> q_read;
    mutable tdb::Fn_insert          <tdb::Tag_sqlite, std::tuple<>,  std::tuple<Rowid_t,std::optional<value_type>> , false> q_update;

    //--- config ---
    DateTime_config datetime_config;
    std::string default_v;

};




#endif // COLUMN_DATETIME_PIERRE_HPP
