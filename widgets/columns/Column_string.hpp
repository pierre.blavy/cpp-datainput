#ifndef COLUMN_string_PIERRE_HPP
#define COLUMN_string_PIERRE_HPP

#include <optional>
#include <string>
#include <QRegularExpression>
#include <QRegularExpressionValidator>
#include "Column_abstract.hpp"
namespace config{class Block;}

class QLineEdit;
class QPushButton;
class Column_string;
class IsOk;
class QCheckBox;
class Column_string;
#include <tdb/tdb_sqlite.hpp>
#include <tdb/functors/all.hpp>

class Column_string;



class Column_string_validator : public QRegularExpressionValidator , qtt::Translatable{
    Q_OBJECT

    public:
    explicit Column_string_validator(const Column_string & col_, const QRegularExpression &re, QObject *parent = nullptr);

    static void mix_state(QValidator::State &here,QValidator::State mix_me);

    QValidator::State validate(QString &input, int &pos) const override final;

    void translate(bool =true)override final;

    void fixup(QString &input)const override final;

    QString err_too_short;
    QString err_too_long;
    QString err_regex;

    //store last state
    const Column_string& col;
    mutable QString why;
    mutable QValidator::State state;
};



class Column_string_widget:public Column_widget{
    Q_OBJECT

    public:
    typedef std::string value_type;
    typedef tdb::Connection_t<tdb::Tag_sqlite> Db_t;
    typedef tdb::Rowid<tdb::Tag_sqlite> Rowid_t;

    Column_string_widget(const Column_string&, QWidget* parent=nullptr);
    QLineEdit * value_edit = nullptr;
    QCheckBox * checkbox_na      = nullptr;
    IsOk      * isok       = nullptr;


    virtual void db_write( tdb::Rowid<tdb::Tag_sqlite> rowid)override final;
    virtual void db_read ( tdb::Rowid<tdb::Tag_sqlite> rowid)override final;
    virtual void setFocus() override final;
    virtual void setDefault()override final;

    virtual void translate(bool=true)override final;

    std::optional<value_type> value()const;
    void setValue( std::optional<value_type> );

    Column_string_validator * validator = nullptr;
    const Column_string & col;


    private:
    void force_validate();
    bool is_na()const;

    QString err_title;
    QString err_message;

    private slots:
    void on_change();
};





class Column_string: public Column_abstract{

  public:
  typedef std::string value_type;
  typedef tdb::Connection_t<tdb::Tag_sqlite> Db_t;
  typedef tdb::Rowid<tdb::Tag_sqlite> Rowid_t;

  explicit  Column_string(Db_t& db, const config::Block&);

  Column_string_widget * new_widget(QWidget*parent=nullptr) override final;
  std::string            sql_create()const override final;
  [[nodiscard]] QTableWidgetItem*  new_db_read(tdb::Rowid<tdb::Tag_sqlite> rowid)const override final;
  void prepare()override final;

  virtual std::string save_data(Rowid_t)const override final;

  //--- db and queries ---
  Db_t&db;
  mutable tdb::Fn_get_value_unique<tdb::Tag_sqlite, std::tuple< std::optional<value_type> >, std::tuple<Rowid_t> , false> q_read;
  mutable tdb::Fn_insert          <tdb::Tag_sqlite, std::tuple<>,  std::tuple<Rowid_t,std::optional<value_type>> , false> q_update;

  //--- col config
  bool trim = true;

  std::optional<std::string> default_v;
  std::optional<qsizetype>   max_size;
  std::optional<qsizetype>   min_size;
  std::string save_na="NA";//how na are saved

  enum Case_e{
      CASE_INSENSITIVE,  //preserve the case
      TOLOWER,           //convert string to lowercase
      TOUPPER            //convert string to uppercase
  };
  Case_e case_rule=CASE_INSENSITIVE;

  std::optional<QRegularExpression> regex;
};








#endif 
