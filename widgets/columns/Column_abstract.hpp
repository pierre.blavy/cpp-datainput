#ifndef COLUMN_ABSTRACT_PIERRE_HPP
#define COLUMN_ABSTRACT_PIERRE_HPP

#include <QWidget>
#include <memory>
#include <string>
#include <functional>

#include <qtt/qtt_Translatable.hpp>

#include <tdb/tdb_sqlite.hpp>
#include <tdb/functors/Fn_get_value_unique.hpp>

namespace config{class Block;}

class Column_widget;
class QTableWidgetItem;

class Column_keyhandler:public QObject {
    Q_OBJECT
public:
    explicit Column_keyhandler(Column_widget * col_):col(col_){}
protected:
    Column_keyhandler(Column_widget);
    virtual bool eventFilter(QObject* obj, QEvent* event)override;
    Column_widget * col=nullptr;
};



class Column_widget: public QWidget, qtt::Translatable{
    Q_OBJECT;

    public:
    virtual ~Column_widget();
    Column_widget(QWidget*parent=nullptr);

    //TODO change me!
    virtual void db_write( tdb::Rowid<tdb::Tag_sqlite> rowid)=0;
    virtual void db_read ( tdb::Rowid<tdb::Tag_sqlite> rowid)=0;


    virtual void setFocus()=0;
    virtual void setDefault()=0;


    //Column_widget* next_widget=nullptr;//NOT owned, used to set next widget focus if exists
    //void nextFocus(){if( next_widget!=nullptr){next_widget->setFocus();}  }

    std::function<void()> onEnter=[](){};

    protected:

        /*
    //the static version is a herlper, that takes a value
    template<typename T>
    static void db_write(tdb::Connection_t<tdb::Tag_sqlite>&db, const std::string &table_name, const std::string & column_name,  tdb::Rowid<tdb::Tag_sqlite> rowid , const T &value);

    //the static version is a herlper, that returns a value
    template<typename T>
    static std::optional<T> db_read(tdb::Connection_t<tdb::Tag_sqlite>&db, const std::string &table_name, const std::string & column_name,  tdb::Rowid<tdb::Tag_sqlite> rowid);
*/
};
/*
template<typename T>
void Column_widget::db_write(
    tdb::Connection_t<tdb::Tag_sqlite>&db,
    const std::string &table_name,
    const std::string &column_name,
    tdb::Rowid<tdb::Tag_sqlite> rowid,
    const T &value
){
    tdb::execute_a(db,"update \""+table_name+"\" set\""+column_name+"\"=? where rowid=?", value, rowid  );
}


template<typename T>
std::optional<T> Column_widget::db_read(
    tdb::Connection_t<tdb::Tag_sqlite>&db,
    const std::string &table_name,
    const std::string &column_name,
    tdb::Rowid<tdb::Tag_sqlite> rowid
    ){

    tdb::Fn_get_value_unique<
        tdb::Tag_sqlite,
        std::tuple<std::optional<T>>,
        std::tuple<tdb::Rowid<tdb::Tag_sqlite>>,
        false
    > fn;
    fn.prepare(db,"select \""+column_name+"\" from \""+table_name+"\" where rowid=?");

    return fn(rowid);
}
*/



class Column_abstract{
  public:
  typedef typename tdb::Rowid_t<tdb::Tag_sqlite>::type Rowid_t;

  virtual ~Column_abstract()=default;

  virtual Column_widget *    new_widget(QWidget*parent=nullptr)=0;                  //not owned
  [[nodiscard]] virtual  QTableWidgetItem*  new_db_read(tdb::Rowid<tdb::Tag_sqlite> rowid)const =0; //not owned
  virtual std::string        sql_create()const=0;
  virtual void prepare(){}//post prone queries preparation, as the table must be created

  virtual std::string save_header()const{return column_name;}
  virtual std::string save_data(Rowid_t)const=0;


  std::string table_name;
  std::string column_name;
  std::string description;
  
  protected:
  explicit Column_abstract(const config::Block &);
  Column_abstract(const Column_abstract&)=delete;
  Column_abstract& operator=(const Column_abstract&)=delete;



  
};

typedef std::unique_ptr<Column_abstract> Column_ptr;


#endif 
