#include "Column_abstract.hpp"

#include <config/config.hpp>
#include <config/get_multiline.hpp>
#include <validate/is_name.hpp>
#include <algorithm>

#include<tdb/tdb_sqlite.hpp>
#include<tdb/functors/all.hpp>

#include <QEvent>
#include <QKeyEvent>

Column_widget::Column_widget(QWidget*parent):QWidget(parent){
    this->installEventFilter(new Column_keyhandler(this));

}

Column_widget::~Column_widget(){}

bool Column_keyhandler::eventFilter(QObject* obj, QEvent* event){
    //https://wiki.qt.io/How_to_catch_enter_key
    if (event->type()==QEvent::KeyPress) {
        QKeyEvent* key = static_cast<QKeyEvent*>(event);
        if ( (key->key()==Qt::Key_Enter) || (key->key()==Qt::Key_Return) ) {
            col->onEnter();
        } else {
            return QObject::eventFilter(obj, event);
        }
        return true;
    } else {
        return QObject::eventFilter(obj, event);
    }
    return false;
}


Column_abstract::Column_abstract(const config::Block &conf){
    if(conf.blocks.enclosing_block==nullptr){
        throw std::runtime_error("Invalid conf structure, a column must be in a table. Error at "+conf.debug_str() );
    }

    column_name = conf.values.get_unique("name");
    table_name  = conf.blocks.enclosing_block->values.get_unique("name");
    description = get_multiline(conf,"description");

    if(!validate::is_name(table_name)){
        throw std::runtime_error("Invalid table_name name, a name start with a letter, and contains only letters, numbers and underscores. Error at "+conf.blocks.enclosing_block->debug_str() );
    }

    if(!validate::is_name(column_name)){
        throw std::runtime_error("Invalid column name, a name start with a letter, and contains only letters, numbers and underscores. Error at "+conf.debug_str() );
    }

    std::string lower_name;
    std::transform(column_name.begin(), column_name.end(), lower_name.begin(), [](unsigned char c){ return std::tolower(c); });
    if(lower_name == "rowid" or lower_name=="oid" or lower_name=="_rowid_" ){
        throw std::runtime_error("Invalid column name, rowid, oid and _rowid_ are reserved names. Error at "+conf.debug_str() );
    }

}

/*
QTableWidgetItem Column_abstract::db_read (tdb::Connection_t<tdb::Tag_sqlite>&db, const std::string &table_name, tdb::Rowid<tdb::Tag_sqlite> rowid)const{
    tdb::Fn_get_value_unique<
        tdb::Tag_sqlite,
        std::tuple<std::optional<std::string> >,
        std::tuple<tdb::Rowid<tdb::Tag_sqlite> >,
        false
    > fn;

    fn.prepare(db,"SELECT cast(\""+name+"\" as text) from \""+ table_name +"\" WHERE rowid=?");

    auto s = fn(rowid);

    return QString::fromStdString(s.value_or(""));
}
*/

/*
void Column_abstract::db_read_all (tdb::Connection_t<tdb::Tag_sqlite>&db, const std::string &table_name, std::function<void(std::optional<std::string>)> do_something)const{
    tdb::Fn_foreach<
        tdb::Tag_sqlite,
        std::tuple< std::optional<std::string>  >,
        std::tuple<>,
        false
        >fn;

    fn.prepare(db,"SELECT cast(\""+name+"\" as text) from \""+ table_name+"\"");

    fn(do_something);

}
*/
