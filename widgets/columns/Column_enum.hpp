#ifndef COLUMN_ENUM_PIERRE_HPP
#define COLUMN_ENUM_PIERRE_HPP

#include <map>
#include <unordered_map>

#include <QString>

#include <tdb/tdb_sqlite.hpp>
#include <tdb/functors/all.hpp>

#include "Column_abstract.hpp"
#include <OkNanErr_t.hpp>

namespace config{class Block;}
class QLineEdit;
class QPushButton;
class QLabel;
class IsOk;
class Column_enum;



class Column_enum_widget:public Column_widget{
    Q_OBJECT

    public:
    typedef tdb::Connection_t<tdb::Tag_sqlite> Db_t;
    typedef tdb::Rowid<tdb::Tag_sqlite>        Rowid_t;


    Column_enum_widget(const Column_enum &col_, QWidget* parent=nullptr);
    QLineEdit    * value_edit  = nullptr;
    QLabel       * value_label = nullptr;
    IsOk         * isok        = nullptr;

    virtual void db_write(tdb::Rowid<tdb::Tag_sqlite> rowid)override final;
    virtual void db_read (tdb::Rowid<tdb::Tag_sqlite> rowid)override final;

    virtual void setFocus()override final;
    virtual void setDefault()override final;

    virtual void translate(bool=true)override final;

    std::string value()const;
    void setValue( const std::string &s ); //throw if s is not a valid value

    private:
    void on_change();

    private:
    const Column_enum & col;
    QString err_title;
    QString err_message;
    QString err_invalid_value;

    std::string value_s;


};

class Column_enum: public Column_abstract{


  public:
  typedef tdb::Connection_t<tdb::Tag_sqlite> Db_t;
  typedef tdb::Rowid<tdb::Tag_sqlite> Rowid_t;



  explicit  Column_enum(Db_t& db_,const config::Block&);

  Column_enum_widget * new_widget(QWidget*parent=nullptr) override final;
  std::string sql_create()const override final;
  [[nodiscard]] QTableWidgetItem*  new_db_read(tdb::Rowid<tdb::Tag_sqlite> rowid)const override final;
  void prepare()override final;

  virtual std::string save_data(Rowid_t)const override final;

  //--- db and queries ---
  Db_t&db;
  mutable tdb::Fn_get_value_unique<tdb::Tag_sqlite, std::tuple< std::string >, std::tuple<Rowid_t> , false> q_read;    //read the value
  mutable tdb::Fn_insert          <tdb::Tag_sqlite, std::tuple<>,  std::tuple<Rowid_t,std::string> , false> q_update;  //update the value

  //col config
  bool tolower = true;
  bool trim    = true;
  std::string na_value;
  std::string default_v;


  //--- content ---
  struct El{
      std::string value;
      std::string label;
      std::string description;
  };
  std::map<std::string,El> value_map; //shortcut => El, alphabetical order
  std::unordered_map<std::string,std::string>value_to_shortcut;

  friend Column_enum_widget;



};








#endif 
