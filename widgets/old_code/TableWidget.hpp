#ifndef TABLEWIDGET_PIERRE_HPP
#define TABLEWIDGET_PIERRE_HPP

#include "columns/Column_abstract.hpp"


#include <QWidget>
#include <vector>
#include <qtt/qtt_Translatable.hpp>

#include <tdb/tdb_sqlite.hpp>
#include <tdb/functors/all.hpp>


namespace config{class Block;}

class QTableWidget;
class QWidget;
class QLabel;
class QPushButton;

class TableWidget:public QWidget, qtt::Translatable{
    Q_OBJECT;

    public:
    explicit TableWidget(const config::Block &conf, tdb::Connection_t<tdb::Tag_sqlite> &db_, QWidget* parent = nullptr);

    //std::string sql_select_all()const;

    std::string name;
    std::string description;


    void translate(bool tr_sons=true)override final;


    QLabel *      editor_title = nullptr;
    QTableWidget* table_widget = nullptr;

    QPushButton * button_delete = nullptr;
    QPushButton * button_modify = nullptr;
    QPushButton * button_create = nullptr;

    //--- columns --
    std::vector<Column_ptr>     col_vect;    //Columns
    std::vector<Column_widget*> widget_vect; //Column widgets for editing values


    size_t col_size()const{return col_vect.size();} //both col_vect and widget_vect have the same size

    private:
    typedef tdb::Rowid<tdb::Tag_sqlite> Rowid;


    void on_delete();
    void on_modify();
    void on_create();

    //--- helpers ---
    void mode_add();
    void mode_edit(Rowid);

    //void update_line(int line_index);

    static void disable(QPushButton *b);
    static void enable (QPushButton *b);



    //--- queries---
    tdb::Connection_t<tdb::Tag_sqlite> &db;
    tdb::Fn_insert <tdb::Tag_sqlite,std::tuple<>,std::tuple<>     ,false> q_newline;
    tdb::Fn_execute<tdb::Tag_sqlite,std::tuple<>,std::tuple<Rowid>,false> q_delline;


private:
    std::string sql_create()const;

};


#endif
