#ifndef QLONGLONGSPINBOX_HPP
#define QLONGLONGSPINBOX_HPP
//code from https://stackoverflow.com/questions/8383620/64bit-int-spin-box-in-qt

#include <QWidget>
#include <QAbstractSpinBox>
#include <QLineEdit>

#include <limits>
#include <optional>

class Q_WIDGETS_EXPORT QLongLongSpinBox : public QAbstractSpinBox{
    Q_OBJECT
    typedef qlonglong numeric_type;
    typedef std::optional<qlonglong> value_type;


    //Q_PROPERTY(value_type minimum READ minimum WRITE setMinimum)
    //Q_PROPERTY(value_type maximum READ maximum WRITE setMaximum)
    Q_PROPERTY(value_type value READ value WRITE setValue NOTIFY valueChanged USER true)



    public:
    explicit QLongLongSpinBox(QWidget *parent = 0);

    QLongLongSpinBox(const QLongLongSpinBox&)=delete;
    QLongLongSpinBox& operator=(const QLongLongSpinBox&)=delete;


    value_type value() const;
    numeric_type minimum() const;
    numeric_type maximum() const;

    void setMinimum(numeric_type min);
    void setMaximum(numeric_type max);
    void setRange(numeric_type min, numeric_type max);

    virtual void stepBy(int steps) override;

    protected:
    //virtual QValidator::State validate(QString &input, int &pos) const override;
    virtual QAbstractSpinBox::StepEnabled stepEnabled()          const override;

    virtual value_type        valueFromText(const QString &text) const;
    virtual QString           textFromValue(value_type val) const;

    public slots:
    void setValue(value_type val);
    void onEditFinished();

    signals:
    void valueChanged(value_type v);

    private:
    //Q_DISABLE_COPY(QLongLongSpinBox)

    numeric_type m_minimum = std::numeric_limits<numeric_type>::lowest();
    numeric_type m_maximum = std::numeric_limits<numeric_type>::max();
    value_type m_value = 0;

};







#endif // QLONGLONGSPINBOX_HPP
