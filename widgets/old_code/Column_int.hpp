#ifndef COLUMN_INT_PIERRE_HPP
#define COLUMN_INT_PIERRE_HPP

#include <optional>
#include <QSpinBox>


#include "Column_abstract.hpp"
#include "widgets/QLongLongSpinBox.hpp"


namespace config{class Block;}
class QLineEdit;

/*
class MyIntSpinBox:public QAbstractSpinBox{
    //https://stackoverflow.com/questions/8383620/64bit-int-spin-box-in-qt
public:
    using QAbstractSpinBox::QAbstractSpinBox;
    int valueFromText(const QString &text) const override final{
        auto tmp=text.trimmed().toLower();
        if(tmp=="" or tmp=="na" or tmp == "nan"){return minimum();}//minimum is a magic value, meaning NA
        return QDoubleSpinBox::valueFromText(tmp);
    }
};
*/




class MyIntSpinBox : public QAbstractSpinBox{
    Q_OBJECT
    typedef qlonglong numeric_type;
    typedef std::optional<qlonglong> value_type;


    public:
    explicit MyIntSpinBox(QWidget *parent = 0);

    MyIntSpinBox(const QLongLongSpinBox&)=delete;
    MyIntSpinBox& operator=(const QLongLongSpinBox&)=delete;


    value_type value() const;
    numeric_type minimum() const;
    numeric_type maximum() const;

    void setMinimum(numeric_type min);
    void setMaximum(numeric_type max);
    void setRange(numeric_type min, numeric_type max);

    virtual void stepBy(int steps) override;

protected:
    //virtual QValidator::State validate(QString &input, int &pos) const override;
    virtual QAbstractSpinBox::StepEnabled stepEnabled()          const override;

    virtual value_type        valueFromText(const QString &text) const;
    virtual QString           textFromValue(value_type val) const;

    public slots:
    void setValue(value_type val);
    void onEditFinished();

    signals:
    void valueChanged(value_type v);

    private:
    numeric_type m_minimum = std::numeric_limits<numeric_type>::lowest();
    numeric_type m_maximum = std::numeric_limits<numeric_type>::max();
    value_type m_value = 0;

};







class Column_widget_int:public Column_widget{

    public:
    typedef qlonglong value_type;

    Column_widget_int(std::optional<value_type> min_v, std::optional<value_type> max_v, QWidget* parent=nullptr);
    //QLineEdit * value_edit=nullptr;
    QLongLongSpinBox * value_edit=nullptr;

    virtual void db_write(tdb::Connection_t<tdb::Tag_sqlite>&db, const std::string &table_name, const std::string & column_name, tdb::Rowid<tdb::Tag_sqlite> rowid)override final;
    virtual void db_read (tdb::Connection_t<tdb::Tag_sqlite>&db, const std::string &table_name, const std::string & column_name, tdb::Rowid<tdb::Tag_sqlite> rowid)override final;
    virtual void setFocus()override final;
    virtual void setDefault()override final;

    virtual void translate(bool=true)override final;

    std::optional<value_type> value()const;
    void setValue( std::optional<value_type> );

    private slots:
    void on_change();


};

class Column_int: public Column_abstract{

  public:
    typedef qlonglong value_type;
  explicit  Column_int(const config::Block&);

    std::optional<value_type> min_v;
  std::optional<value_type> max_v;

  Column_widget_int * new_widget(QWidget*parent=nullptr) override final;
  std::string sql()const override final;

};








#endif 
