#include "QLongLongSpinBox.hpp"



QLongLongSpinBox::QLongLongSpinBox(QWidget *parent):QAbstractSpinBox(parent){
    connect(lineEdit(), SIGNAL(textEdited(QString)), this, SLOT(onEditFinished()));
    this->setSpecialValueText("NA");
};


QLongLongSpinBox::value_type QLongLongSpinBox::value() const{
    return m_value;
};




QLongLongSpinBox::numeric_type QLongLongSpinBox::minimum() const{return m_minimum;}
QLongLongSpinBox::numeric_type QLongLongSpinBox::maximum() const{return m_maximum;}

void QLongLongSpinBox::setMinimum(numeric_type min){ m_minimum = min;}
void QLongLongSpinBox::setMaximum(numeric_type max){ m_maximum = max;}

void QLongLongSpinBox::setRange(numeric_type min, numeric_type max){
    setMinimum(min);
    setMaximum(max);
}



void QLongLongSpinBox::stepBy(int steps){

    numeric_type new_value;

    //if a value exists, use it, else use minimum
    if(m_value.has_value()){
        new_value=m_value.value();
    }else{
        if(0>=m_minimum and 0<= m_maximum){
            new_value=0;
        }else{
            new_value=m_minimum;
        }
    }

    //add steps to new_value
    if (steps < 0 && new_value + steps > new_value) {
        new_value = std::numeric_limits<numeric_type>::min();
    }
    else if (steps > 0 && new_value + steps < new_value) {
        new_value = std::numeric_limits<numeric_type>::max();
    }
    else {
        new_value += steps;
    }

    lineEdit()->setText(textFromValue(new_value));
    setValue(new_value);
}

/*
QValidator::State QLongLongSpinBox::validate(QString &input, int &pos) const{
    input=input.trimmed().toUpper();

    if(input=="")   {return QValidator::Intermediate;}
    if(input=="N")  {return QValidator::Intermediate;}
    if(input=="NA") {return QValidator::Acceptable;}
    if(input=="NAN"){input="NA"; return QValidator::Acceptable;}

    if(input=="-"){return QValidator::Intermediate;}


    bool ok;
    value_type val = input.toLongLong(&ok);
    if (!ok)
        return QValidator::Invalid;

    if (val < m_minimum || val > m_maximum)
        return QValidator::Invalid;

    return QValidator::Acceptable;
}
*/


QLongLongSpinBox::value_type QLongLongSpinBox::valueFromText(const QString &text) const{
    QString input=text.trimmed().toUpper();
    if(input=="")   {return std::nullopt;}
    if(input=="N")  {return std::nullopt;}
    if(input=="NA") {return std::nullopt;}
    if(input=="NAN"){return std::nullopt;}

    return input.toLongLong();
}


QString QLongLongSpinBox::textFromValue(value_type val) const{
    if(val.has_value()){return  QString::number(val.value());}
    else{return "NA";}


    //return QString::number(val);
}


QAbstractSpinBox::StepEnabled QLongLongSpinBox::stepEnabled() const{
    return StepUpEnabled | StepDownEnabled;
}


void QLongLongSpinBox::setValue(value_type val){
    if (m_value != val) {
        lineEdit()->setText(textFromValue(val));
        m_value = val;
    }
}



void  QLongLongSpinBox::onEditFinished(){
    QString input = lineEdit()->text();
    int pos = 0;
    if (QValidator::Acceptable == validate(input, pos))
        setValue(valueFromText(input));
    else
        lineEdit()->setText(textFromValue(m_value));
}
