#include "TableWidget.hpp"

#include "columns/Column_factory.hpp"

#include <config/config.hpp>
#include <config/get_multiline.hpp>

#include <validate/is_name.hpp>
#include <qtt/qtt_layout.hpp>


#include <QLabel>
#include <QTableWidget>
#include <QPushButton>

#include "convert/to_QString.hpp"
#include "convert/from_QString.hpp"


void TableWidget::translate(bool tr_sons){

    if(button_delete!=nullptr){ button_delete->setText(tr("Delete")); }
    if(button_modify!=nullptr){ button_modify->setText(tr("Modify")); }
    if(button_create!=nullptr){ button_create->setText(tr("Create")); }

    if(tr_sons){

    }

}


namespace{
template<typename T>
[[nodiscard]] QTableWidgetItem * new_item(const T&t){
    QTableWidgetItem * r = new QTableWidgetItem( convert<QString>(t) );
    r->setFlags( r->flags() &~ Qt::ItemIsEditable );
    return r;
}


}


TableWidget::TableWidget(const config::Block &conf, tdb::Connection_t<tdb::Tag_sqlite> &db_,  QWidget*parent):QWidget(parent),db(db_){


    name        = conf.values.get_unique("name");
    description = get_multiline(conf,"description");

    if(!validate::is_name(name)){
        throw std::runtime_error("Invalid table name, a name start with a letter, and contains only letters, numbers and underscores. Error at "+conf.debug_str() );
    }

    //Configure columns
    col_vect   .reserve(conf.blocks.size("column"));
    widget_vect.reserve(conf.blocks.size("column"));

    for(const config::Config &conf_col : conf.blocks.crange("column") ){
        col_vect.emplace_back( new_column(conf_col) );
    }


    //--- create table ---
    tdb::execute(db,sql_create());

    //Create widgets

    //Main layout contains editor_widget and table_widget
    auto main_layout = new QVBoxLayout();
    this->setLayout(main_layout);

    QWidget*      editor_widget = nullptr;
    qtt::cstr_in_layout(editor_widget,main_layout);
    qtt::cstr_in_layout(table_widget,main_layout,0,col_vect.size()+1);



    //editor constains editor_title and editor_columns_widget
    auto editor_layout = qtt::new_layout<QVBoxLayout>();
    editor_widget->setLayout(editor_layout);

    qtt::cstr_in_layout(editor_title,editor_layout);
    editor_title->setText("TODO title");

    QWidget* editor_columns_widget = nullptr;
    qtt::cstr_in_layout(editor_columns_widget,editor_layout);


    //editor_columns_widget contains columns stuff
    auto editor_columns_layout = qtt::new_layout<QGridLayout>();
    editor_columns_widget->setLayout(editor_columns_layout);
    editor_columns_layout->setColumnStretch(0,0);
    editor_columns_layout->setColumnStretch(1,1);

    //editor_action contains buttons
    QWidget* editor_button_widget = nullptr;
    qtt::cstr_in_layout(editor_button_widget,editor_layout);
    auto editor_button_layout = qtt::new_layout<QHBoxLayout>();
    editor_button_widget->setLayout(editor_button_layout);

    qtt::cstr_in_layout(button_delete,editor_button_layout);
    qtt::cstr_in_layout(button_modify,editor_button_layout);
    qtt::cstr_in_layout(button_create,editor_button_layout);

    //--- populate columns ---

    QStringList horizontal_headers;
    horizontal_headers.append("rowid");

    for(size_t col_index = 0; col_index < col_vect.size(); ++col_index){
        auto& col = *col_vect[col_index];

        //populate in editor
        QLabel * editor_label = nullptr;
        qtt::cstr_in_grid(editor_label,editor_columns_layout,col_index,0);
        editor_label->setText   ( QString::fromStdString(col.name) );
        editor_label->setToolTip( QString::fromStdString(col.description) );
        //set_label(editor_label,col);

        auto editor_edit = col.new_widget();
        editor_columns_layout->addWidget(editor_edit,col_index,1);
        widget_vect.push_back(editor_edit);

        horizontal_headers.append(QString::fromStdString(col.name));
        //auto it = new QTableWidgetItem(1000);
        //it->setFlags( it->flags()   & ~Qt::ItemIsEditable );
        //table_widget->setItem(0, col_index+1 , it );
    }
    table_widget->setHorizontalHeaderLabels(horizontal_headers);

    //--- TODO populate lines (read from database) ---

    //--- TODO create and prepare delete, edit, create queries ---
    //TODO loop bind ???
    //TODO variants to sql ???

    //--- connect ---
    connect(this->button_create,&QPushButton::clicked,this,&TableWidget::on_create);
    connect(this->button_modify,&QPushButton::clicked,this,&TableWidget::on_modify);
    connect(this->button_delete,&QPushButton::clicked,this,&TableWidget::on_delete);

    //--- queries ---
    q_newline.prepare(db,"insert into \""+name+"\" (rowid) VALUES (NULL)");
    q_delline.prepare(db,"delete from \""+name+"\" where rowid=?");


    //---load content from the database in the table ---
    //rowid
    tdb::Fn_foreach<
        tdb::Tag_sqlite,
        std::tuple< std::string  >,
        std::tuple<>,
        false
    > fn_rowid;

    int line_index=0;
    fn_rowid.prepare(db,"SELECT cast(rowid as text) from \""+ name +"\" ");
    fn_rowid( [&line_index,this](std::string rowid){
        table_widget->setRowCount(line_index+1);
        table_widget->setItem(line_index,0, new_item(rowid) );
        ++line_index;
    } );

    //other columns
    for(size_t i=0; i < col_size(); ++i){
        int line_index=0;
        col_vect[i]->db_read_all(db,name,[&](std::optional<std::string>  s){
            if( s.has_value() ){ table_widget->setItem(line_index,i+1, new_item( s.value() ) );}
            ++line_index;
        });
    }

    //set the next_widget for chaining focus
    for(size_t i=1; i < col_size(); ++i){
        widget_vect[i-1]->next_widget =widget_vect[i] ;
    }



    table_widget->setSortingEnabled(true);
    table_widget->sortByColumn(0,Qt::DescendingOrder);


    //--- connect ---

    connect(table_widget,&QTableWidget::cellClicked, this, [&](int row, int column){
        //std::cout << "row:"<<row << "," << column <<std::endl;

        QString rowid_str = table_widget->item(row,0)->text();
        //std::cout << "STR:"<<rowid_str.toStdString()<<std::endl;

        Rowid   rowid = convert<Rowid>(rowid_str);
        //std::cout << "rowid:"<<rowid<<std::endl;

        mode_edit(rowid);

        //column0 in table is rowid, which is not editable
        //column 1+ in table matches widget_vect 0+
        if(column>0){
            widget_vect.at(column-1)->setFocus();
        }else{
            if(widget_vect.size()>0){ widget_vect.at(0)->setFocus();}
        }
    } );


    mode_add();
    translate();
}

void TableWidget::disable(QPushButton *b){
    b->setEnabled(false);
    b->setStyleSheet("QPushButton{color: grey;}");
}

void TableWidget::enable(QPushButton *b){
    b->setEnabled(true);
    b->setStyleSheet("");
}


void TableWidget::mode_add(){
    disable(button_delete);
    disable(button_modify);
    enable (button_create);
    editor_title->setText(tr("new line"));
}

void TableWidget::mode_edit(Rowid rowid){
    enable(button_delete);
    enable(button_modify);
    enable(button_create);

    editor_title->setText(QString("rowid:%1").arg(rowid));

    //load line in widgets
    for(size_t i =0; i< col_size(); ++i){
        widget_vect[i]->db_read(db,name,col_vect[i]->name ,rowid);
    }

}


void TableWidget::on_delete(){
    //get the rowid
    auto line_index = table_widget->currentRow();
    auto rowid_item =  table_widget->item(line_index,0);
    Rowid rowid     = convert<Rowid>(rowid_item->text() );

    //delete in the database
    q_delline(rowid);

    //delete in the table
    table_widget->removeRow(line_index);

}

void TableWidget::on_modify(){
  //get the rowid
  auto line_index = table_widget->currentRow();
  auto rowid_item =  table_widget->item(line_index,0);
  Rowid rowid     = convert<Rowid>(rowid_item->text() );

  //set the database
  auto tr = tdb::transaction(db);
  for(size_t i=0; i < col_size(); ++i ){
      const std::string &col_name = col_vect[i]->name;
      widget_vect[i]->db_write(db,name,col_name,rowid);
  }
  tr.commit();

  //update table from database content
  for(size_t i=0; i < col_size(); ++i){
      QString text = col_vect[i]->db_read(db,name,rowid);
      table_widget->setItem(line_index,i+1, new_item(text) );
      line_index=table_widget->indexFromItem(rowid_item).row(); //see (1)
  }
  //(1) : IMPORTANT: as the table may be sorted, the newly added item, or the item after edition, may appears on a different row.
  //we, therefore need to update the line index



}

void TableWidget::on_create(){

    auto tr = tdb::transaction(db);
    auto rowid = q_newline();

    //set database content
    for(size_t i=0; i < col_size(); ++i ){
        const std::string &col_name = col_vect[i]->name;
        widget_vect[i]->db_write(db,name,col_name,rowid);
    }
    tr.commit();


    //create a new line in table_widget
    auto line_index = table_widget->rowCount();
    table_widget->setRowCount(line_index+1);
    auto rowid_item = new_item(rowid);
    table_widget->setItem(line_index,0, rowid_item );
    line_index=table_widget->indexFromItem(rowid_item).row(); //see (1)


    //get database content, write it in the new line
    for(size_t i=0; i < col_size(); ++i){
        QString text = col_vect[i]->db_read(db,name,rowid);
        table_widget->setItem(line_index,i+1, new_item(text) );
        line_index=table_widget->indexFromItem(rowid_item).row(); //see (1)
    }

    //(1) : IMPORTANT: as the table may be sorted, the newly added item, or the item after edition, may appears on a different row.
    //we, therefore need to update the line index

    mode_add();
    table_widget->scrollToItem(rowid_item);
    table_widget->clearSelection() ;

    //set the widgets to their default values
    for(size_t i=0; i < col_size(); ++i){
        widget_vect[i]->setDefault();
    }

    //if at least one widget, set focus
    if(col_size()>0){
        widget_vect[0]->setFocus();
    }




}



std::string TableWidget::sql_create()const{
    bool is_first=true;
    std::string s =  "CREATE TABLE IF NOT EXISTS \""+name+"\"(\n";
    for(const auto &c : col_vect){
        if(is_first)  {is_first=false; s+="   ";}
        else[[likely]]{s+="\n  ,";}
        s+=c->sql();
    }
    s+="\n);";
    return s;
}

/*
std::string TableWidget::sql_select_all()const{
    std::string s =  "SELECT cast(rowid as text) as rowid ";
    for(const auto &c : col_vect){
        s+=", cast(\""+c->name+"\" as text)";
    }
    s+="\n);";
    return s;
}
*/
