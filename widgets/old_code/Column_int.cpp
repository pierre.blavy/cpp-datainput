#include "Column_int.hpp"
#include <QLineEdit>

#include <qtt/qtt_layout.hpp>
#include <convert/to_QString.hpp>
#include <QMessageBox>

#include <config/config.hpp>
#include <config/convert/convert_lexical.hpp>
#include <widgets/IntValidator_t.hpp>



namespace{


class Column_widget_int_validator : public  IntValidator_t<Column_widget_int::value_type>  {

    public:
    explicit Column_widget_int_validator(QObject * parent = nullptr ): IntValidator_t<Column_widget_int::value_type>(parent){}
    virtual QValidator::State validate(QString &input, int &pos)const override final{

        //empty strings are valid NA
        input = input.trimmed();
        if(input==""){return QValidator::Acceptable ;}

        //NA are replaced by empty strings
        if(input.toLower()=="na"){
            input="";
            return QValidator::Acceptable;
        }

        //--- fallback to int validator ---
        return  IntValidator_t<Column_widget_int::value_type>::validate(input,pos);

    }

};

}


Column_widget_int::Column_widget_int(std::optional<value_type> min_v, std::optional<value_type> max_v, QWidget * parent):Column_widget(parent){
    auto main_layout = qtt::new_layout<QHBoxLayout>();
    this->setLayout(main_layout);
    qtt::cstr_in_layout(value_edit,main_layout);

    //this->connect(value_edit)
    Column_widget_int_validator* validator = new Column_widget_int_validator();

    //value_edit->setValidator(validator);
    if(min_v.has_value()){validator->setBottom(min_v.value());}
    if(max_v.has_value()){validator->setTop   (max_v.value());}

    translate(false);

}

void Column_widget_int::translate(bool){

}

void Column_widget_int::setFocus(){
    value_edit->setFocus();
}

void Column_widget_int::setDefault(){
    //value_edit->setText("");
    //TODO
}



auto Column_widget_int::value()const->std::optional<value_type>{
    QString s = value_edit->text().trimmed().toLower();
    if(s=="na"){return std::nullopt;}
    if(s==""){return std::nullopt;}

    try{
        return convert<value_type>(s);
    }catch(...){}

    QMessageBox msgBox(
        QMessageBox::Warning,
        tr("Error : wrong value"),
        tr("Wrong value : the string %1 cannot be converted to integer. It will be replaced by NA").arg(value_edit->text())
        );
    msgBox.setStandardButtons(QMessageBox::Ok);
    msgBox.setDefaultButton  (QMessageBox::Ok);
    msgBox.exec();

    return std::nullopt;
}


void Column_widget_int::setValue( std::optional<value_type> opt_int ){
    /*
    if(opt_int.has_value() ){
        value_edit->setText( convert<QString>( opt_int.value() )  );
    }else{
        value_edit->setText("");
    }*/
    //TODO
}



void Column_widget_int::db_write(
    tdb::Connection_t<tdb::Tag_sqlite>&db,
    const std::string &table_name,
    const std::string & column_name,
    tdb::Rowid<tdb::Tag_sqlite> rowid
    ){
    auto v=value();
    Column_widget::db_write(db,table_name,column_name,rowid, v );
}



void Column_widget_int::db_read(
    tdb::Connection_t<tdb::Tag_sqlite>&db,
    const std::string &table_name,
    const std::string & column_name,
    tdb::Rowid<tdb::Tag_sqlite> rowid
    ){

    auto opt_int = Column_widget::db_read<value_type>(db,table_name,column_name,rowid);
    setValue(opt_int);
}









Column_widget_int * Column_int::new_widget(QWidget*parent){
  return new Column_widget_int( min_v, max_v, parent);
}






Column_int::Column_int(const config::Block&conf):Column_abstract(conf){
    if(conf.values.size("min")!=0 ){ min_v=conf.values.get_unique<value_type>("min"); }
    if(conf.values.size("max")!=0 ){ max_v=conf.values.get_unique<value_type>("max"); }
}





std::string Column_int::sql()const{
    return "\""+name + "\" integer";
}
