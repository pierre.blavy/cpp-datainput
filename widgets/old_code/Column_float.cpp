#include "Column_float.hpp"

#include <qtt/qtt_layout.hpp>
#include <convert/to_QString.hpp>

#include <QDoubleSpinBox>
#include <QMessageBox>
#include <QPushButton>
#include <QSizePolicy>

#include <config/config.hpp>
#include <config/convert/convert_lexical.hpp>
#include <widgets/IntValidator_t.hpp>

#include <limits>

#include <tdb/tdb_sqlite.hpp>

class MyDoubleSpinBox:public QDoubleSpinBox{
    public:
    using QDoubleSpinBox::QDoubleSpinBox;
    double valueFromText(const QString &text) const override final{
        auto tmp=text.trimmed().toLower();
        if(tmp=="" or tmp=="na" or tmp == "nan"){return minimum();}//minimum is a magic value, meaning NA
        return QDoubleSpinBox::valueFromText(tmp);
    }
};



Column_widget_float::Column_widget_float(
      std::optional<value_type> min_v,
      std::optional<value_type> max_v,
      int decimals,
      QWidget * parent
 ):Column_widget(parent){


    auto main_layout = qtt::new_layout<QHBoxLayout>();
    this->setLayout(main_layout);
    qtt::cstr_in_layout(value_edit,main_layout);
    qtt::cstr_in_layout(nan_button,main_layout);

    //--- config value_edit ---
    value_edit->setDecimals(decimals);
    value_edit->setMinimum(std::numeric_limits<value_type>::lowest()+1 );
    value_edit->setMaximum(std::numeric_limits<value_type>::max() );


    if(min_v.has_value()){value_edit->setMinimum(min_v.value()-1);}
    if(max_v.has_value()){value_edit->setMaximum(max_v.value());}

    if(min_v.has_value()){min_str= convert<QString>(min_v.value());}else{min_str="nan";}
    if(max_v.has_value()){max_str= convert<QString>(max_v.value());}else{max_str="nan";}

    value_edit->setSpecialValueText("NA");
    value_edit->setValue(min_v.value()-1);  //this is a magic value meaning NA

    value_edit->setSizePolicy(QSizePolicy::Expanding,QSizePolicy::Minimum );


    //--- config nan_button ---
    nan_button->setSizePolicy(QSizePolicy::Minimum,QSizePolicy::Minimum );
    nan_button->setStyleSheet("padding: 3px;");

    //--- connect ---
    connect(nan_button,&QPushButton::clicked, this, [&](){value_edit->setValue(value_edit->minimum());} );//minimum is a magic value meaning NA


    translate(false);

}

void Column_widget_float::translate(bool){
    value_edit->setToolTip( tr("min:%1, max:%2").arg(min_str,max_str) );

    nan_button->setText( tr("NA") );
    nan_button->setToolTip( tr("Set to missing value"));

}

void Column_widget_float::setFocus(){
    value_edit->setFocus();
}

void Column_widget_float::setDefault(){
    setValue(std::nullopt);
}


auto Column_widget_float::value()const->std::optional<value_type>{
    if(value_edit->value() == (value_edit->minimum())){return std::nullopt;} //minimum is a magic value meaning NA
    return value_edit->value();
}


void Column_widget_float::setValue( std::optional<value_type> opt_int ){
    if(opt_int.has_value() ){
        value_edit->setValue( opt_int.value()  );
    }else{
        value_edit->setValue(value_edit->minimum() );//set to NA
    }

}

void Column_widget_float::db_write(
    tdb::Connection_t<tdb::Tag_sqlite>&db,
    const std::string &table_name,
    const std::string & column_name,
    tdb::Rowid<tdb::Tag_sqlite> rowid
    ){
    auto v=value();
    Column_widget::db_write(db,table_name,column_name,rowid, v );
}

void Column_widget_float::db_read(
    tdb::Connection_t<tdb::Tag_sqlite>&db,
    const std::string &table_name,
    const std::string & column_name,
    tdb::Rowid<tdb::Tag_sqlite> rowid
    ){

    auto opt_value = Column_widget::db_read<value_type>(db,table_name,column_name,rowid);
    setValue(opt_value);
}









Column_widget_float * Column_float::new_widget(QWidget*parent){
  return new Column_widget_float( min_v, max_v, decimals, parent);
}






Column_float::Column_float(const config::Block&conf):Column_abstract(conf){
    if(conf.values.size("min")!=0 )     { min_v=conf.values.get_unique<value_type>("min"); }
    if(conf.values.size("max")!=0 )     { max_v=conf.values.get_unique<value_type>("max"); }
    if(conf.values.size("decimals")!=0 ){ decimals=conf.values.get_unique<int>("decimals"); }
}





std::string Column_float::sql()const{
    return "\""+name + "\" real";
}
