#ifndef COLUMN_FLOAT_PIERRE_HPP
#define COLUMN_FLOAT_PIERRE_HPP

#include <optional>

#include "Column_abstract.hpp"
namespace config{class Block;}

class MyDoubleSpinBox;
class QPushButton;

class Column_widget_float:public Column_widget{

    public:
    typedef double value_type;
    static constexpr value_type NaN=std::numeric_limits<value_type>::quiet_NaN();


    Column_widget_float(std::optional<value_type> min_v, std::optional<value_type> max_v, int decumals, QWidget* parent=nullptr);
    MyDoubleSpinBox * value_edit=nullptr;
    QPushButton     * nan_button=nullptr;

    virtual void db_write(tdb::Connection_t<tdb::Tag_sqlite>&db, const std::string &table_name, const std::string & column_name, tdb::Rowid<tdb::Tag_sqlite> rowid)override final;
    virtual void db_read (tdb::Connection_t<tdb::Tag_sqlite>&db, const std::string &table_name, const std::string & column_name, tdb::Rowid<tdb::Tag_sqlite> rowid)override final;
    virtual void setFocus() override final;
    virtual void setDefault()override final;


    virtual void translate(bool=true)override final;

    std::optional<value_type> value()const;
    void setValue( std::optional<value_type> );

    private slots:
    void on_change();

    QString max_str;
    QString min_str;


};

class Column_float: public Column_abstract{

  public:
  typedef double value_type;
  explicit  Column_float(const config::Block&);

  std::optional<value_type> min_v;
  std::optional<value_type> max_v;
  int    decimals=2;

  Column_widget_float * new_widget(QWidget*parent=nullptr) override final;
  std::string sql()const override final;

};








#endif 
