<?xml version="1.0" encoding="utf-8"?>
<!DOCTYPE TS>
<TS version="2.1">
<context>
    <name>Column_datetime_widget</name>
    <message>
        <location filename="../widgets/columns/Column_datetime.cpp" line="36"/>
        <source>Error : wrong value</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../widgets/columns/Column_datetime.cpp" line="37"/>
        <source>Wrong value : the string %1 cannot be converted to date time. It will be replaced by NA</source>
        <translation type="unfinished"></translation>
    </message>
</context>
<context>
    <name>Column_enum_widget</name>
    <message>
        <location filename="../widgets/columns/Column_enum.cpp" line="58"/>
        <source>Enum value</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../widgets/columns/Column_enum.cpp" line="64"/>
        <source>Error : wrong enum value</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../widgets/columns/Column_enum.cpp" line="65"/>
        <source>Wrong enum value. The input %1 will be replaced by %2 </source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../widgets/columns/Column_enum.cpp" line="66"/>
        <source>Error : invalid value %1, please use %2</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../widgets/columns/Column_enum.cpp" line="87"/>
        <source>Invalid value interpreted as </source>
        <translation type="unfinished"></translation>
    </message>
</context>
<context>
    <name>Column_float_widget</name>
    <message>
        <location filename="../widgets/columns/Column_float.cpp" line="54"/>
        <source>NA</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../widgets/columns/Column_float.cpp" line="55"/>
        <source>Set to missing value</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../widgets/columns/Column_float.cpp" line="57"/>
        <source>Float value</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../widgets/columns/Column_float.cpp" line="60"/>
        <source>Minimum = %1</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../widgets/columns/Column_float.cpp" line="64"/>
        <source>Maximum = %1</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../widgets/columns/Column_float.cpp" line="67"/>
        <source>Any float</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../widgets/columns/Column_float.cpp" line="71"/>
        <source>Error : wrong value</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../widgets/columns/Column_float.cpp" line="72"/>
        <source>Wrong value : %1</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../widgets/columns/Column_float.cpp" line="81"/>
        <source>Empty text, please use NA for missing values</source>
        <comment>Don&apos;t translate NA, it&apos;s a magic value</comment>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../widgets/columns/Column_float.cpp" line="92"/>
        <source>Wrong value : the string %1 cannot be converted to float.</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../widgets/columns/Column_float.cpp" line="99"/>
        <source>The value is too small. Value=%1, min=%2.</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../widgets/columns/Column_float.cpp" line="105"/>
        <source>The value is too large. Value=%1, max=%2.</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../widgets/columns/Column_float.cpp" line="126"/>
        <source>Value is NA</source>
        <translation type="unfinished"></translation>
    </message>
</context>
<context>
    <name>Column_int_widget</name>
    <message>
        <location filename="../widgets/columns/Column_int.cpp" line="56"/>
        <source>NA</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../widgets/columns/Column_int.cpp" line="57"/>
        <source>Set to missing value</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../widgets/columns/Column_int.cpp" line="59"/>
        <source>Integer value</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../widgets/columns/Column_int.cpp" line="62"/>
        <source>Minimum = %1</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../widgets/columns/Column_int.cpp" line="66"/>
        <source>Maximum = %1</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../widgets/columns/Column_int.cpp" line="69"/>
        <source>Any integer</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../widgets/columns/Column_int.cpp" line="73"/>
        <source>Error : wrong value</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../widgets/columns/Column_int.cpp" line="74"/>
        <source>Wrong value : %1</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../widgets/columns/Column_int.cpp" line="82"/>
        <source>Empty text, please use NA for missing values</source>
        <comment>Don&apos;t translate NA, it&apos;s a magic value</comment>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../widgets/columns/Column_int.cpp" line="93"/>
        <source>Wrong value : the string %1 cannot be converted to integer.</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../widgets/columns/Column_int.cpp" line="100"/>
        <source>The value is too small. Value=%1, min=%2.</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../widgets/columns/Column_int.cpp" line="106"/>
        <source>The value is too large. Value=%1, max=%2.</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../widgets/columns/Column_int.cpp" line="127"/>
        <source>Value is NA</source>
        <translation type="unfinished"></translation>
    </message>
</context>
<context>
    <name>Column_string_validator</name>
    <message>
        <location filename="../widgets/columns/Column_string.cpp" line="28"/>
        <source>String is too short, size=%1, min_size=%2.</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../widgets/columns/Column_string.cpp" line="29"/>
        <source>String is too long, size=%1, max_size=%2.</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../widgets/columns/Column_string.cpp" line="30"/>
        <source>Regex mismatch, regex=%1.</source>
        <translation type="unfinished"></translation>
    </message>
</context>
<context>
    <name>Column_string_widget</name>
    <message>
        <location filename="../widgets/columns/Column_string.cpp" line="130"/>
        <source>Not trimmed</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../widgets/columns/Column_string.cpp" line="136"/>
        <source>Trimmed</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../widgets/columns/Column_string.cpp" line="142"/>
        <source>Max size : %1</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../widgets/columns/Column_string.cpp" line="148"/>
        <source>Min size : %1</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../widgets/columns/Column_string.cpp" line="154"/>
        <source>Lowercase only</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../widgets/columns/Column_string.cpp" line="160"/>
        <source>Uppercase only</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../widgets/columns/Column_string.cpp" line="166"/>
        <source>Regex : %1</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../widgets/columns/Column_string.cpp" line="172"/>
        <source>NA</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../widgets/columns/Column_string.cpp" line="173"/>
        <source>Check this box if string is NA</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../widgets/columns/Column_string.cpp" line="180"/>
        <source>Error : wrong value</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../widgets/columns/Column_string.cpp" line="181"/>
        <source>Wrong value : the string %1 is not valid. It will be replaced by NA.

%2</source>
        <translation type="unfinished"></translation>
    </message>
</context>
<context>
    <name>DateTime</name>
    <message>
        <location filename="../widgets/DateTime.cpp" line="114"/>
        <source>Now</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../widgets/DateTime.cpp" line="115"/>
        <source>Set date and time to the current date. Keyboard shortcut Ctrl+T</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../widgets/DateTime.cpp" line="117"/>
        <source>NA</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../widgets/DateTime.cpp" line="118"/>
        <source>Set to NA</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../widgets/DateTime.cpp" line="126"/>
        <source>Available timezones:%1</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../widgets/DateTime.cpp" line="171"/>
        <source>Invalid date format</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../widgets/DateTime.cpp" line="259"/>
        <source>Invalid timezone, timezone=%1</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../widgets/DateTime.cpp" line="265"/>
        <source>Invalid date</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../widgets/DateTime.cpp" line="271"/>
        <source>Invalid time</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../widgets/DateTime.cpp" line="290"/>
        <source>Error=%1</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../widgets/DateTime.cpp" line="297"/>
        <location filename="../widgets/DateTime.cpp" line="311"/>
        <source>Value=%1</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../widgets/DateTime.cpp" line="298"/>
        <source>UTC=%1</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../widgets/DateTime.cpp" line="299"/>
        <source>timestamp=%1</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../widgets/DateTime.cpp" line="310"/>
        <source>Datetime is NA</source>
        <translation type="unfinished"></translation>
    </message>
</context>
<context>
    <name>Filepath</name>
    <message>
        <location filename="../widgets/Filepath.cpp" line="36"/>
        <source>Browse...</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../widgets/Filepath.cpp" line="56"/>
        <source>Chose a file that exists</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../widgets/Filepath.cpp" line="57"/>
        <source>Choose a new file</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../widgets/Filepath.cpp" line="58"/>
        <source>Choose a file</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../widgets/Filepath.cpp" line="62"/>
        <source>Chose a directory that exists</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../widgets/Filepath.cpp" line="63"/>
        <source>Choose a new directory</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../widgets/Filepath.cpp" line="64"/>
        <source>Choose a directory</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../widgets/Filepath.cpp" line="96"/>
        <source>A regular file is expected</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../widgets/Filepath.cpp" line="97"/>
        <source>A directory is expected</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../widgets/Filepath.cpp" line="101"/>
        <source>The file must exist</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../widgets/Filepath.cpp" line="102"/>
        <source>The file must NOT exist</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../widgets/Filepath.cpp" line="103"/>
        <source>The file already exist</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../widgets/Filepath.cpp" line="107"/>
        <source>The directory must exist</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../widgets/Filepath.cpp" line="108"/>
        <source>The directory must NOT exist</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../widgets/Filepath.cpp" line="109"/>
        <source>The directory already exist</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../widgets/Filepath.cpp" line="132"/>
        <source>A regular file that exists is expected.</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../widgets/Filepath.cpp" line="133"/>
        <source>A regular file that do NOT exist is expected.</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../widgets/Filepath.cpp" line="134"/>
        <source>A regular file is expected.</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../widgets/Filepath.cpp" line="136"/>
        <source>File path:</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../widgets/Filepath.cpp" line="140"/>
        <source>A directory that exists is expected.</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../widgets/Filepath.cpp" line="141"/>
        <source>A directory that do NOT exist is expected.</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../widgets/Filepath.cpp" line="142"/>
        <source>A directory is expected.</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../widgets/Filepath.cpp" line="143"/>
        <source>Directory path:</source>
        <translation type="unfinished"></translation>
    </message>
</context>
<context>
    <name>MainWindow</name>
    <message>
        <location filename="../widgets/MainWindow.cpp" line="77"/>
        <source>&lt;Export&gt;</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../widgets/MainWindow.cpp" line="81"/>
        <source>&lt;Tools&gt;</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../widgets/MainWindow.cpp" line="91"/>
        <source>Input editor</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../widgets/MainWindow.cpp" line="93"/>
        <source>&amp;Export data</source>
        <comment>Export data tab, use E as shortcut</comment>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../widgets/MainWindow.cpp" line="94"/>
        <source>&amp;Tools</source>
        <comment>Tools tabs, use T as shortcut</comment>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../widgets/MainWindow.cpp" line="129"/>
        <source>Error : cannot save</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../widgets/MainWindow.cpp" line="130"/>
        <source>Errors:
</source>
        <translation type="unfinished"></translation>
    </message>
</context>
<context>
    <name>SaveData</name>
    <message>
        <location filename="../widgets/SaveData.cpp" line="85"/>
        <source>Save directory</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../widgets/SaveData.cpp" line="86"/>
        <source>Tables</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../widgets/SaveData.cpp" line="87"/>
        <source>Checked tables will be saved</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../widgets/SaveData.cpp" line="130"/>
        <source>Nothing to save</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../widgets/SaveData.cpp" line="131"/>
        <source>Please, check at least one table</source>
        <translation type="unfinished"></translation>
    </message>
    <message numerus="yes">
        <location filename="../widgets/SaveData.cpp" line="134"/>
        <source>Save %n table(s)</source>
        <translation type="unfinished">
            <numerusform></numerusform>
        </translation>
    </message>
    <message>
        <location filename="../widgets/SaveData.cpp" line="140"/>
        <source>Invalid directory</source>
        <comment>must be short</comment>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../widgets/SaveData.cpp" line="141"/>
        <source>Invalid save directory</source>
        <comment>can be long</comment>
        <translation type="unfinished"></translation>
    </message>
</context>
<context>
    <name>TableWidget</name>
    <message>
        <location filename="../widgets/TableWidget.cpp" line="63"/>
        <source>Delete</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../widgets/TableWidget.cpp" line="64"/>
        <source>Modify</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../widgets/TableWidget.cpp" line="65"/>
        <source>Add new line</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../widgets/TableWidget.cpp" line="67"/>
        <source>Mode</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../widgets/TableWidget.cpp" line="69"/>
        <source>Add</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../widgets/TableWidget.cpp" line="70"/>
        <source>Modify/Delete</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../widgets/TableWidget.cpp" line="368"/>
        <source>Cloned from rowid:%1</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../widgets/TableWidget.cpp" line="371"/>
        <source>Edit rowid:%1</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../widgets/TableWidget.cpp" line="377"/>
        <source>New line</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../widgets/TableWidget.cpp" line="379"/>
        <source>No line selected</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../widgets/TableWidget.cpp" line="610"/>
        <source>Cannot write to file %1</source>
        <translation type="unfinished"></translation>
    </message>
</context>
<context>
    <name>Tool_datetime</name>
    <message>
        <location filename="../widgets/Tools.cpp" line="106"/>
        <source>Format</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../widgets/Tools.cpp" line="107"/>
        <source>Timestamp</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../widgets/Tools.cpp" line="108"/>
        <location filename="../widgets/Tools.cpp" line="117"/>
        <source>seconds</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../widgets/Tools.cpp" line="109"/>
        <location filename="../widgets/Tools.cpp" line="118"/>
        <source>milliseconds</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../widgets/Tools.cpp" line="111"/>
        <source>Dates and time</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../widgets/Tools.cpp" line="112"/>
        <source>year</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../widgets/Tools.cpp" line="113"/>
        <source>month</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../widgets/Tools.cpp" line="114"/>
        <source>day</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../widgets/Tools.cpp" line="115"/>
        <source>hours</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../widgets/Tools.cpp" line="116"/>
        <source>minutes</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../widgets/Tools.cpp" line="120"/>
        <source>Timezone</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../widgets/Tools.cpp" line="124"/>
        <source>Input config</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../widgets/Tools.cpp" line="125"/>
        <source>Output config</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../widgets/Tools.cpp" line="133"/>
        <source>Error_string</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../widgets/Tools.cpp" line="134"/>
        <source>Output this string when the input is invalid</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../widgets/Tools.cpp" line="136"/>
        <source>Input data</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../widgets/Tools.cpp" line="137"/>
        <source>Output data</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../widgets/Tools.cpp" line="139"/>
        <source>Ok</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../widgets/Tools.cpp" line="140"/>
        <source>Errors</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../widgets/Tools.cpp" line="141"/>
        <source>Empy</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../widgets/Tools.cpp" line="143"/>
        <source>Number of lines correctly converted</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../widgets/Tools.cpp" line="144"/>
        <source>Number of lines with an error</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../widgets/Tools.cpp" line="145"/>
        <source>Number of empty lines</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../widgets/Tools.cpp" line="147"/>
        <source>Convert and copy</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../widgets/Tools.cpp" line="216"/>
        <source>Timestamp in &lt;b&gt;seconds&lt;/b&gt;</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../widgets/Tools.cpp" line="222"/>
        <source>Timestamp in &lt;/b&gt;milliseconds&lt;/b&gt;</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../widgets/Tools.cpp" line="230"/>
        <source>unused</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../widgets/Tools.cpp" line="243"/>
        <source>Empty string</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../widgets/Tools.cpp" line="260"/>
        <source>Nothing related to a date</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../widgets/Tools.cpp" line="265"/>
        <source>Strange format, use MM for month, not mm</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../widgets/Tools.cpp" line="271"/>
        <source>Strange format, use z for milliseconds, not zz or zzz</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../widgets/Tools.cpp" line="276"/>
        <source>Strange format, use z for milliseconds, not ms</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../widgets/Tools.cpp" line="281"/>
        <source>yes</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../widgets/Tools.cpp" line="282"/>
        <source>no</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../widgets/Tools.cpp" line="288"/>
        <source>Datetime</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../widgets/Tools.cpp" line="289"/>
        <source>Year</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../widgets/Tools.cpp" line="290"/>
        <source>Month</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../widgets/Tools.cpp" line="291"/>
        <source>Day</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../widgets/Tools.cpp" line="292"/>
        <source>Hours</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../widgets/Tools.cpp" line="293"/>
        <source>Minutes</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../widgets/Tools.cpp" line="294"/>
        <source>Seconds</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../widgets/Tools.cpp" line="295"/>
        <source>Milliseconds</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../widgets/Tools.cpp" line="318"/>
        <source>Invalid timezone</source>
        <translation type="unfinished"></translation>
    </message>
</context>
<context>
    <name>Tools</name>
    <message>
        <location filename="../widgets/Tools.cpp" line="412"/>
        <source>Datetime</source>
        <translation type="unfinished"></translation>
    </message>
</context>
</TS>
