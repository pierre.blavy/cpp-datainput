#ifndef VALIDATE_IS_NAME_PIERRE_HPP
#define VALIDATE_IS_NAME_PIERRE_HPP

#include <string>


//A name starts with a letter, and contains only letters, digits, and _
//A name can be safely used as table or column name to forge a sql query

namespace validate{
  bool is_name(const std::string &s);
}

#endif
