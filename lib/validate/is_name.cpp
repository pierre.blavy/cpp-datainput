#include "is_name.hpp"

#include <regex>

namespace validate{
  bool is_name(const std::string &s){
    static const std::regex re_name("[a-zA_Z][a-zA-Z0-9_]*");
    return regex_match(s,re_name);
  }
}
