#ifndef OKNANERR_T_HPP
#define OKNANERR_T_HPP

#include <variant>
#include <optional>
#include <QString>

#include <string>
#include <algorithm>
#include <convert/convert.hpp>
#include <config/convert/convert_lexical.hpp>


template<typename T, typename S>
struct OkNanErr_t{
    class Nan_t{};
    typedef T value_type;
    typedef S error_type;
    //Warning S can be the same as T, so use indexe 0,1,2 in variant template parameters instead of Nan_t, T, S


    OkNanErr_t(){set_error("undef");}
    OkNanErr_t(const OkNanErr_t&)=default;
    OkNanErr_t& operator=(const OkNanErr_t&)=default;
    OkNanErr_t(OkNanErr_t&&)=default;
    OkNanErr_t& operator=(OkNanErr_t&&)=default;

    bool is_nan()  const noexcept{return m_value.index()==0;}
    bool is_value()const noexcept{return m_value.index()==1;}
    bool is_error()const noexcept{return m_value.index()==2;}

    void set_nan()           noexcept{m_value.template emplace<0>(Nan_t());}
    void set_value(const T&t)noexcept{m_value.template emplace<1>(t);}
    void set_error(const S&s)noexcept{m_value.template emplace<2>(s);}
    void set_optional(const std::optional<T>&t)noexcept;

    void     get_nan()  const{std::get<0>(m_value);}  //may throw
    const T& get_value()const{return std::get<1>(m_value);}      //may throw
    const S& get_error()const{return std::get<2>(m_value);}//may throw
    std::optional<T> get_optional()const;//may throw

    static OkNanErr_t nan()           {OkNanErr_t r; r.set_nan(); return r;}
    static OkNanErr_t value(const T&t){OkNanErr_t r; r.set_value(t); return r;}
    static OkNanErr_t error(const S&s){OkNanErr_t r; r.set_error(s); return r;}
    static OkNanErr_t optional(const std::optional<T>&t){OkNanErr_t r; r.set_optional(t); return r;}

private:
    std::variant<Nan_t,T,S> m_value;
    template<typename T1, typename S1> friend bool operator == (const OkNanErr_t<T1,S1>&a, const OkNanErr_t<T1,S1> &b );
    template<typename T1, typename S1> friend bool operator != (const OkNanErr_t<T1,S1>&a, const OkNanErr_t<T1,S1> &b );
};



template<typename T, typename S>
bool operator == (const OkNanErr_t<T,S>&a, const OkNanErr_t<T,S> &b ){
    if(a.m_value.index()!=b.m_value.index()){return false;}
    switch(a.m_value.index()){
    case 0: return true; //both are NAN
    case 1: {const T&x= std::get<1>(a.m_value); const T&y= std::get<1>(b.m_value); return x==y; }
    case 2: {const S&x= std::get<2>(a.m_value); const S&y= std::get<2>(b.m_value); return x==y; }
    }
    throw std::logic_error("Error in OkNanErr_t::operator==");
}


template<typename T, typename S>
bool operator != (const OkNanErr_t<T,S>&a, const OkNanErr_t<T,S> &b ){
    return !(a==b);
}



template<typename T, typename S>
void OkNanErr_t<T,S>::set_optional(const std::optional<T>&t)noexcept{
    if(t.has_value()){set_value(t.value());}
    else{set_nan();}
}


template<typename T, typename S>
std::optional<T> OkNanErr_t<T,S>::get_optional()const{
    if(is_nan() ){return std::nullopt;}
    else{         return std::get<1>(m_value);}
}








template<typename T>
struct ValueNanEmpty_t{
    class Nan_t{};
    class Empty_t{};
    typedef T value_type;


    ValueNanEmpty_t(){set_empty();}
    ValueNanEmpty_t(const value_type &v){set_value(v);}

    ValueNanEmpty_t(const ValueNanEmpty_t&)=default;
    ValueNanEmpty_t& operator=(const ValueNanEmpty_t&)=default;
    ValueNanEmpty_t(ValueNanEmpty_t&&)=default;
    ValueNanEmpty_t& operator=(ValueNanEmpty_t&&)=default;

    bool is_nan()  const noexcept{return m_value.index()==0;}
    bool is_empty()const noexcept{return m_value.index()==1;}
    bool is_value()const noexcept{return m_value.index()==2;}

    void set_nan()   noexcept{m_value.template emplace<0>(Nan_t());}
    void set_empty() noexcept{m_value.template emplace<1>(Empty_t());}
    void set_value(const T&t)noexcept{m_value.template emplace<2>(t);}

    void     get_nan()  const{       std::get<0>(m_value);}  //may throw
    void     get_empty()const{       std::get<1>(m_value);}  //may throw
    const T& get_value()const{return std::get<2>(m_value);}  //may throw

    static ValueNanEmpty_t nan()           {ValueNanEmpty_t r; r.set_nan(); return r;}
    static ValueNanEmpty_t empty()         {ValueNanEmpty_t r; r.set_nan(); return r;}
    static ValueNanEmpty_t value(const T&t){ValueNanEmpty_t r; r.set_value(t); return r;}

    template<typename Context_t=void>
    void from_string(std::string s){
        if(s==""){set_empty(); return;}


        std::transform(s.begin(), s.end(), s.begin(), [](unsigned char c){ return std::tolower(c); });
        if(s=="na" or s=="nan") {set_nan();return;}
        if(s=="empty"){set_empty(); return;}

        set_value(convert<T,Context_t>(s));

    }


    private:
    std::variant<Nan_t,Empty_t,T> m_value;
    template<typename T1, typename S1> friend bool operator == (const OkNanErr_t<T1,S1>&a, const OkNanErr_t<T1,S1> &b );
    template<typename T1, typename S1> friend bool operator != (const OkNanErr_t<T1,S1>&a, const OkNanErr_t<T1,S1> &b );
};






template<typename T>
bool operator == (const ValueNanEmpty_t<T>&a, const ValueNanEmpty_t<T> &b ){
    if(a.m_value.index()!=b.m_value.index()){return false;}
    switch(a.m_value.index()){
    case 0: return true; //both are NAN
    case 1: return true; //both are Empty
    case 2: {const T&x= std::get<2>(a.m_value); const T&y= std::get<2>(b.m_value); return x==y; }
    }
    throw std::logic_error("Error in ValueNanEmpty_t::operator==");
}


template<typename T>
bool operator != (const ValueNanEmpty_t<T>&a, const ValueNanEmpty_t<T> &b ){
    return !(a==b);
}







#endif // OKNANERR_T_HPP
