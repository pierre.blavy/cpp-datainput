#ifndef STR_TO_DIGITS_HPP
#define STR_TO_DIGITS_HPP

#include <type_traits>
#include <convert/convert.hpp>


namespace str{


template<typename String_t, typename Context=void, typename T, typename I>
String_t to_digits(const T&t, I n){
    static_assert(std::is_integral_v<I>, "n (number of digits) must be an integral type like int or size_t" );
    String_t r = convert<String_t,Context>(t); //error here, you need to include something that can convert T to String_t, or you need to do this convertion in the caller
    while(r.size()<n){r='0'+r;}
    return r;
}




}

#endif
