#ifndef QTT_ICON_OR_TEXT_HPP
#define QTT_ICON_OR_TEXT_HPP

#include <QString>
#include <QIcon>

#include <QDebug>

namespace qtt{

static inline size_t count_error_icon_or_text=0;

template<typename Widget_t>
void icon_or_text(
    Widget_t *widget,
    const QString& path,
    const QString& text,
    int w=16,
    int h=16
){
    try{
        auto i = QIcon(path);
        if(i.isNull()){throw std::runtime_error("no icon to load");};
        if(i.availableSizes().size()==0){throw std::runtime_error("cannot load icon");};
        widget->setIcon(std::move(i));
        widget->setIconSize(QSize(w, h));
        widget->setText("");
    }catch(std::exception &e){
        widget->setText(text);
        if(count_error_icon_or_text>10){return;}
        ++ count_error_icon_or_text;
        qCritical()<<"Error loading icon, icon="<<path<<", error="<<e.what() ;
        if(count_error_icon_or_text==10){qCritical()<<"Too many icon errors";}
    }
}



template<typename Widget_t>
void icon_and_text(
    Widget_t *widget,
    const QString& path,
    const QString& text,
    const QString& alternate="",
    int w=16,
    int h=16
    ){
    try{
        auto i = QIcon(path);
        if(i.isNull()){throw std::runtime_error("no icon to load");};
        if(i.availableSizes().size()==0){throw std::runtime_error("cannot load icon");};
        widget->setIcon(std::move(i));
        widget->setIconSize(QSize(w, h));
        widget->setText(text);
    }catch(std::exception &e){
        widget->setText(alternate + " " + text);
        if(count_error_icon_or_text>10){return;}
        ++ count_error_icon_or_text;
        qCritical()<<"Error loading icon, icon="<<path<<", error="<<e.what() ;
        if(count_error_icon_or_text==10){qCritical()<<"Too many icon errors";}
    }
}



}



#endif // QTT_ICON_OR_TEXT_HPP
