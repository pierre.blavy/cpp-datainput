#ifndef LIB_CONFIG_CONVERT_CHRONO_HPP_
#define LIB_CONFIG_CONVERT_CHRONO_HPP_

#include <convert/convert.hpp>

#include <string>
#include <QString>

namespace config{
  struct Config_tag;
}

template<>
struct Convert_t<QString,std::string,config::Config_tag>{
	typedef QString     To_t;
	typedef std::string From_t;

	static To_t run(const From_t &s){
        return QString::fromStdString(s);
	}
};



#endif /* LIB_CONFIG_CONVERT_CHRONO_HPP_ */
