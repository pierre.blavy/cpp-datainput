

#ifndef LIB_CONFIG_CONVERT_ID_HPP_
#define LIB_CONFIG_CONVERT_ID_HPP_



#include <convert/convert.hpp>
#include <Id.hpp>

#include <string>
#include <chrono>

namespace config{
  struct Config_tag;
}



template<typename Value_t, typename Tag_t, bool has_arithmetic_b >
struct Convert_t<Id_t<Value_t,Tag_t,has_arithmetic_b> ,std::string,config::Config_tag>{
	typedef Id_t<Value_t,Tag_t,has_arithmetic_b> To_t;
	typedef std::string From_t;
	typedef config::Config_tag Context_t;

	static To_t run(const From_t &s){
		typedef typename To_t::value_type value_type;
		value_type v= Convert_t<value_type, From_t,Context_t >::run(s);
		return To_t::from_value(v);
	}
};





#endif /* LIB_CONFIG_CONVERT_ID_HPP_ */
