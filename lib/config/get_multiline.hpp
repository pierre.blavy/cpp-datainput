#ifndef GET_MULTILINE_HPP
#define GET_MULTILINE_HPP

#include "config.hpp"

namespace config{
inline std::string get_multiline(const Block&conf, const std::string &key, bool required=false){

    //throw if required and value missing
    if(required){
        if(conf.values.size(key)==0){throw std::runtime_error("Missing value, key="+key+" at "+conf.debug_str());}
    }

    std::string r;
    bool is_first=true;
    for(const auto &x: conf.values.crange(key)){
        if(is_first)  {is_first=false;}
        else[[likely]]{r+="\n";}
        r+=x.value;
    }
    return r;
}
}


#endif // GET_MULTILINE_HPP
