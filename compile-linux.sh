#!/bin/sh

OUT_PATH='linux-datainput/'

rm -rf "$OUT_PATH"     &&\
mkdir -p "$OUT_PATH"  &&\
cd "$OUT_PATH"  &&\
qmake ../  &&\
make -j$(nproc) &&\
make clean  &&\
rm Makefile




