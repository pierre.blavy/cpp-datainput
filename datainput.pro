QT += widgets
CONFIG += icu
  #important windows doesn't support timezones correctly (most timezones are lacking, and the exiting ones are buggy)
  #this setting uses libicu to avoid this problem
TEMPLATE=app


#Translate
#  /usr/lib64/qt6/bin/lupdate ./datainput.pro
#  /usr/lib64/qt6/bin/linguist  translations/fr.ts
#  /usr/lib64/qt6/bin/lrelease ./datainput.pro

TRANSLATIONS += \
    translations/en.ts \
    translations/fr.ts

copy_files.commands += echo post link stuff; \
    $${QMAKE_MKDIR} $$shell_path($${OUT_PWD}/translations); \
    $(COPY_FILE)    $$shell_path($$PWD/translations/*.qm)  $$shell_path($${OUT_PWD}/translations/) ; \
    $${QMAKE_MKDIR} $$shell_path($${OUT_PWD}/in); \
    $(COPY_FILE)    $$shell_path($$PWD/in/*)  $$shell_path($${OUT_PWD}/in/) ; \

QMAKE_EXTRA_TARGETS += copy_files
QMAKE_POST_LINK     += make copy_files

CONFIG += qt -std=c++23

QMAKE_CXXFLAGS += -O0 -g

INCLUDEPATH += ./lib


# Dependancies
#linux :
#win32 : dnf -y install  mingw32-boost mingw32-boost-static
#win64 : dnf -y install  mingw64-boost mingw64-boost-static

HEADERS += \
./lib/config/config.hpp \
./lib/config/convert/convert_bool.hpp \
./lib/config/convert/convert_chrono.hpp \
./lib/config/convert/convert_id.hpp \
./lib/config/convert/convert_lexical.hpp \
./lib/config/convert/convert_path.hpp \
./lib/config/convert/convert_unit_lite.hpp \
./lib/config/convert/convert_QString.hpp \
./lib/config/convert/string_cstring.hpp \
./lib/container/array.hpp \
./lib/container/container.hpp \
./lib/container/deque.hpp \
./lib/container/impl/Complexity.hpp \
./lib/container/set.hpp \
./lib/container/unordered_set.hpp \
./lib/container/vector.hpp \
./lib/convert/convert.hpp \
./lib/convert/string_cstring.hpp \
./lib/tdb/bind/sqlite_Id.hpp \
./lib/tdb/bind/sqlite_chrono.hpp \
./lib/tdb/functors/Fn_create.hpp \
./lib/tdb/functors/Fn_execute.hpp \
./lib/tdb/functors/Fn_foreach.hpp \
./lib/tdb/functors/Fn_function.hpp \
./lib/tdb/functors/Fn_get_column.hpp \
./lib/tdb/functors/Fn_get_row_optional.hpp \
./lib/tdb/functors/Fn_get_row_unique.hpp \
./lib/tdb/functors/Fn_get_table.hpp \
./lib/tdb/functors/Fn_get_value_optional.hpp \
./lib/tdb/functors/Fn_get_value_unique.hpp \
./lib/tdb/functors/Fn_insert.hpp \
./lib/tdb/functors/all.hpp \
./lib/tdb/functors/impl/Foreach.hpp \
./lib/tdb/functors/impl/is_iterator.hpp \
./lib/tdb/helpers/Mutex_do_nothing.hpp \
./lib/tdb/helpers/select_type.hpp \
./lib/tdb/helpers/tuple_ref.hpp \
./lib/tdb/tdb.hpp \
./lib/tdb/tdb_psql.hpp \
./lib/tdb/tdb_sqlite.hpp \
./lib/qtt/qtt.hpp \
./lib/qtt/qtt_icon_or_text.hpp \
./lib/qtt/qtt_layout.hpp \
./lib/qtt/qtt_Translatable.hpp \
./lib/validate/is_name.hpp \
./widgets/MainWindow.hpp \
./widgets/TableWidget.hpp \
./lib/config/get_multiline.hpp \
./lib/convert/from_QString.hpp \
./lib/convert/to_QString.hpp \
./widgets/IntValidator_t.hpp \
./widgets/IsOk.hpp \
./widgets/DateTime.hpp \
./widgets/columns/Column_abstract.hpp \
./widgets/columns/Column_factory.hpp \
./widgets/columns/Column_enum.hpp \
./widgets/columns/Column_int.hpp \
./widgets/columns/Column_float.hpp \
./widgets/columns/Column_string.hpp \
./widgets/columns/Column_datetime.hpp \
    lib/OkNanErr_t.hpp \
    lib/convert/QDateTime_to_QString.hpp \
    lib/convert/QDateTime_to_string.hpp \
    lib/convert/to_string.hpp \
    widgets/Filepath.hpp \
    widgets/SaveData.hpp \
./lib/str/to_digits.hpp \
    widgets/Tools.hpp




SOURCES += \
./main.cpp \
./lib/config/config.cpp \
./lib/tdb/tdb_sqlite.cpp \
./lib/validate/is_name.cpp \
./widgets/MainWindow.cpp \
./widgets/TableWidget.cpp \
./widgets/IsOk.cpp \
./widgets/DateTime.cpp \
./widgets/columns/Column_factory.cpp \
./widgets/columns/Column_abstract.cpp \
./widgets/columns/Column_enum.cpp \
./widgets/columns/Column_int.cpp \
./widgets/columns/Column_float.cpp \
./widgets/columns/Column_string.cpp \
./widgets/columns/Column_datetime.cpp \
    widgets/Filepath.cpp \
    widgets/SaveData.cpp \
    widgets/Tools.cpp



LIBS += \
-lz  \
-lsqlite3  \

unix {
  LIBS += \
  -ldl  \
  -lpthread \
}


win32{
  #win32 actualy means any windows, not 32 bits windows

  contains(QT_ARCH, i386) { #32 bits
  message(WIN32: $$QT_ARCH )

#  copy_files.commands += echo "WIN32 copy dll";
  copy_files.commands += $(COPY_FILE) "/usr/i686-w64-mingw32/sys-root/mingw/bin/libbz2-1.dll"        $$shell_path($${OUT_PWD}/);
  copy_files.commands += $(COPY_FILE) "/usr/i686-w64-mingw32/sys-root/mingw/bin/iconv.dll"        $$shell_path($${OUT_PWD}/);
  copy_files.commands += $(COPY_FILE) "/usr/i686-w64-mingw32/sys-root/mingw/bin/icudata71.dll"       $$shell_path($${OUT_PWD}/);
  copy_files.commands += $(COPY_FILE) "/usr/i686-w64-mingw32/sys-root/mingw/bin/icui18n71.dll"       $$shell_path($${OUT_PWD}/);
  copy_files.commands += $(COPY_FILE) "/usr/i686-w64-mingw32/sys-root/mingw/bin/icui18n71.dll"       $$shell_path($${OUT_PWD}/);
  copy_files.commands += $(COPY_FILE) "/usr/i686-w64-mingw32/sys-root/mingw/bin/icuuc71.dll"         $$shell_path($${OUT_PWD}/);

  copy_files.commands += $(COPY_FILE) "/usr/i686-w64-mingw32/sys-root/mingw/bin/icui18n73.dll"       $$shell_path($${OUT_PWD}/);
  copy_files.commands += $(COPY_FILE) "/usr/i686-w64-mingw32/sys-root/mingw/bin/icuuc73.dll"         $$shell_path($${OUT_PWD}/);
  copy_files.commands += $(COPY_FILE) "/usr/i686-w64-mingw32/sys-root/mingw/bin/icudata73.dll"       $$shell_path($${OUT_PWD}/);

  copy_files.commands += $(COPY_FILE) "/usr/i686-w64-mingw32/sys-root/mingw/bin/libcrypto-3.dll"     $$shell_path($${OUT_PWD}/);
  copy_files.commands += $(COPY_FILE) "/usr/i686-w64-mingw32/sys-root/mingw/bin/libexpat-1.dll"      $$shell_path($${OUT_PWD}/);
  copy_files.commands += $(COPY_FILE) "/usr/i686-w64-mingw32/sys-root/mingw/bin/libfontconfig-1.dll" $$shell_path($${OUT_PWD}/);
  copy_files.commands += $(COPY_FILE) "/usr/i686-w64-mingw32/sys-root/mingw/bin/libfreetype-6.dll"   $$shell_path($${OUT_PWD}/);
  copy_files.commands += $(COPY_FILE) "/usr/i686-w64-mingw32/sys-root/mingw/bin/libgcc_s_dw2-1.dll"  $$shell_path($${OUT_PWD}/);
  copy_files.commands += $(COPY_FILE) "/usr/i686-w64-mingw32/sys-root/mingw/bin/libglib-2.0-0.dll"   $$shell_path($${OUT_PWD}/);
  copy_files.commands += $(COPY_FILE) "/usr/i686-w64-mingw32/sys-root/mingw/bin/libharfbuzz-0.dll"   $$shell_path($${OUT_PWD}/);
  copy_files.commands += $(COPY_FILE) "/usr/i686-w64-mingw32/sys-root/mingw/bin/libintl-8.dll"       $$shell_path($${OUT_PWD}/);

  copy_files.commands += $(COPY_FILE) "/usr/i686-w64-mingw32/sys-root/mingw/bin/libpcre2-16-0.dll"   $$shell_path($${OUT_PWD}/);
  copy_files.commands += $(COPY_FILE) "/usr/i686-w64-mingw32/sys-root/mingw/bin/libpcre2-8-0.dll"    $$shell_path($${OUT_PWD}/);
  copy_files.commands += $(COPY_FILE) "/usr/i686-w64-mingw32/sys-root/mingw/bin/libpng16-16.dll"     $$shell_path($${OUT_PWD}/);
  copy_files.commands += $(COPY_FILE) "/usr/i686-w64-mingw32/sys-root/mingw/bin/libsqlite3-0.dll"    $$shell_path($${OUT_PWD}/);
  copy_files.commands += $(COPY_FILE) "/usr/i686-w64-mingw32/sys-root/mingw/bin/libssp-0.dll"        $$shell_path($${OUT_PWD}/);
  copy_files.commands += $(COPY_FILE) "/usr/i686-w64-mingw32/sys-root/mingw/bin/libstdc++-6.dll"     $$shell_path($${OUT_PWD}/);
  copy_files.commands += $(COPY_FILE) "/usr/i686-w64-mingw32/sys-root/mingw/bin/libwinpthread-1.dll" $$shell_path($${OUT_PWD}/);
  copy_files.commands += $(COPY_FILE) "/usr/i686-w64-mingw32/sys-root/mingw/bin/Qt6Core.dll"         $$shell_path($${OUT_PWD}/);
  copy_files.commands += $(COPY_FILE) "/usr/i686-w64-mingw32/sys-root/mingw/bin/Qt6Gui.dll"          $$shell_path($${OUT_PWD}/);
  copy_files.commands += $(COPY_FILE) "/usr/i686-w64-mingw32/sys-root/mingw/bin/Qt6Widgets.dll"      $$shell_path($${OUT_PWD}/);

  copy_files.commands += $(COPY_FILE) "/usr/i686-w64-mingw32/sys-root/mingw/bin/zlib1.dll"           $$shell_path($${OUT_PWD}/);
  copy_files.commands += $(COPY_FILE) "/usr/i686-w64-mingw32/sys-root/mingw/bin/icui18n71.dll"       $$shell_path($${OUT_PWD}/);
  copy_files.commands += $(COPY_FILE) "/usr/i686-w64-mingw32/sys-root/mingw/bin/icuuc71.dll"         $$shell_path($${OUT_PWD}/);



  copy_files.commands += $(COPY_DIR) $$shell_path(/usr/i686-w64-mingw32/sys-root/mingw/lib/qt6/plugins/platforms)  $$shell_path($${OUT_PWD}/) ; \

}}


win32{
  #win32 actualy means any windows, not 32 bits windows


  contains(QT_ARCH, x86_64) { #64 bits
  message(WIN64: $$QT_ARCH )
  copy_files.commands += echo "WIN64 copy dll";
  copy_files.commands += $(COPY_FILE) "/usr/x86_64-w64-mingw32/sys-root/mingw/bin/Qt6Core.dll"         $$shell_path($${OUT_PWD}/);
  copy_files.commands += $(COPY_FILE) "/usr/x86_64-w64-mingw32/sys-root/mingw/bin/Qt6Gui.dll"          $$shell_path($${OUT_PWD}/);
  copy_files.commands += $(COPY_FILE) "/usr/x86_64-w64-mingw32/sys-root/mingw/bin/Qt6Multimedia.dll"   $$shell_path($${OUT_PWD}/);
  copy_files.commands += $(COPY_FILE) "/usr/x86_64-w64-mingw32/sys-root/mingw/bin/Qt6MultimediaWidgets.dll" $$shell_path($${OUT_PWD}/);
  copy_files.commands += $(COPY_FILE) "/usr/x86_64-w64-mingw32/sys-root/mingw/bin/Qt6Network.dll"      $$shell_path($${OUT_PWD}/);
  copy_files.commands += $(COPY_FILE) "/usr/x86_64-w64-mingw32/sys-root/mingw/bin/Qt6Widgets.dll"      $$shell_path($${OUT_PWD}/);


  copy_files.commands += $(COPY_FILE) "/usr/x86_64-w64-mingw32/sys-root/mingw/bin/iconv.dll"     $$shell_path($${OUT_PWD}/);
  copy_files.commands += $(COPY_FILE) "/usr/x86_64-w64-mingw32/sys-root/mingw/bin/icudata71.dll" $$shell_path($${OUT_PWD}/);
  copy_files.commands += $(COPY_FILE) "/usr/x86_64-w64-mingw32/sys-root/mingw/bin/icui18n71.dll" $$shell_path($${OUT_PWD}/);
  copy_files.commands += $(COPY_FILE) "/usr/x86_64-w64-mingw32/sys-root/mingw/bin/icuuc71.dll"   $$shell_path($${OUT_PWD}/);
  copy_files.commands += $(COPY_FILE) "/usr/x86_64-w64-mingw32/sys-root/mingw/bin/icuuc73.dll"    $$shell_path($${OUT_PWD}/);
  copy_files.commands += $(COPY_FILE) "/usr/x86_64-w64-mingw32/sys-root/mingw/bin/icui18n73.dll"  $$shell_path($${OUT_PWD}/);
  copy_files.commands += $(COPY_FILE) "/usr/x86_64-w64-mingw32/sys-root/mingw/bin/icudata73.dll"  $$shell_path($${OUT_PWD}/);

  copy_files.commands += $(COPY_FILE) "/usr/x86_64-w64-mingw32/sys-root/mingw/bin/libbz2-1.dll" $$shell_path($${OUT_PWD}/);
  copy_files.commands += $(COPY_FILE) "/usr/x86_64-w64-mingw32/sys-root/mingw/bin/libcrypto-3-x64.dll" $$shell_path($${OUT_PWD}/);
  copy_files.commands += $(COPY_FILE) "/usr/x86_64-w64-mingw32/sys-root/mingw/bin/libexpat-1.dll" $$shell_path($${OUT_PWD}/);

  copy_files.commands += $(COPY_FILE) "/usr/x86_64-w64-mingw32/sys-root/mingw/bin/libfontconfig-1.dll" $$shell_path($${OUT_PWD}/);
  copy_files.commands += $(COPY_FILE) "/usr/x86_64-w64-mingw32/sys-root/mingw/bin/libfreetype-6.dll" $$shell_path($${OUT_PWD}/);
  copy_files.commands += $(COPY_FILE) "/usr/x86_64-w64-mingw32/sys-root/mingw/bin/libglib-2.0-0.dll" $$shell_path($${OUT_PWD}/);
  copy_files.commands += $(COPY_FILE) "/usr/x86_64-w64-mingw32/sys-root/mingw/bin/libharfbuzz-0.dll" $$shell_path($${OUT_PWD}/);
  copy_files.commands += $(COPY_FILE) "/usr/x86_64-w64-mingw32/sys-root/mingw/bin/libintl-8.dll" $$shell_path($${OUT_PWD}/);
  copy_files.commands += $(COPY_FILE) "/usr/x86_64-w64-mingw32/sys-root/mingw/bin/libpcre2-16-0.dll" $$shell_path($${OUT_PWD}/);
  copy_files.commands += $(COPY_FILE) "/usr/x86_64-w64-mingw32/sys-root/mingw/bin/libpcre2-8-0.dll" $$shell_path($${OUT_PWD}/);
  copy_files.commands += $(COPY_FILE) "/usr/x86_64-w64-mingw32/sys-root/mingw/bin/libpng16-16.dll" $$shell_path($${OUT_PWD}/);
  copy_files.commands += $(COPY_FILE) "/usr/x86_64-w64-mingw32/sys-root/mingw/bin/libsqlite3-0.dll" $$shell_path($${OUT_PWD}/);
  copy_files.commands += $(COPY_FILE) "/usr/x86_64-w64-mingw32/sys-root/mingw/bin/libssp-0.dll" $$shell_path($${OUT_PWD}/);
  copy_files.commands += $(COPY_FILE) "/usr/x86_64-w64-mingw32/sys-root/mingw/bin/libstdc++-6.dll" $$shell_path($${OUT_PWD}/);
  copy_files.commands += $(COPY_FILE) "/usr/x86_64-w64-mingw32/sys-root/mingw/bin/libwinpthread-1.dll" $$shell_path($${OUT_PWD}/);
  copy_files.commands += $(COPY_FILE) "/usr/x86_64-w64-mingw32/sys-root/mingw/bin/zlib1.dll" $$shell_path($${OUT_PWD}/);

  copy_files.commands += $(COPY_FILE) "/usr/x86_64-w64-mingw32/sys-root/mingw/bin/libgcc_s_seh-1.dll"           $$shell_path($${OUT_PWD}/);



  copy_files.commands += $(COPY_DIR) $$shell_path(/usr/x86_64-w64-mingw32/sys-root/mingw/lib/qt6/plugins/platforms)  $$shell_path($${OUT_PWD}/) ; \
}
}



